# Write function to convert polygon to bool field
##TODO: this file may need to be moved to MetricSpace/MetricTool after finished

using Test, Revise
import PyPlot, MetaGraphs, PyCall
using Suppressor
using MetricTools, MetricSpaces, MetricSpaces.Planar, MetricTools.MetricGraphs
using SignedDistanceFields
using JLD

using
    MotionPlanning,
    MotionPlanning.MetricCells
using 
    MetricTools, 
    MetricSpaces.Planar, 
    MetricTools.MetricGraphs,
    MetricSpaces,
    MetricSpaces.Arms

function generate_bool_field(n_x, n_y, space::Space)
    result = Array{Bool}(undef, n_x, n_y)
    # read file 
    xx = range(space.bounds[1].lo, space.bounds[1].hi, length=n_x)
    yy = range(space.bounds[2].lo, space.bounds[2].hi, length=n_y)
    println("xx is: ", xx)
    println("yy is: ", yy)
    for i in 1:n_x
        for j in 1:n_y
            pt::Point = [xx[i], yy[j]]
            if isfree(space, pt)
                result[i, j] = false
            else
                result[i, j] = true
            end
        end
    end
    return result
end


function generate_distance_field(space::Space, nx::Integer, ny::Integer, interval::Float64)
    result = generate_bool_field(nx, ny, space)
    final_result = sdf(result)*interval
    return final_result
end

function read_jld(space_name, nx)
    #read file
    final_result = load("data/output/jld/"*space_name*"-"*string(nx)*".jld")["data"]
    return final_result
end

function test_error(space::Space, nx::Integer, ny::Integer)
    xx = range(space.bounds[1].lo, space.bounds[1].hi, length=nx)
    yy = range(space.bounds[2].lo, space.bounds[2].hi, length=ny)
    residual = 0
    counter = 0
    for i in 1:nx
        for j in 1:ny
            pt::Point = [xx[i], yy[j]]
            if isfree(space, pt)
                d_safe::Float64 = cert(space, pt)
                # println("difference is: ", d_safe - final_result[i, j])
                # @time residual += abs(final_result[i, j] - d_safe) 
                counter += 1
            else
                # println("blocked")
                # println(final_result[i,j])
            end
        end
    end
    println(residual/counter)
end

function calculate_ws_bounds(arm::Arm)::Array{Bound}
    l::Float64 = sum(arm.lengths)
    return [Bound(p-l, p+l) for p in arm.base] 
end

function generate_to_jld()
    space_names = ["world1-v2", "world-ball", "world-maze", "world-u_l", "wholeOB", "world2", "world5", "world-3dArm", "world-4dArm", "world-4dArm2", "world-halfdoor"]

    space_name = space_names[11]
    space::Space = PlanarSpace(filename="data/input/spaces/planar/"*space_name*".json") 
    nx = 1000
    ny = nx

    arm_index = 1

    use_arms = ["", "2D", "3D", "4D"]
    use_arm = use_arms[arm_index]

    function return_arm(i)
        if i == 2 return load_arm("data/input/spaces/arms/arms2-new.json") end
        if i == 3 return load_arm("data/input/spaces/arms/arms3-new.json") end
        if i == 4 return load_arm("data/input/spaces/arms/arms4-new.json") end
    end

    if arm_index > 1
        arm::Arm = return_arm(arm_index)
        space.bounds = calculate_ws_bounds(arm)
    end
    
    println("space.bounds are: ", space.bounds)

    interval = (space.bounds[1].hi - space.bounds[1].lo)/(nx-1) #Only works for the case that the map has same x_interval and y_interval
    print("the interval is: ", interval)
    println("space bounds: ", space.bounds)
    sdf = generate_distance_field(space, nx, ny, interval)
    save("data/output/jld/"*space_name*"-"*string(nx)*use_arm*".jld", "data", sdf)

    # read_jld(space_name, nx)
end


function convert_pt_to_index(pt::Point, interval)
    x_i::Integer = div(pt[1], interval) + 1
    x_extra = mod(pt[1], interval)
    if x_extra >= interval/2.0  x_i += 1  end

    y_i::Integer = div(pt[2], interval) + 1
    y_extra = mod(pt[2], interval)
    if y_extra >= interval/2.0  y_i += 1  end
    return x_i, y_i
end



# pt= [0.45, 0.55]
# x_i, y_i = convert_pt_to_index(pt, 0.25)
# println("the x_i y_i: ", x_i, " , ", y_i)

@time generate_to_jld()

