#region imports
using Test, Revise
import PyPlot

using MetricTools, MetricSpaces, MetricSpaces.Planar

include("scdm.jl")
#endregion imports


function create_simple_prm(N,space)
    k::Integer = floor(log(2, N))
    prm = PRM(space; k=k)
    grow!(prm, N)
    return space, prm
end

function test_prm(N,start,goal,space)
    println("\nTesting PRM...")
    space, prm = create_simple_prm(N,space)
    path = plan(prm, start, goal)
    dis = metric(prm.graph, path)
    return path,dis
end


function calculating_num_samples_for_prm_star(total_FP, D)
    
    return k::Integer = round(Int, log(2, N))
    total_FP = N*D + N*k*3/2.0
    return total_FP
end

function calculate_parameters_for_xgboost(total_FP,n_cells;n_trees=-1, n_depth=-1)
    #total_FP = n_cells*n_estimator*(2^(max_depth+1)-1)
    if n_depth != -1
        n_trees = round(Int,total_FP/(n_cells*(2^(n_depth+1)-1)))
        return n_trees
    else
        n_depth = round(Int,log(2,(floor(Int,total_FP/n_cells/n_trees)+1)))-1
        return n_depth
    end
end
#num_of_nodes for each tree is: 2^(max_depth+1)-1
#Thus, # parameters in total is <= n_cells*n_estimator*(2^(max_depth+1)-1)


function total_FP_of_prm(N,D)
    # total_FP = N*D + N*log(2,N)*3/2.0 
    ## adjacent list can only save index_of_p1, index_of_p2, dis
    k::Integer = round(Int, log(2, N))
    total_FP = N*D + N*k*3/2.0
    return total_FP
end

function total_FP_of_xgboost(n_cells,n_depth, n_trees)
    temp = n_cells*n_trees*(2^(n_depth+1)-1)
    println("temp is: ", temp)
    return temp
end
# function total_FP_of_rplr()
    ### Take every cell into account

function total_FP_of_one_rplr(plr)
    leaves::Array{BinaryPartition} = getleaves(plr)
    numparams::Integer = getnumparams(plr)
    println("num of leaves: ", length(leaves))
    println("num params: ", numparams)
    println("whether the relationship is *(D+1)")
    num_fp_in_bsp = length(leaves) - 1
    num_fp_bounds = 2 * D 
    return numparams + num_fp_in_bsp + num_fp_bounds
end




function main()
    space::Space = PlanarSpace(filename="data/input/spaces/planar/world1.json") #world1  world_door
    start::Point = [1, 1]
    goal::Point = [0, 0] #[0.5, 0.495] #

    N = 500 #num of samples
    D = 2 #dimension
    
    depth_of_dividing_cell = 2
    n_cells = 2^depth_of_dividing_cell
    total_FP_prm = total_FP_of_prm(N,D)
    println("Total_FP of prm* is: ", total_FP_prm)


    n_estimator= 1000
    max_depth = calculate_parameters_of_scdm(total_FP_prm,n_cells,n_estimator) 

    println("The max depth is: ", max_depth)
    total_FP_scdm = num_of_floating_points_of_scdm(n_cells,n_estimator,max_depth) 
    println("Total_FP of scdm is: ", total_FP_scdm)


    prm_path,dis_prm = test_prm(N,start,goal,space)
    prm_path_plot::Array{Line} = [(prm_path[i],prm_path[i+1]) for i in 1:length(prm_path)-1 ]
    println("The dis got from prm is: ", dis_prm)
    n::Integer = 20^2    # number of samples per cell
    ax = test_scdm(n,n_estimator,max_depth,depth_of_dividing_cell,space,start,goal)
    plot_lines(prm_path_plot,ax,color="green", zorder=2 ,vertices=true, linestyle="-.",alpha=0.5)


    

end


main()
