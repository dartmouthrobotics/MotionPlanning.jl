#region imports
# using Test
using PyPlot, PyCall, Revise
# using Plots
using MetricTools
using MetricSpaces, MetricSpaces.Planar,MetricTools.MetricGraphs
using Suppressor
if true
    include("../src/MotionPlanning.jl")
    using
        .MotionPlanning,
        .MotionPlanning.VisibilityGraphs,
        .MotionPlanning.ShadowCells,
        .MotionPlanning.PRMs,
        .MotionPlanning.SCDM
else    
    using
        MotionPlanning,
        MotionPlanning.VisibilityGraphs,
        MotionPlanning.ShadowCells,
        MotionPlanning.PRMs,
        MotionPlanning.SCDM
end
# Samplers = MotionPlanning.Planners.Samplers
#endregion imports

#region plot helpers
"""
Plots the contour of a function
"""
function plot_cell(func::Function, length, bounds, ax::PyObject;
    flevels=100::Integer, clevels=10::Integer, cbar=true::Bool)

    len = length
    x_bounds =  bounds[1]
    y_bounds =  bounds[2]

    plot_cell_rec(bounds, ax, facecolor="w", zorder=1)

    x = range(x_bounds.lo, stop=x_bounds.hi, length=len)
    y = range(y_bounds.lo, stop=y_bounds.hi, length=len)'
    
    function V(x_,y_)
        i = findall(e->e==x_, x)[1] #TODO: a faster way
        j = findall(e->e==y_, y')[1]
        # println(i,j)
        # println(func(i,j))
        return func(i, j)
    end
    z = V.(x, y)
    # println("z is: ", z )
    cpf = ax.contourf(x, y, z, levels=clevels, zorder=2) #levels=flevels,
    cp = ax.contour(x, y, z, levels=clevels, colors="black", zorder=3)
    if cbar 
        cbar_ = PyPlot.colorbar(cpf, ax=ax) 
#         cbar.set_label('Electric Field [V/m]')
        cbar_.set_ticks(clevels)
    end
end

"""
Plots a line
"""
function plot_line(line::Line, ax::PyObject; color="grey"::String, zorder=2::Integer, linestyle::String="solid",alpha=1.0)
    x, y = [p[1] for p in line], [p[2] for p in line]
    ax.plot(x, y; color=color, zorder=zorder, linestyle=linestyle,alpha=alpha)
end



"""
Plots an array of lines
"""
function plot_lines(lines::Array{Line}, ax::PyObject;
        color="grey"::String, zorder=2::Integer, vertices=false::Bool, linestyle::String="solid",alpha::Float64=1.0)

    for line in lines
        if vertices plot(line[1], ax; color=color, zorder=zorder) end
        plot_line(line, ax; color=color, zorder=zorder,linestyle=linestyle, alpha=0.7)
    end
    if vertices plot(last(last(lines)), ax; color=color, zorder=zorder) end
end

"""
Plots the boundary of a cell (the Rectangle)
"""
function plot_cell_rec(bounds, ax::PyObject;facecolor::String="grey", zorder::Integer=2)

    x_bounds =  bounds[1]
    y_bounds =  bounds[2]

    poly::Polygon = Polygon([
        [x_bounds.lo, y_bounds.lo],
        [x_bounds.hi, y_bounds.lo],
        [x_bounds.hi, y_bounds.hi],
        [x_bounds.lo, y_bounds.hi]
    ])
    poly_array::Array{Polygon} = [poly]
    MetricTools.plot(poly_array; ax=ax,facecolor=facecolor,zorder=zorder)

end

"""
Plots a point
"""
function plot(point::Point, ax::PyObject; color="grey"::String, zorder=3)
    ax.scatter([point[1]], [point[2]], color=color, edgecolors="black", zorder=3)
end

"""
Plots a list of points
"""
function plot(points::Array{Point}, ax::PyObject; color="grey"::String, zorder=3::Integer)
    if isempty(points) return end
    x, y = [p[1] for p in points], [p[2] for p in points]
    ax.scatter(x, y, color=color, edgecolors="black", zorder=zorder)
end
#endregion plot helpers

#region plots
function plot_gt(leaves)
    fig, ax = PyPlot.subplots()
    ax.title.set_text("Ground Truth of 1 cells")
    ax.set_aspect(aspect=1)
    cmin = -0.1
    cmax = 0.2
    lvls = range(cmin,stop=cmax, length=10)
    # for leaf in leaves
    for l in 1:length(leaves)
        leaf=leaves[l]
        ground_truth = ShadowCells.ground_truth(leaf.shadow_cell)
        if l != length(leaves)
            plot_cell(ground_truth, length(leaf.shadow_cell.total_bd_points),leaf.bounds, ax, clevels=lvls, cbar = false)
        else
            plot_cell(ground_truth, length(leaf.shadow_cell.total_bd_points),leaf.bounds, ax, clevels=lvls, cbar = true)
        end
    end
end


function plot_pred(leaves)
    fig, ax = PyPlot.subplots()
    ax.title.set_text("Prediction of 1 cells")
    ax.set_aspect(aspect=1)
    cmin = -0.1
    cmax = 0.2
    lvls = range(cmin,stop=cmax, length=10)
    for l in 1:length(leaves)
        leaf=leaves[l]
        predictforplot = ShadowCells.predict_for_plot(leaf.shadow_cell)
        if l != length(leaves)
            plot_cell(predictforplot, length(leaf.shadow_cell.total_bd_points), leaf.bounds, ax, clevels=lvls, cbar = false)
        else
            plot_cell(predictforplot, length(leaf.shadow_cell.total_bd_points), leaf.bounds, ax, clevels=lvls, cbar = true)
        end
    end
end



function plot_error(leaves,total_RMSE)
    fig, ax = PyPlot.subplots()
    ax.title.set_text("Error of 4 cells")
    ax.set_aspect(aspect=1)
    cmin = 0.0
    cmax = 0.12
    lvls = range(cmin,stop=cmax, length=50)
    for l in 1:length(leaves)
        leaf = leaves[l]
        ground_truth = ShadowCells.ground_truth(leaf.shadow_cell)
        predictforplot = ShadowCells.predict_for_plot(leaf.shadow_cell)
        RMSE = 0
        counter = 0
        function differ_function(x_i,y_i)
            temp = predictforplot(x_i,y_i)-ground_truth(x_i,y_i)
            t = temp*temp
            RMSE += t
            counter +=1
            return abs(temp)
        end
        if l != length(leaves)
            plot_cell(differ_function, length(leaf.shadow_cell.total_bd_points), leaf.bounds, ax, clevels=lvls, cbar = false)
        else
            plot_cell(differ_function, length(leaf.shadow_cell.total_bd_points), leaf.bounds, ax, clevels=lvls, cbar = true)
        end
        total_RMSE += sqrt(RMSE/counter)
        println("RMSE of current cell is: ", RMSE)
    end
    return total_RMSE
end

function plot_real_env(leaves, path::Array{Point}; refined_path::Array=[])
    fig,ax = PyPlot.subplots()
    ax.set_aspect(aspect=1)
    ax.title.set_text("Real Map of 4 cells")
    cmin = 0.0
    cmax = 2.0  ### previously, 2.0
    lvls = range(cmin,stop=cmax, length=50)
    for leaf in leaves
        plot_cell_rec(leaf.bounds, ax, facecolor="w", zorder=1)
        # MetricGraphs.plot(leaf.shadow_cell.prm.graph; fig=fig, ax=ax) ### plot the graph -for generate all-pairs
    end
    MetricTools.plot(leaves[1].shadow_cell.prm.space.obstacles; ax=ax)  #plot obstacle, only need to plot once        #################
    # MetricGraphs.plot(global_graph; fig=fig, ax=ax)  #plot all-points-connecton
    path_plot::Array{Line} = [(path[i],path[i+1]) for i in 1:length(path)-1 ]
    plot_lines(path_plot,ax,color="red", zorder=2 ,vertices=true,linestyle="-.",alpha=0.5)
    # Visualization.plot_path(path, ax) #Use same ax
    """Plot the refined path using color = blue"""
    println("--------------The refined path:", refined_path)
    if !isempty(refined_path)
        refined_path_plot::Array{Line} = [(refined_path[i],refined_path[i+1]) for i in 1:length(refined_path)-1 ]
        plot_lines(refined_path_plot,ax,color="blue", zorder=2 ,vertices=true,linestyle="-.",alpha=0.5)
        # Visualization.plot_path(refined_path_plot, ax, color = "blue") #Use same ax
    end
    #PyPlot.savefig("1")
    return ax
end


function plot_vg_path(ax,space, start,goal)
    vg = VisibilityGraphs.VisibilityGraph(space)
    vg_path::Array{Point} = VisibilityGraphs.plan(vg, start, goal)
    println("Path got from vg is: ", vg_path)
    vg_dis = metric(vg.graph, vg_path)
    println("--------final dis got from vg is: ", vg_dis)
    vg_path_plot::Array{Line} = [(vg_path[i],vg_path[i+1]) for i in 1:length(vg_path)-1 ]
    plot_lines(vg_path_plot,ax,color="orange", zorder=2 ,vertices=true, linestyle="-.",alpha=0.5)
end
#endregion plots

#region utils
function construct_scdm(xb,yb,depth,space,n,n_estimator,depth_of_tree)
    root_cell = SCDM.Cell([xb,yb],0)
    SCDM.divide_scdm(root_cell, depth)                                                
    leaves = SCDM.get_all_leaves(root_cell)
    vg = VisibilityGraphs.VisibilityGraph(space)
    for leaf in leaves
        leaf.shadow_cell= ShadowCell(
            space,
            n,
            leaf.bounds;
            use_2D_rplr=true,
            use_localplanner=true,
            r_n_estimators=n_estimator,
            max_depth=depth_of_tree,
            vg_array=[vg]
            )
    end
    return leaves
end

#####TODO: should change construct glocal graph with find a path
function query_scdm(start,goal,leaves,global_graph; if_refine=true)
    space = leaves[1].shadow_cell.prm.space
    if isfree(space, start) && isfree(space, goal)
        SCDM.add_points_into_global_graph([start, goal],leaves, global_graph)
        path::Array{Point}, path_dis = SCDM.find_path(start, goal, global_graph)
        all_path_segs_free, refined_path, refined_leaves_set,new_path_dis = SCDM.refine_path(path, space, leaves,path_dis, global_graph)
        println("final Refined path distance is: ", new_path_dis)  
        if all_path_segs_free
            println("Originial path's segs are all free")
        end
        #### Make change here  ==> hard to add refine in, but may need to use A* since only one inter point may not work
        # return path, refined_path,new_path_dis
        return path, path,path_dis
    else
        println("There is NO path between start and goal because one of it is not free")
        return [], [], -1
    end
    
end

#endregion utils

function test_scdm(n,n_estimator,max_depth,depth_of_dividing_cell,space,start,goal)

    xb = Bound(0.0, 1.0)
    yb = Bound(0.0, 1.0)
    leaves = construct_scdm(xb,yb,depth_of_dividing_cell,space,n,n_estimator,max_depth)
    # global_graph = SCDM.construct_global_graph(leaves) #construct the global_graph
    # path, refined_path,new_path_dis = query_scdm(start,goal,leaves,global_graph; if_refine=true)
    println("The number of cells is: ", length(leaves))
    #----------- Plot scdm -----------------------#
    plot_gt(leaves)
    plot_pred(leaves)
    total_RMSE = 0
    total_RMSE = plot_error(leaves, total_RMSE)
    println("RMSE in total is: ", total_RMSE)

    # ax = plot_real_env(leaves, path, refined_path=refined_path)
    # plot_vg_path(ax,space, start,goal)
    #----------- Plot scdm -----------------------#
    return leaves[1]
end
### Write a function for plot the paths

function predict_for_plot2(shadowcell::ShadowCell, global_graph,leaves)::Function
    prm = shadowcell.prm
    vg_array = shadowcell.vg_array
    all_free_points = shadowcell.all_free_points
    total_bd_points = shadowcell.total_bd_points
    function paths_helper(x_i,y_i)
        i_point = total_bd_points[x_i]
        j_point = total_bd_points[y_i]
        if (!isfree(prm.space, i_point)) || (!isfree(prm.space, j_point)) # TODO: need to have ability to auto got
            return -1.0
        else
            i_index = findall(x->x==i_point, all_free_points)[1]
            j_index = findall(x->x==j_point, all_free_points)[1]
            if localplanner(prm.space, i_point,j_point)  # TODO: delete this????
                return 0.0
            else
                # if dis == Inf  # TODO: should cover this one???
                #     return -2.0
                @suppress begin
                    path, refined_path, dis = query_scdm(i_point,j_point,leaves,global_graph, if_refine=true)
                    metric_dis = MetricGraphs.metric(prm.graph, i_point, j_point)
                    return dis-metric_dis  
                end
            end
        end
    end
    return (x, y) -> begin
        temp = paths_helper(x,y)
        if temp < 0
            println("WRONG!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            println("the distance of :",total_bd_points[x]," and ",total_bd_points[y], " is :", temp , " and metric dis is: ", MetricGraphs.metric(prm.graph, total_bd_points[x], total_bd_points[y]) )
        end
        return temp
    end
end

function plot_paths(one_shadowcell,global_graph,leaves)
    fig, ax = PyPlot.subplots()
    ax.title.set_text("Path values using 4 cells")
    ax.set_aspect(aspect=1)
    cmin = -0.1
    cmax = 0.2
    lvls = range(cmin,stop=cmax, length=10)
    PathsOfCell = predict_for_plot2(one_shadowcell.shadow_cell, global_graph,leaves)
    plot_cell(PathsOfCell, length(one_shadowcell.shadow_cell.total_bd_points), one_shadowcell.bounds, ax, clevels=lvls, cbar = true)

end



function test_scdm_plot(n,n_estimator,max_depth,space, one_shadowcell)
    depth_of_dividing_cell=2
    xb = Bound(0.0, 1.0)
    yb = Bound(0.0, 1.0)
    leaves = construct_scdm(xb,yb,depth_of_dividing_cell,space,n,n_estimator,max_depth)
    global_graph = SCDM.construct_global_graph(leaves) #construct the global_graph
    plot_paths(one_shadowcell, global_graph,leaves)
end

function main()
    space::Space = PlanarSpace(filename="data/input/spaces/planar/world1.json") #world_maze_original world1 world10 empty world-spanner world-wholeOB  world_small world_maze
    n1::Integer = 39^2#9^2#    # number of samples per cell
    depth_of_dividing_cell = 0 #depth of dividing cell
    max_depth = 3  #depth of each tree
    n_estimator = 1000
    goal::Point = [0.0, 0.0]  #[0.5, 0.495]
    start::Point = [1.0, 1.0]#[0.5, 0.495]
    one_shadowcell = test_scdm(n1,n_estimator,max_depth,depth_of_dividing_cell,space,start,goal)
    n2::Integer = 20^2#5^2
    test_scdm_plot(n2,n_estimator,max_depth,space, one_shadowcell)
end
@suppress begin main() end
# main()
