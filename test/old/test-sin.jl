#region imports
# using Test
using PyPlot, PyCall, Revise
using MetricTools, MetricTools.MetricGraphs, MetricRegressors

using
    MetricSpaces,
    MetricSpaces.Planar,
    MetricSpaces.Samplers,
    MetricSpaces.BinaryPartitions,
    MetricSpaces.PLRs

function plot_line_chart(x::Array, y::Array, ax::PyObject, label::String)
    ax.plot(x,y,label=label,linestyle="--")
end

D = 1

function y(x)
    return sin(x)
end

function calculate_parameters_of_xgboost(total_FP,n_cells;n_trees=-1, n_depth=-1)
    #total_FP = n_cells*n_estimator*(2^(max_depth+1)-1)
    if n_depth != -1
        n_trees = round(Int,total_FP/(n_cells*(2^(n_depth+1)-1)))
        return n_trees
    else
        n_depth = round(Int,log(2,(floor(Int,total_FP/n_cells/n_trees)+1)))-1
        return n_depth
    end
end

function total_FP_of_xgboost(n_cells,n_depth, n_trees)
    temp = n_cells*n_trees*(2^(n_depth+1)-1)
    println("temp is: ", temp)
    return temp
end
# function total_FP_of_rplr()
    ### Take every cell into account

function total_FP_of_one_rplr(plr)
    leaves::Array{BinaryPartition} = getleaves(plr)
    numparams::Integer = getnumparams(plr)
    println("num of leaves: ", length(leaves))
    println("num params: ", numparams)
    println("whether the relationship is *(D+1)")
    num_fp_in_bsp = length(leaves) - 1
    num_fp_bounds = 2 * D 
    return numparams + num_fp_in_bsp + num_fp_bounds
end

function main()
    x = 0:0.1:10
    X::Array{Point} = [[i] for i in x]
    println("X: ", X)
    Y = [y(i) for i in x]
    println("Num of samples are: ", length(x))
    fig, ax = PyPlot.subplots()
    plot_line_chart(X,Y, ax, "Real value")
    function create_model(use_rplr;n_tree=8, n_depth=3)
        reg::Union{Regressor, PyCall.PyObject} = use_rplr ? 
            PLR(PlanarSpace(bounds=[Bound(0, 10)])) : 
            XGBoostRegressor(; epochs=n_tree, maxdepth=n_depth)                       
        if use_rplr
            println("Use plr")
            train!(reg, X, Y; maxerror=0.00001, maxdepth=20)
        else
            train!(reg, X, Y)
        end
        println(reg)
        Ps = predict(reg, X)
        if use_rplr
            plot_line_chart(X,Ps, ax, "plr")
        else
            plot_line_chart(X,Ps, ax, "xgboost")
        end
        return reg
        # println(reg)
    end
    plr = create_model(true)
    total_FP_rplr = total_FP_of_one_rplr(plr)
    println("total_FP_rplr: ", total_FP_rplr)
    n_tree = calculate_parameters_of_xgboost(total_FP_rplr,1;n_depth=3)
    total_FP_xgboost = total_FP_of_xgboost(1,3, n_tree)
    println("number of trees is : ", n_tree)
    println("total_FP_of_xgboost: ",total_FP_of_xgboost)
    xgboost = create_model(false,n_tree=n_tree, n_depth=3)
    
    
    ax.legend()
    # Add titles
    ax.title.set_text("y = sin(x)")
    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    # PyPlot.savefig("data/output/images/Test how plr works")
end

main()
