#region imports
# using Test
import PyPlot
using PyCall, Revise
# using Plots
using MetricTools, MetricRegressors
using
    MetricSpaces,
    MetricSpaces.Planar,
    MetricSpaces.Samplers,
    MetricSpaces.BinaryPartitions,
    MetricSpaces.PLRs
    

using Suppressor
using BenchmarkTools
if false
    include("../src/MotionPlanning.jl")
    using
        .MotionPlanning,
        .MotionPlanning.VisibilityGraphs,
        .MotionPlanning.ShadowCells,
        .MotionPlanning.PRMs,
        .MotionPlanning.SCDM
else    
    using
        MotionPlanning,
        MotionPlanning.VisibilityGraphs,
        MotionPlanning.ShadowCells,
        MotionPlanning.PRMs,
        MotionPlanning.SCDM
end
# Samplers = MotionPlanning.Planners.Samplers
#endregion imports

function create_simple_prm(N,space)
    k::Integer = floor(log(2, N))
    prm = PRM(space; k=k)
    grow!(prm, N)
    return space, prm
end

function test_prm(N,start,goal,space)
    println("\nTesting PRM...")
    space, prm = create_simple_prm(N,space)
    path = plan(prm, start, goal)
    dis = metric(prm.graph, path)
    return path, dis
end

function main()
    space::Space = PlanarSpace(filename="data/input/spaces/planar/world3.json") #world-wholeOB  world1-v2#world_maze_original world1 world10 empty world-spanner world-wholeOB  world_small world_maze
    n_pts_per_edge::Integer = 10
    n_pts_per_edge_used_for_global_graph::Integer = 10
    n::Integer = n_pts_per_edge^2    # number of samples per cell
    depth_of_dividing_cell = 0 #depth of dividing cell
    start::Point = [0.0, 0.0]#[0.05, 0.1]  # #  #
    goal::Point =  [0.5, 1.0] #[0.5, 0.495] [0.8, 0.8]
    # start::Point = [0.05, 0.1]  
    # goal::Point = [0.8, 0.8]
    r_epoch = 1000  ## Used for xgboost regressor
    clf = XGBoostRegressor(; epochs=200, maxdepth=3)  ## Only xgboost is used for classifier
    
    use_2D_rplr = false
    use_nD_rplr = true

    """*---------------Define which regressor to use--------------*"""
    reg = use_2D_rplr ? 
            PLR([Bound(0, n_pts_per_edge*4-4+1) for _ in 1:2]; maxerror=0.0000001, maxdepth=50, minsamples=5) :
            (   use_nD_rplr ? 
                PLR([Bound(0, 1) for _ in 1:space.dim*2]; maxerror=0.0000001, maxdepth=50, minsamples=space.dim*2+2) :
                XGBoostRegressor(; epochs=r_epoch, maxdepth=3)
            )
    """*---------------Construct scdm--------------*"""
    leaves = construct_scdm(
            clf,
            reg,
            depth_of_dividing_cell,
            space,
            n;
            num_pts_per_edge=n_pts_per_edge_used_for_global_graph, #Used to construct global graph
            use_vg_as_gt=true,
            use_localplanner=true,
            use_2D_rplr=use_2D_rplr,
            use_nD_rplr=use_nD_rplr,
            plot_rplr=false,
            use_multi_resolution_approach=false,
            max_cell_depth=15)

    ### When set use_multi_resolution_approach = true,
    ###     -> need to set use_vg_as_gt = false

    # println("The number of cells is: ", length(leaves))

    """*---------------Query scdm--------------*"""
    path, path_dis, refined_path,refined_path_dis = SCDM.query_path_and_construct_global_graph_using_Astar(start, goal, leaves)
    
    println("Whether cell is traversed: ")
    for leaf in leaves
        println("Cell: ", leaf.bounds, " : ", leaf.added_to_global_graph)
    end
    
    # global_graph = SCDM.construct_global_graph(leaves) #construct the global_graph
    # path, path_dis, refined_path,refined_path_dis =  SCDM.query_scdm(start,goal,leaves,global_graph; if_refine=true)
    
    println("The path is: ", path, " dis is: ", path_dis)
    println("The refined path is: ", refined_path, " dis is: ", refined_path_dis)
    if path_dis == Inf
        println("There is NO path between start and goal")
        println("----It may due to one of that is not free")
        println("----Or no path between these two ")
        println("----Or the resolution is too low")
    end

    """*---------------Plot env and paths--------------*"""
    # ax, vg_path, vg_dis = plot_env_and_paths(
    #     leaves,
    #     space,
    #     start,
    #     goal;
    #     path=path,
    #     refined_path=refined_path,
    #     plot_env=true,
    #     plot_path_of_scdm=false,
    #     plot_path_of_vg=true
    #     )
    # # println("The vg path is: ", vg_path, " dis is: ", vg_dis)


    fig, ax = PyPlot.subplots()
    ax.set_aspect(aspect=1)
    plot_real_env(leaves,ax,title="") 
    
    plot_vg_path(ax,space, start,goal;linestyle="-", linecolor="red")
    PyPlot.savefig("data/output/images/localplanner-no_obstacle")

end
    
main()
# @suppress begin main() end
# @btime @suppress begin main() end
# output = @capture_out begin
#     @btime @suppress begin main() end
# end
# println("println the output: ", output)


