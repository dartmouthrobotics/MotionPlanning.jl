# Tests the shadow cells

#region imports
using Test, Revise
import PyPlot
using MetricTools
using MetricSpaces, MetricSpaces.Planar

if false
    include("../src/MotionPlanning.jl")
    using
        .MotionPlanning,
        .MotionPlanning.ShadowCells,
        .MotionPlanning.PRMs
end

using
    MotionPlanning,
    MotionPlanning.ShadowCells,
    MotionPlanning.PRMs
#endregion imports

function test_shadow()
    space::Space = PlanarSpace(filename="data/input/spaces/planar/world1.json")
    n::Integer = 7^2    # number of samples

    shadow_cell::ShadowCell = ShadowCell(space, n)
end

test_shadow()
