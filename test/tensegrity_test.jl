#=
 * File: arms.jl
 * Project: test
 * File Created: Sunday, 8th December 2019 4:05:45 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 1st March 2020 6:04:08 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#


###! Use Differtial kinematics to let 2D arm to draw a straight line
using Test, Revise
import PyPlot, MetaGraphs, PyCall
using Suppressor #BenchmarkTools
using 
    MetricTools, 
    MetricSpaces.Planar, 
    MetricTools.MetricGraphs,
    MetricSpaces,
    MetricSpaces.Arms,
    MetricSpaces.Samplers,
    MetricSpaces.BinaryPartitions

using
    MotionPlanning,
    MotionPlanning.MetricCells
using NLsolve
# We have x2_dot, y2_dot, we want to know theta1_dot, theta2_dot to calculate next configuration theta1, theta2
# initialize value

l1 = l2 = 0.6


function calculate_end_point(theta::Point)
    x1 = l1*cos(theta[1])+l2*cos(theta[1]+theta[2])
    y1 = l1*sin(theta[1])+l2*sin(theta[1]+theta[2])
    end_point::Point = [x1, y1]
    return end_point
end
function test()

    space::ArmSpace = ArmSpace(
        "data/input/spaces/arms/arms2.json", 
        "data/input/spaces/planar/world-empty.json", 
    )

    # initialize
    x2_dot = 1
    y2_dot = 0
    # theta1 = pi/2
    # theta2 = -pi
    theta1 = pi/4
    theta2 = -pi/2
    timestamp = 0.01
    path::Array{Point} = []
    c_point::Point = [theta1, theta2]
    end_point::Point = calculate_end_point(c_point)
    push!(path, c_point)
    path_end_points::Array{Point} = []
    push!(path_end_points, end_point)
    for i in 1:100
        J11 = -(l1*sin(theta1)+l2*sin(theta1+theta2))
        J12 = -l2*sin(theta1+theta2)
        J21 = l1*cos(theta1)+l2*cos(theta1+theta2)
        J22 = l2*cos(theta1+theta2)


        # J11*theta1_dot + J12*theta2_dot = x2_dot
        # J21*theta1_dot + J22*theta2_dot = y2_dot

        #Then got relationship between theta1_dot and theta2_dot

        function f!(F, theta_dot)
            F[1] = J11*theta_dot[1] + J12*theta_dot[2] - x2_dot
            F[2] = J21*theta_dot[1] + J22*theta_dot[2] - y2_dot
        end
        r = nlsolve(f!, [c_point[1]; c_point[2]], autodiff = :forward)

        ## Got theta1_dot, theta2_dot
        (theta1_dot, theta2_dot) = r.zero

        ## Update theta1 and theta2

        theta1 += theta1_dot * timestamp
        theta2 += theta2_dot * timestamp

        c_point = [theta1, theta2]
        push!(path, c_point)
        end_point = calculate_end_point(c_point)
        push!(path_end_points, end_point)
        println("current theta1, theta2 is: ", theta1, " , ", theta2)
        println("current end point is: ", end_point)
        if 1.2 >= end_point[1] >= -1.2 && 1.2 >= end_point[2] >= -1.2
            continue
        else
            println("the end_point is illegal")
            break
        end
    end
    fig, ax = PyPlot.subplots()
    plot(space, path, ax=ax,tstep=0.25)
    fig,ax = PyPlot.subplots()
    plot(path_end_points)
end

test()

