#=
 * File: arms.jl
 * Project: test
 * File Created: Sunday, 8th December 2019 4:05:45 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 1st March 2020 6:04:08 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#
using Test, Revise
import PyPlot, MetaGraphs, PyCall
using Suppressor #BenchmarkTools
using 
    MetricTools, 
    MetricSpaces.Planar, 
    MetricTools.MetricGraphs,
    MetricSpaces,
    MetricSpaces.ReedsShepp,
    MetricSpaces.Samplers,
    MetricSpaces.BinaryPartitions

using
    MotionPlanning,
    MotionPlanning.MetricCells

    # MetricSpaces.PLRs
    
function get_num_samples_per_edge_for_current_cell(; current_depth=0, max_cell_depth=7 ,n_pts_per_edge_for_multi_reso = 3)
    function get_num_samples_per_edge_for_current_cell_helper(d)
        if d == max_cell_depth
            return n_pts_per_edge_for_multi_reso
        else
            return 2 * get_num_samples_per_edge_for_current_cell_helper(d+1) - 1
        end
    end
    get_num_samples_per_edge_for_current_cell_helper(current_depth)
end


function testRScar(; opt=true, verbose=true, ifplot=true)

    space::ReedsSheppSpace = ReedsSheppSpace(;
    filename="data/input/spaces/planar/angled-doors.json",
    r=0.5
    )
    start::Point = [0, 0, 0]
    goal::Point = [0.9, 0.9, pi/2]

    if verbose println("space.bounds is: ", space.bounds) end

    delta::Float64 = 0.1
    epsilon::Float64 = 0.5

    n_pts::Int64 = get_num_samples_per_edge_for_current_cell(;current_depth=0, max_cell_depth=7,n_pts_per_edge_for_multi_reso = 3)
    if verbose println("n_pts is: ", n_pts) end

    root_cell::MetricCell = MetricCell(space, space.bounds,0; n_pts=n_pts)
    if verbose println("start to construct cells ...") end
    fleaves::Array{MetricCell}, mleaves::Array{MetricCell}, bleaves::Array{MetricCell} = construct!(space, root_cell; epsilon=epsilon, delta=delta)

    fmleaves::Array{MetricCell} = [deepcopy(fleaves)..., deepcopy(mleaves)...]
    u_bleaves::Array{MetricCell} = [deepcopy(mleaves)..., deepcopy(bleaves)...]
    if verbose println("start to construct global graph...") end
    
    global_graph = construct_global_graph_with_s_g(start, goal, fmleaves)
    if verbose println("start to initialize lpa star") end
    m_g::MetricGraph, s::Int, g::Int, pq, g_score, came_from, heuristic::Function, rhs = MetricGraphs.lp_astar_initialize(global_graph, start, goal)
   
    # # global_graph2 = construct_global_graph_with_s_g(start, goal, fleaves)
    # # m_g2::MetricGraph, s2::Int, g2::Int, pq2, g_score2, came_from2, heuristic2::Function, rhs2 = MetricGraphs.lp_astar_initialize(global_graph2, start, goal)
    # fpath_dis::Union{Nothing,Float64} = 12
    counter = 0
    # fmpath::Array{Point}, fmpath_dis::Union{Nothing,Float64} = MetricGraphs.lp_astar_compute_shortest_path(m_g, s, g, pq, g_score, came_from, heuristic, rhs)
    # ax1 = plot_env_and_paths(fmleaves; bleaves=bleaves, path=fmpath, if_plot_env=true, if_plot_path=true, type="lower/", name = "Lower bound", plot_obstacle=false)

    for i in 1:12

        counter += 1
        if verbose
            println("------------------------------------------------------------------------")
            println("counter is: ", counter)
        end


        if verbose println("start to compute shortest path") end
        fmpath, fmpath_dis = opt ?  MetricGraphs.lp_astar_compute_shortest_path_opt(m_g, s, g, pq, g_score, came_from, heuristic, rhs) :
        MetricGraphs.lp_astar_compute_shortest_path(m_g, s, g, pq, g_score, came_from, heuristic, rhs)

        if verbose println("path is: ", fmpath) end
        if ifplot
            fig, axes = PyPlot.subplots(1,2)
            axes[1].set_aspect("equal")
            axes[2].set_aspect("equal")
            ax1 = plot_env_and_paths(fmleaves; ax=axes[1], bleaves=bleaves, path=fmpath, if_plot_env=true, if_plot_path=true, type="lower/", name = "Lower bound", plot_obstacle=false, save=false)
            plot(space, fmpath; ax=axes[2]) #, filename="testRScar"
        end
        # PyPlot.savefig("data/output/images/arm-lower/"*string("2DArm",counter))

        # fpath::Array{Point} =  MetricGraphs.lp_astar_compute_shortest_path(m_g2, s2, g2, pq2, g_score2, came_from2, heuristic2, rhs2)
        # fpath_dis = distance(m_g2, fpath)

        # plot(m_g)

        # ax2 = plot_env_and_paths(fleaves; bleaves=u_bleaves, path=fpath, if_plot_env=true, if_plot_path=true, type="upper/", name = "Upper bound")

        # plot_upper_and_lower(fleaves, fmleaves, name1="Upper bound", name2="Lower bound", path1=fpath, path2=fmpath, serial=counter)

        mvisited = label_visited_mix_cells(fmpath, fmleaves)
        if verbose  println("num of visited: ", length(mvisited)) end
        ccc = 0

        for cell in mvisited
            temp_fleaves::Array{MetricCell}, temp_mleaves::Array{MetricCell} = [], []
            ccc += 1
            target_point_array::Array{Point}, current_contain_both = check_contain_start_and_goal(cell, start, goal)
            if opt
                update_cell_graph_blocked_opt(cell, m_g, s, g_score, rhs, pq, heuristic, came_from; target_point_array=target_point_array)
            else
                outer_points = update_cell_graph_blocked(cell, m_g; target_point_array=target_point_array) # set all edge to inf
                for p in outer_points  ##!! Do we need this?
                    v = get_vertex(m_g, p)       
                    updateNode(v,s, g_score, rhs, pq, heuristic, m_g, came_from)
                end
            end


            side_length = max_side_size(cell)
            epsilon = side_length/2.0
            temp_fleaves, temp_mleaves, temp_bleaves = construct!(space, cell; epsilon=epsilon, delta=delta)
            new_leaves::Array{MetricCell} = [deepcopy(temp_fleaves)..., deepcopy(temp_mleaves)...]
            
            
            for new_leaf in new_leaves
                target_point_array, current_contain_both = check_contain_start_and_goal(new_leaf, start, goal)
                if opt
                    update_cell_graph_opt(new_leaf, m_g, s, g_score, rhs, pq, heuristic, came_from; target_point_array=target_point_array)
                else
                    temp_new_points::Array{Point}, temp_old_points::Array{Point} = update_cell_graph(new_leaf, m_g, target_point_array=target_point_array)

                    for p in temp_old_points
                        v = get_vertex(m_g, p)
                        updateNode(v,s, g_score, rhs, pq, heuristic, m_g, came_from)
                    end

                    for p in temp_new_points
                        v = get_vertex(m_g, p)
                        updateNode(v,s, g_score, rhs, pq, heuristic, m_g, came_from)
                    end
                end
 
            end

            append!(fleaves, deepcopy(temp_fleaves))
            filter!(e->e≠cell,fmleaves) # delete cell that was divided
            append!(fmleaves, deepcopy(temp_fleaves))
            append!(fmleaves, deepcopy(temp_mleaves)) 
            append!(bleaves, deepcopy(temp_bleaves))
            filter!(e->e≠cell,u_bleaves)
            append!(u_bleaves, deepcopy(temp_mleaves))
            append!(u_bleaves, deepcopy(temp_bleaves))
            println("num of free cells: ", length(fleaves))
            println("num of blocked cells: ", length(bleaves))

           
            # fmpath = MetricGraphs.lp_astar_compute_shortest_path(m_g, s, g, pq, g_score, came_from, heuristic, rhs)
            # for new_leaf in temp_fleaves
            #     target_point_array, current_contain_both = check_contain_start_and_goal(new_leaf, start, goal)
            #     temp_new_points2::Array{Point}, temp_old_points2::Array{Point} = update_cell_graph(new_leaf, m_g2, target_point_array=target_point_array)
            #     ### current new points will be old points in next round
            #     for p in temp_old_points2
            #         v2 = get_vertex(m_g2, p)
            #         updateNode(v2,s2, g_score2, rhs2, pq2, heuristic2, m_g2, came_from2)
            #     end
            #     for p in temp_new_points2
            #         v2 = get_vertex(m_g2, p)
            #         updateNode(v2,s2, g_score2, rhs2, pq2, heuristic2, m_g2, came_from2)
            #     end
            # end



        end



    

    end
    

end



# @time @suppress test2DArm()

@time testRScar()

# testprm2D()
# testprm3D()

# @suppress begin testscdm3D() end




# video3()