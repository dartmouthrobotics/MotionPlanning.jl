#=
 * File: hc.jl
 * Project: rg
 * File Created: Saturday, 11th January 2020 12:47:10 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 26th February 2020 3:09:19 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#
import PyPlot
using MetricTools
using MetricSpaces, MetricSpaces.Planar, MetricSpaces.PLRs
using MetricRegressors

if false    # fix linter
    include("../../src/MotionPlanning.jl")
    using
        .MotionPlanning,
        .MotionPlanning.RegressionCells,
        .MotionPlanning.RecursiveCellRoadmaps,
        .MotionPlanning.RegressionGraphs,
        .MotionPlanning.VisibilityGraphs
else
    using
        MotionPlanning,
        MotionPlanning.RegressionCells,
        MotionPlanning.RecursiveCellRoadmaps,
        MotionPlanning.RegressionGraphs,
        MotionPlanning.VisibilityGraphs
end

function test_rcrm()
    space::Space = PlanarSpace(;
        bounds=[Bound(0, 1), Bound(0, 1)],
        filename="data/input/spaces/planar/world-maze.json")
        
    vg::VisibilityGraph = VisibilityGraph(space)
    rcrm::RecursiveCellRoadmap = RecursiveCellRoadmap(
        space;
        boundstep=1/32,
        maxdepth=10)
    PyPlot.gca().set_aspect("equal")
    plot(rcrm)
    
    start1::Point = [0.49, 0.49]
    # start1::Point = [0., 0.]
    goal1::Point = [1., 1.]
    # goal1::Point = [0.1875, 0.5]
    path1::Array{Point} = plan(rcrm, start1, goal1)
    dist1 = metric(rcrm, path1)

    plot(path1; ispath=true, color="red")
    display(path1)
    println(dist1)

    start2::Point = [0., 1.]
    goal2::Point = [1., 0.]
    path2::Array{Point} = plan(rcrm, start2, goal2)
    dist2 = metric(rcrm, path2)
    
    plot(path2; ispath=true, color="red")
    display(path2)
    println(dist2)
end

test_rcrm()
