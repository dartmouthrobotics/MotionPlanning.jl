#=
 * File: regressor.jl
 * Project: rg
 * File Created: Tuesday, 4th February 2020 9:25:05 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 26th February 2020 3:09:19 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using MetricTools
using MetricSpaces, MetricSpaces.Planar
using MetricRegressors

include("./cellplanners.jl")

function regressallpairs()
    subspace::Space = PlanarSpace()
    step::Float64 = 1/64
    rc::RegressionCell = RegressionCell(
        subspace;
        cellplanner=vg_cellplanner(subspace),
        # cellplanner=prm_cellplanner(subspace; step=step/1, n=250),
        # cellplanner=rcrm_cellplanner(subspace; step=step/4),
        # reconstructor=() -> prm_cellplanner(subspace; step=step/1, n=250, plotprm=false),
        # regressor=XGBoostRegressor(; epochs=12, eta=1, maxdepth=15),
        regressor=PLR(
            subspace;
            maxdepth=15,
            maxerror=0.0000001,
            minsamples=subspace.dim * 2 + 2),
        # classifier=XGBoostRegressor(; epochs=20, eta=1, maxdepth=10),
        classifier=PLR(
            subspace;
            maxdepth=15,
            maxerror=0.0000001,
            minsamples=subspace.dim * 2 + 2),
        step=step)

    println("Done.")
end

regressallpairs()
