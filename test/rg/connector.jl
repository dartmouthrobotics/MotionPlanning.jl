#=
 * File: connector.jl
 * Project: rg
 * File Created: Monday, 24th February 2020 11:42:22 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 26th February 2020 3:09:19 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using MetricTools
using MetricSpaces, MetricSpaces.Planar, MetricSpaces.PLRs

if false    # fix linter
    include("../../src/MotionPlanning.jl")
    using
        .MotionPlanning,
        .MotionPlanning.Connectors
else
    using
        MotionPlanning,
        MotionPlanning.Connectors
end

function testconnectors()
    println("Testing connectors...")
    space::Space = PlanarSpace(filename="data/input/spaces/planar/world1.json")
    start::Point = [1, 1]
    goal::Point = [0, 0]
    isreachable::Bool = reachable(space, start, goal; maxattempts=100)
    show(isreachable)
end

testconnectors()
