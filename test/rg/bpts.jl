#=
 * File: bpts.jl
 * Project: rg
 * File Created: Monday, 20th January 2020 8:36:50 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 1st March 2020 10:32:54 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using MetricTools
using
    MotionPlanning,
    MotionPlanning.SampleCells

function test_bpts()
    bounds::Array{Bound} = [Bound(0, 1), Bound(0, 1), Bound(0, 1), Bound(0, 1)]
    bpts::Array{Point} = boundary_points(bounds; step=1/6)
    println(length(bpts))
    display(bpts)
end

test_bpts()
