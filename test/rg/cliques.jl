#=
 * File: cliques.jl
 * Project: rg
 * File Created: Friday, 20th March 2020 3:11:15 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Friday, 20th March 2020 8:53:27 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

import PyPlot
using ProgressMeter
using Polyhedra
using QHull

using MetricTools, MetricTools.Geometry
using MetricSpaces, MetricSpaces.Samplers

using MetricSpaces.Planar

# Utilities
@inline validpolyhedron(p::Polygon)::Bool = length(p) > length(first(p))
@inline topolyhedron(p::Polygon)::Polyhedron = polyhedron(vrep(hcat(p...)'), QHull.Library())
# @inline topolyhedron(p::Polygon)::Polyhedron = polyhedron(vrep(hcat(p...)'))
@inline topointlist(p::Polyhedron)::Polygon = [pt for pt in eachslice(vrep(p).V; dims=1)]

"""
Struct for a clique decomposition
"""
mutable struct CliqueDecomposition
    space::Space
    cliques::Array{Polygon}
    polyhedra::Array{Union{Polyhedron}}
    maxdist::Float64

    function CliqueDecomposition(space::Space;
            n::Integer=100, maxdist::Float64=1., maxattempts::Integer=100)
        cd::CliqueDecomposition = new(space, Polygon[], Polyhedron[], maxdist)
        construct!(cd, n; maxattempts=maxattempts)
        return cd
    end
end

function MetricTools.plot(cd::CliqueDecomposition, i::Integer)
    colors::Array{String} = [
        "red", "purple", "indigo", "blue", "green", "yellow", "orange"
    ]
    color = colors[i % length(colors) + 1]
    polys::Array{Polygon} = [cd.cliques[i]]
    plot(polys; facecolor=color, alpha=0.3, edgecolor=color, points=true)
end

function MetricTools.plot(cd::CliqueDecomposition)
    PyPlot.clf()
    plot(cd.space)
    
    for i in eachindex(cd.cliques)
        plot(cd, i)
    end
end

function find(cd::CliqueDecomposition, point::Point)::Integer
    for i in eachindex(cd.polyhedra)
        if !validpolyhedron(cd.cliques[i]) continue end
        if in(point, hrep(cd.polyhedra[i]))
            println("$point was inside polyhedra $(cd.cliques[i])")
            return i
        end
    end
    
    return 0
end

function delete_invalid!(cd::CliqueDecomposition, point::Point, i::Integer)
    if !isfree(cd.space, point)
        println("Point $point wasn't free but was in clique $i. Deleting...")
        deleteat!(cd.cliques, i)
        deleteat!(cd.polyhedra, i)
    end
end

"""
Gets a free point not inside of a clique
If we find a point inside of a previous clique and it's not free, delete that clique
"""
function sample_point(cd::CliqueDecomposition;
        maxattempts::Integer=10)::Point
    for _ in 1:maxattempts
        point::Point = sample_inbounds(cd.space)
        i::Integer = find(cd, point)
        if i > 0
            # delete_invalid!(cd, point, i)
        elseif isfree(cd.space, point)
            return point
        end
    end
    
    # Failed to find a point
    return []
end

function all_connect(cd::CliqueDecomposition, clique::Polygon, point::Point)
    for cpoint in clique
        if !(localplanner(cd.space, point, cpoint) &&
                metric(cd.space, point, cpoint) < cd.maxdist)
            return false
        end
    end

    return true
end

function has_intersect(p1::Polyhedron, p2::Polyhedron)::Bool
    p3 = intersect(p1, p2)
    intersection::Polygon = topointlist(p3)
    println(intersection)
    return !isempty(intersection) && validpolyhedron(intersection)
end

function intersects_any(cd::CliqueDecomposition, i::Integer)::Bool
    # if !validpolyhedron(cd.cliques[i]) return false end
    for j in eachindex(cd.polyhedra)
        if i == j continue end
        # if !validpolyhedron(cd.cliques[j]) continue end

        if has_intersect(cd.polyhedra[i], cd.polyhedra[j]) return true end
    end

    return false
end

function attempt_join!(cd::CliqueDecomposition, point::Point)::Bool
    anyadded::Bool = false

    # try to add to all cliques
    for i in eachindex(cd.cliques)

        # if can join the clique
        if all_connect(cd, cd.cliques[i], point)
            anyadded = true
            
            # add this point to clique
            push!(cd.cliques[i], point)
            cd.polyhedra[i] = topolyhedron(cd.cliques[i])
            
            # if enough points to be redundant
            if validpolyhedron(cd.cliques[i])
                # make sure we didn't cross any previous cells
                # if intersects_any(cd, i)
                #     pop!(cd.cliques[i])
                #     cd.polyhedra[i] = topolyhedron(cd.cliques[i])
                #     continue
                # end
                
                # remove any redundant points
                removevredundancy!(cd.polyhedra[i])
                cd.cliques[i] = topointlist(cd.polyhedra[i])
            end
        end
    end

    return anyadded
end

"""
Creates a new clique
We know that point cannot be added to any other cliques
Try to connect all previous points to the clique
"""
function new_clique!(cd::CliqueDecomposition, point::Point)
    new_clique::Polygon = [point]
    for clique in cd.cliques
        for cpoint in clique
            if all_connect(cd, new_clique, cpoint)
                push!(new_clique, cpoint)
            end
        end
    end

    push!(cd.cliques, new_clique)
    push!(cd.polyhedra, topolyhedron(new_clique))
end

"""
Constructs the clique decomposition
Repeatedly add new samples.
First try to have samples join previous cliques. 
"""
function construct!(cd::CliqueDecomposition, n::Integer;
        maxattempts::Integer=100)

    # plot(cd)
    
    @showprogress for i in 1:n
        point::Point = sample_point(cd)
        if isempty(point) continue end
        
        anyadded::Bool = attempt_join!(cd, point)
        if !anyadded new_clique!(cd, point) end

        # verbose settings
        plot(cd)
        if readline() == "i" display(cd.cliques) end
    end
end

function test_growing_cliques()
    space::Space = PlanarSpace(; filename="data/input/spaces/planar/world1.json")
    cd::CliqueDecomposition = CliqueDecomposition(space; n=100)
    plot(cd)
end

function test_intersect()
    polygon1::Polygon = [
        [0.4, 0.4],
        [0.4, 0.6],
        [0.6, 0.6],
        [0.6, 0.4]
    ]
    polygon2::Polygon = [
        [0.5, 0.4],
        [0.6, 0.6],
        [0.8, 0.6],
        [0.8, 0.4]
    ]
    plot(Polygon[polygon1, polygon2]; alpha=0.5)
    p1 = topolyhedron(polygon1)
    p2 = topolyhedron(polygon2)
    print(has_intersect(p1, p2))
end

test_growing_cliques()
# test_intersect()
