#=
 * File: arms.jl
 * Project: rg
 * File Created: Monday, 20th January 2020 8:39:01 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 26th April 2020 9:18:21 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

import PyPlot
using MetricTools, MetricTools.MetricGraphs
using
    MetricSpaces,
    MetricSpaces.ReedsShepp,
    MetricSpaces.PLRs
using MetricRegressors

using
    MotionPlanning,
    MotionPlanning.RecursiveCellRoadmaps,
    MotionPlanning.RegressionGraphs,
    MotionPlanning.VisibilityGraphs,
    MotionPlanning.PRMs

include("./cellplanners.jl")

function test_rg_rs()
    space::ReedsSheppSpace = ReedsSheppSpace(;
        filename="data/input/spaces/planar/angled-doors.json",
        r=0.5
    )

    start::Point = [0, 0, 0]
    goal::Point = [0.9, 0.9, pi/2]

    steps::Array{Float64} = [1/8, 1/8, pi/4]

    # define the regression graph, give it the needed factories
    rg::RegressionGraph = @time RegressionGraph(space;
        sizes=[1/4, 1/4, pi/2],
        cellfactory=(subspace) -> RegressionCell(
            subspace;
            # cellplanner=vg_cellplanner(subspace),
            cellplanner=prm_cellplanner(subspace; n=1000, steps=steps, kfactor=10.),
            reconstructor=() -> prm_cellplanner(subspace; n=1000, steps=steps, kfactor=10.),
            # cellplanner=rcrm_cellplanner(subspace; step=step/1),
            regressor=PLR(
                vcat(subspace.bounds, subspace.bounds);
                maxdepth=20,
                maxerror=0.0000001),
            # classifier=XGBoostRegressor(; epochs=20, eta=1, maxdepth=20),
            classifier=PLR(
                vcat(subspace.bounds, subspace.bounds);
                maxdepth=20,
                maxerror=0.0000001),
            steps=steps))
    
    path::Array{Point} = @time plan(rg, start, goal)
    dist = metric(rg, path)
    
    println("path =")
    print(path)
    println("dist = ", dist)
    plot(space, path)

    println("Total_FP of RC is: ", getnumparams(rg))
    PyPlot.xlim(-0.1, 1.1)
    PyPlot.ylim(-0.1, 1.1)
    PyPlot.gca().set_aspect("equal")
    println("Done.")
end

function plot_both_paths()
    space::ReedsSheppSpace = ReedsSheppSpace(;
        filename="data/input/spaces/planar/angled-doors.json",
        r=0.5
    )
    rg_path::Array{Point} = [[0.8999995342233655, 0.9000136018960292, 1.5707841111226972], [0.8859467625455794, 0.8613163324470462, 1.2874889800695881], [0.7774579603182856, 0.6129652979014973, 1.0280093283064886], [0.5926454699644421, 0.3134129671968918, 1.1221081513419155], [0.5639789294918193, 0.2684000395514817, 1.396531871609353], [0.5679658932067174, 0.6824281613580485, 1.5055700877827778], [0.5419507069288861, 0.7454959620032262, 1.0071422839358612], [0.3580426457676608, 0.5952969827789455, 0.6281416423524926], [0.16316289077767618, 0.4439846138389784, 0.9199448933794871], [0.162458694369672, 0.35292450445496265, 0.8537140663222502], [0.16660413517906836, 0.2813082325643948, 0.5901031516708586], [0.17188733853924704, 0.17188733853924704, 0.3000000000000001], [0.0, 0.0, 0.0]]
    prm_path::Array{Point} = [[0.0, 0.0, 0.0], [0.15238144823405375, 0.009584515809801797, 0.033904631992318635], [0.13297403483914572, 0.00821499603807152, 0.35036732301655027], [0.03383129623990366, 0.020674461885414663, 0.934210819130822], [0.18206264694722551, 0.44754164271021657, 1.4788593830030363], [0.1730832456878395, 0.5395195447380423, 1.8549345422631491], [0.07172073809565704, 0.7694953291945112, 2.16026057265939], [0.20076891414736076, 0.6140051368843236, 2.4350082320045017], [0.5730653781238737, 0.4013921898234527, 2.7477183312625266], [0.9670719868110187, 0.22213867816617539, 2.476997415276575], [0.9985219353423522, 0.29186390312443633, 1.941912149403266], [0.9161971906678104, 0.694588307900764, 1.753039362557491], [0.9, 0.9, 1.5707963267948966]]
    plot(space, rg_path; color="red")
    plot(space, prm_path; color="blue")
    PyPlot.xlim(-0.2, 1.1)
    PyPlot.ylim(-0.2, 1.1)
    PyPlot.gca().set_aspect("equal")

    println("metric cost rg: ", metric(space, rg_path))
    println("metric cost prm: ", metric(space, prm_path))
    println("Done.")
end

# test_rg_rs()
plot_both_paths()
