#=
 * File: arms.jl
 * Project: rg
 * File Created: Monday, 20th January 2020 8:39:01 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 1st March 2020 5:43:13 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using MetricTools, MetricTools.MetricGraphs
using
    MetricSpaces,
    MetricSpaces.Planar,
    MetricSpaces.Arms,
    MetricSpaces.PLRs
using MetricRegressors

using
    MotionPlanning,
    MotionPlanning.RecursiveCellRoadmaps,
    MotionPlanning.RegressionGraphs,
    MotionPlanning.VisibilityGraphs,
    MotionPlanning.PRMs

include("./cellplanners.jl")

function test_rg_arms2D()
    space::ArmSpace = ArmSpace(
        "data/input/spaces/arms/arms2.json", 
        "data/input/spaces/planar/world2.json"
    )

    start::Point = [0, 0]
    goal::Point = [pi/2, 0]

    step::Float64 = pi/16
    
    # define the regression graph, give it the needed factories
    rg::RegressionGraph = @time RegressionGraph(space;
        sizes=[pi/2, pi/2],
        cellfactory=(subspace) -> RegressionCell(
            subspace;
            # cellplanner=vg_cellplanner(subspace),
            cellplanner=prm_cellplanner(subspace; step=step/1, n=300),
            # cellplanner=rcrm_cellplanner(subspace; step=step/1, n=300),
            regressor=PLR(
                vcat(subspace.bounds, subspace.bounds);
                maxdepth=15,
                maxerror=0.0000001),
            # classifier=XGBoostRegressor(; epochs=20, eta=1, maxdepth=20),
            classifier=PLR(
                vcat(subspace.bounds, subspace.bounds);
                maxdepth=15,
                maxerror=0.0000001),
            step=step))
    
    path::Array{Point} = @time plan(rg, start, goal)
    dist = metric(rg, path)
    
    println("path =")
    display(path)
    println("dist = ", dist)
    plot(space, path; filename="rg-arms2D")
end

function test_rg_arms3D()
    space::ArmSpace = ArmSpace(
        "data/input/spaces/arms/arms3.json", 
        "data/input/spaces/planar/world4.json")

    start::Point = [0, 0, 0]
    goal::Point = [pi/2, 0, 0]

    steps::Array{Float64} = [pi/8, pi/8, pi/8]
    
    # define the regression graph, give it the needed factories
    rg::RegressionGraph = @time RegressionGraph(space;
        sizes=Float64[pi, pi, pi],
        cellfactory=(subspace) -> RegressionCell(
            subspace;
            cellplanner=prm_cellplanner(subspace; steps=steps, n=600),
            reconstructor=() -> prm_cellplanner(subspace; steps=steps, n=600, plotprm=false),
            # cellplanner=rcrm_cellplanner(subspace; step=step/1),
            regressor=PLR(
                vcat(subspace.bounds, subspace.bounds);
                maxdepth=20,
                maxerror=0.0000001),
            # classifier=XGBoostRegressor(; epochs=20, eta=1, maxdepth=20),
            classifier=PLR(
                vcat(subspace.bounds, subspace.bounds);
                maxdepth=20,
                maxerror=0.0000001),
            steps=steps))
    
    getnumparams(rg)
    path::Array{Point} = @time plan(rg, start, goal)
    dist = metric(rg, path)
    
    println("path =")
    display(path)
    println("dist = ", dist)
    plot(space, path; filename="rg-arms3D")
end


function test_rg_arms4D()
    space::ArmSpace = ArmSpace(
        "data/input/spaces/arms/arms4.json", 
        "data/input/spaces/planar/world2.json")

    start::Point = [0, 0, 0, 0]
    goal::Point = [pi/2, 0, 0, 0]

    step::Float64 = pi/6
    
    # define the regression graph, give it the needed factories
    rg::RegressionGraph = @time RegressionGraph(space;
        sizes=Float64[pi, pi, pi, pi],
        cellfactory=(subspace) -> RegressionCell(
            subspace;
            cellplanner=prm_cellplanner(subspace; step=step/1, n=500),
            # cellplanner=rcrm_cellplanner(subspace; step=step/1),
            reconstructor=() -> prm_cellplanner(subspace; step=step/1, n=500, plotprm=false),
            regressor=PLR(
                vcat(subspace.bounds, subspace.bounds);
                maxdepth=20,
                maxerror=0.0000001),
            # classifier=XGBoostRegressor(; epochs=20, eta=1, maxdepth=20),
            classifier=PLR(
                vcat(subspace.bounds, subspace.bounds);
                maxdepth=20,
                maxerror=0.0000001),
            step=step))
    
    path::Array{Point} = @time plan(rg, start, goal)
    dist = metric(rg, path)
    
    println("path =")
    display(path)
    println("dist = ", dist)
    plot(space, path; filename="rg-arms4D")
    getnumparams(rg)
end

# test_rg_arms2D()
test_rg_arms3D()
# test_rg_arms4D()
