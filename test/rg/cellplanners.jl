#=
 * File: cellplanners.jl
 * Project: rg
 * File Created: Monday, 20th January 2020 8:33:03 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 1st March 2020 5:10:07 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using MetricTools
using MetricSpaces, MetricSpaces.PLRs

if false    # fix linter
    include("../../src/MotionPlanning.jl")
    using
        .MotionPlanning,
        .MotionPlanning.RecursiveCellRoadmaps,
        .MotionPlanning.RegressionGraphs,
        .MotionPlanning.VisibilityGraphs,
        .MotionPlanning.PRMs
else
    using
        MotionPlanning,
        MotionPlanning.RecursiveCellRoadmaps,
        MotionPlanning.RegressionGraphs,
        MotionPlanning.VisibilityGraphs,
        MotionPlanning.PRMs
end

function rplr_regactory(space::Space)::Regressor
    return PLR(
        space;
        maxdepth=10,
        maxerror=0.0000001,
        minsamples=space.dim * 2 + 2)
end

function vg_cellplanner(space::Space)::Function
    vg::VisibilityGraph = VisibilityGraph(space)
    println("vg bounds: ", vg.space.bounds)
    return (p1::Point, p2::Point) -> plan(vg, p1, p2) 
end

function prm_cellplanner(space::Space;
        n::Integer=1500,
        step::Float64=1/16,
        steps::Array{Float64}=[],
        plotprm::Bool=false,
        kfactor::Float64=1.)::Function
    prm::PRM = PRM(space; k=round(Int, kfactor * log2(n)))
    grow!(prm, n)
    
    if plotprm plot(prm) end
    if isempty(steps) steps = [step for i in eachindex(space.bounds)] end
    return precompdijkstras(space, prm; steps=steps)
end

function rcrm_cellplanner(space::Space;
        step::Float64=1/32,
        steps::Array{Float64}=[],
        maxdepth::Integer=10)::Function
    if isempty(steps) steps = [step for i in eachindex(space.bounds)] end
    rcrm::RecursiveCellRoadmap = RecursiveCellRoadmap(space;
        boundstep=step,
        maxdepth=maxdepth)
    # plot(rcrm)
    # return (p1::Point, p2::Point) -> plan(rcrm, p1, p2)

    return precompdijkstras(space, rcrm; steps=steps)
end
