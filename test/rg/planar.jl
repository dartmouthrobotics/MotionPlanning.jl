#=
 * File: rc.jl
 * Project: test
 * File Created: Friday, 10th January 2020 10:32:16 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Saturday, 29th February 2020 11:44:59 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

import PyPlot
using Suppressor
using MetricTools, MetricTools.MetricGraphs
using
    MetricSpaces,
    MetricSpaces.Planar,
    MetricSpaces.Arms,
    MetricSpaces.PLRs
using MetricRegressors

using
    MotionPlanning,
    MotionPlanning.RecursiveCellRoadmaps,
    MotionPlanning.RegressionGraphs,
    MotionPlanning.VisibilityGraphs,
    MotionPlanning.PRMs

include("./cellplanners.jl")


function test_rg_planar()
    space::Space = PlanarSpace(;
        # filename="data/input/spaces/planar/world1.json")
        filename="data/input/spaces/planar/world-doors.json")
    
    step::Float64 = 1/64
    # define the regression graph, give it the needed factories
    rg::RegressionGraph = RegressionGraph(space;
        verbose=true,
        sizes=[1/8, 1/8],
        cellfactory=(subspace) -> RegressionCell(
            subspace;
            # cellplanner=vg_cellplanner(subspace),
            cellplanner=prm_cellplanner(subspace; step=step/1, n=250),
            # cellplanner=rcrm_cellplanner(subspace; step=step/4),
            reconstructor=() -> prm_cellplanner(subspace; step=step/1, n=250, plotprm=true),
            # regressor=XGBoostRegressor(; epochs=12, eta=1, maxdepth=15),
            regressor=PLR(
                vcat(subspace.bounds, subspace.bounds);
                maxdepth=20,
                maxerror=0.0000001),
            # classifier=XGBoostRegressor(; epochs=20, eta=1, maxdepth=20),
            classifier=PLR(
                vcat(subspace.bounds, subspace.bounds);
                maxdepth=20,
                maxerror=0.0000001),
            step=step))
    
    start::Point = [0., 0.]
    goal::Point = [1., 1.]
    path::Array{Point} = plan(rg, start, goal)
    dist::Union{Nothing, Float64} = metric(rg, path)
    
    
    println("memcost: ", getnumparams(rg))
    println("path =")
    display(path)
    println("dist = ", dist)
    plot(rg)
    plot(path; ispath=true, color="blue")
end

function test_rg_maze()
    space::Space = PlanarSpace(;
        filename="data/input/spaces/planar/world-maze.json")
    
    step::Float64 = 1/128
    
    # define the regression graph, give it the needed factories
    rg::RegressionGraph = @time RegressionGraph(space;
        sizes=[0.5, 0.5],
        cellfactory=(subspace) -> RegressionCell(
            subspace;
            cellplanner=vg_cellplanner(subspace),
            # cellplanner=prm_cellplanner(subspace; n=1500, step=step),
            # cellplanner=rcrm_cellplanner(subspace; step=step/1),
            # regressor=XGBoostRegressor(; epochs=12, eta=1, maxdepth=10),
            regressor=PLR(
                vcat(subspace.bounds, subspace.bounds);
                maxdepth=30,
                maxerror=0.00000001),
            classifier=PLR(
                vcat(subspace.bounds, subspace.bounds);
                maxdepth=30,
                maxerror=0.00000001),
            step=step))
    
    start::Point = [0., 0.]
    goal::Point = [0.49, 0.49]
    path::Array{Point} = @time plan(rg, start, goal)
    dist = metric(rg, path)
    
    println("path =")
    display(path)
    println("dist = ", dist)
    plot(rg)
    plot(path; ispath=true, color="blue")
    PyPlot.gca().set_aspect("equal")
end

test_rg_planar()
# test_rg_maze()
