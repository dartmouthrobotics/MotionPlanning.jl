using Test, Revise
import PyPlot, MetaGraphs, PyCall
using StatsBase
using Suppressor
using MetricTools, MetricSpaces.Planar, MetricTools.MetricGraphs
using JLD
using
    MotionPlanning,
    # MotionPlanning.VisibilityGraphs
    MotionPlanning.MetricCells

function get_num_samples_per_edge_for_current_cell(; current_depth=0, max_cell_depth=7 ,n_pts_per_edge_for_multi_reso = 3)
    function get_num_samples_per_edge_for_current_cell_helper(d)
        if d == max_cell_depth
            return n_pts_per_edge_for_multi_reso
        else
            return 2 * get_num_samples_per_edge_for_current_cell_helper(d+1) - 1
        end
    end
    get_num_samples_per_edge_for_current_cell_helper(current_depth)
end

function test(space::PlanarSpace, 
                start::Point, 
                goal::Point, 
                delta::Float64, 
                epsilon::Float64; 
                verbose::Bool=true,  
                ifplot::Bool=true,
                use_upper_change=false,
                use_lower_change=false,
                block_mleaves_by_prob=false,
                method::Integer=1)

    n_pts::Int64 = get_num_samples_per_edge_for_current_cell(;current_depth=0, max_cell_depth=8,n_pts_per_edge_for_multi_reso = 3)
    root_cell::MetricCell = MetricCell(space, space.bounds,0; n_pts=n_pts)
    fleaves::Array{MetricCell}, mleaves::Array{MetricCell}, bleaves::Array{MetricCell} = construct!(space, root_cell; epsilon=epsilon, delta=delta)

    ##! Use free_mleaves for upper change case
    (free_mleaves::Array{MetricCell}, n_free_mleaves::Array{MetricCell}) = use_upper_change ? classify_mcells(mleaves) : ([],[])

    ##! Use block_mleaves for lower change case/ for block partial mix cells by probability
    (block_mleaves::Array{MetricCell}, n_block_mleaves::Array{MetricCell}) = use_lower_change ? classify_mcells2(mleaves) : ([],[])

    if block_mleaves_by_prob
        mleaves_prob::Array{Float64} = [leaf.block_prob for leaf in mleaves]
        block_mleaves = unique(sample(mleaves, Weights(mleaves_prob), round(Int, length(mleaves)*0.3), replace=true))
        n_block_mleaves =  [c for c in mleaves if !(c in block_mleaves)]
    end
    
    fmleaves::Array{MetricCell} = (use_lower_change||block_mleaves_by_prob) ? [fleaves..., n_block_mleaves...] : [fleaves..., mleaves...]
    u_bleaves::Array{MetricCell} = [mleaves..., bleaves...]

    global_graph = construct_global_graph_with_s_g(start, goal, fmleaves)
    m_g::MetricGraph, s::Int, g::Int, pq, g_score, came_from, heuristic::Function, rhs = MetricGraphs.lp_astar_initialize(global_graph, start, goal)

    fleaves_with_free_mleaves::Array{MetricCell} = use_upper_change ? [fleaves..., free_mleaves...] : [fleaves...]


    global_graph2 = construct_global_graph_with_s_g(start, goal, fleaves_with_free_mleaves)
    m_g2::MetricGraph, s2::Int, g2::Int, pq2, g_score2, came_from2, heuristic2::Function, rhs2 = MetricGraphs.lp_astar_initialize(global_graph2, start, goal)
    fmpath::Array{Point}, fpath::Array{Point} = [], []
    fpath_dis::Union{Nothing,Float64}, fmpath_dis::Union{Nothing, Float64} = Inf, Inf
    mvisited_upper::Array{MetricCell}, mvisited::Array{MetricCell} = [], []
    counter = 0
    # while fpath_dis == Inf || fpath_dis != fmpath_dis
    was_changed = false
    temp_bbbb_cells::Array{MetricCell} = []
    while true

    # for i in 1:6
        counter += 1

        fmpath, fmpath_dis = MetricGraphs.lp_astar_compute_shortest_path_opt(m_g, s, g, pq, g_score, came_from, heuristic, rhs) 

        fpath, fpath_dis = MetricGraphs.lp_astar_compute_shortest_path_opt(m_g2, s2, g2, pq2, g_score2, came_from2, heuristic2, rhs2) 
        # fig, ax = PyPlot.subplots()
        # ax1 = plot_env_and_paths(;free_leaves=fleaves, bleaves=bleaves, mleaves=mleaves, ax=axes[1], path=fmpath, if_plot_env=true, if_plot_path=true, type="lower/", name = "Lower bound", plot_obstacle=false, save=false)
        # ax2 = plot_env_and_paths(;free_leaves=fleaves, bleaves=bleaves, mleaves=mleaves, ax=axes[2], path=fpath, if_plot_env=true, if_plot_path=true, type="upper/", name = "Upper bound", plot_obstacle=false, save=false)

        if verbose 
            println("------------------------------------------------------------------------")
            println("counter is: ", counter)
        end

        if verbose
            println("the fpath dis is: ", fpath_dis)
            println("the fmpath dis is: ", fmpath_dis)
            println("the difference is: ", fmpath_dis - fpath_dis)
        end

        if ifplot && counter > 0
            plot_upper_and_lower(fleaves, fmleaves, name1="Upper bound", name2="Lower bound", path1=fpath, path2=fmpath, bleaves=bleaves, u_bleaves=u_bleaves, serial=counter)
        end
        mvisited = (use_lower_change||block_mleaves_by_prob) ?
                    (isempty(fmpath) ? block_mleaves : label_visited_mix_cells(fmpath, n_block_mleaves)) :
                    label_visited_mix_cells(fmpath, mleaves) ##n_block_mleaves for lower, other, mleaves/?? ##??? DO WE NEED FMLEAVES?? fmleaves
        ##! mvisited_upper is used for label mix_free cells that upper point traversed
        mvisited_upper = (!isempty(fpath) && use_upper_change) ? label_visited_mix_cells(fpath, free_mleaves; interpolate=true) : []

            
        if isempty(mvisited) 
            if verbose println("so early end? Lower case is already free") end
            break
        end
        if abs(fmpath_dis - fpath_dis) <= 0.01 && isempty(mvisited_upper)
            if verbose 
                println("early end for tiny difference") 
                println("Upper case is free, not know if lower case is free since mvisited is not empty")  end
            break  
        end
        ccc = 0
        combo_mvisited::Array{MetricCell} = [mvisited...] 
        for cell in mvisited_upper ##! add cells for upper case
            if !(cell in combo_mvisited) push!(combo_mvisited, cell) end
        end
        if method==1  ## choose 30% at beginning and the fix it.
            ###????? Made change here!!!
            # was_changed = false
            # temp_bbbb_cells = []
            
            for cell in combo_mvisited
            #     while !isempty(combo_mvisited)
            #         cell = popfirst!(combo_mvisited)
                temp_fleaves::Array{MetricCell}, temp_mleaves::Array{MetricCell} = [], []
                ccc += 1
                target_point_array::Array{Point}, current_contain_both = check_contain_start_and_goal(cell, start, goal)
                
                update_cell_graph_blocked_opt(cell, m_g, s, g_score, rhs, pq, heuristic, came_from; target_point_array=target_point_array)
                

                
                ### ??? add to test that whether this cell can affect the resulting path, if so, sub-divide, otherwise, just block it and continue
                fmpath_new, fmpath_dis_new = MetricGraphs.lp_astar_compute_shortest_path_opt(m_g, s, g, pq, g_score, came_from, heuristic, rhs) 
                println("the dis s: ", fmpath_dis_new)
                if abs(fmpath_dis_new-fmpath_dis) <= 0.01
                    println("less than")
                    println("just ignore it, put it into block cells")
             

                    push!(temp_bbbb_cells, cell)

                    filter!(e->e≠cell,mleaves)
                    push!(bleaves, cell)
                    if use_upper_change
                        filter!(e->e≠cell,free_mleaves) 
                        filter!(e->e≠cell,n_free_mleaves) 
                    end
                    if use_lower_change||block_mleaves_by_prob
                        filter!(e->e≠cell,block_mleaves)
                        filter!(e->e≠cell,n_block_mleaves)
                    end
                    fmleaves = (use_lower_change||block_mleaves_by_prob) ? [fleaves..., n_block_mleaves...] : [fleaves..., mleaves...]
                    u_bleaves = [mleaves..., bleaves...]
                    fleaves_with_free_mleaves = use_upper_change ? [fleaves..., free_mleaves...] : [fleaves...]
                    # plot_upper_and_lower(fleaves, fmleaves, name1="Upper bound-new", name2="Lower bound-new", path1=fpath, path2=fmpath_new, bleaves=bleaves, u_bleaves=u_bleaves, serial=counter)
                    ###??? Made change here
                    # combo_mvisited = label_visited_mix_cells(fmpath_new, mleaves)

                    ##!! Update path cost
                    fmpath_dis = fmpath_dis_new

                else
                    println("greater")
                    side_length = max_side_size(cell)
                    epsilon = side_length/2.0
                    temp_fleaves, temp_mleaves, temp_bleaves = construct!(space, cell; epsilon=epsilon, delta=delta)
                    (temp_free_mleaves::Array{MetricCell}, temp_n_free_mleaves::Array{MetricCell}) = use_upper_change ? classify_mcells(temp_mleaves) : ([],[])
                    (temp_block_mleaves::Array{MetricCell}, temp_n_block_mleaves::Array{MetricCell}) = use_lower_change ? classify_mcells2(temp_mleaves) : ([],[])

                    if block_mleaves_by_prob
                        temp_mleaves_prob::Array{Float64} = [leaf.block_prob for leaf in temp_mleaves]

                        block_number::Integer = round(Int, length(temp_mleaves)*0.3)
                        if block_number > 0
                            temp_block_mleaves = unique(sample(temp_mleaves, Weights(temp_mleaves_prob), block_number, replace=true))
                            temp_n_block_mleaves =  [c for c in temp_mleaves if !(c in temp_block_mleaves)]
                        end
                    end

                    ## For free points, need to update them

                    new_leaves::Array{MetricCell} = (use_lower_change||block_mleaves_by_prob) ? [temp_fleaves..., temp_n_block_mleaves...] : [temp_fleaves..., temp_mleaves...]

                    for new_leaf in [temp_fleaves..., temp_free_mleaves...]

                        target_point_array, current_contain_both = check_contain_start_and_goal(new_leaf, start, goal)
                        update_cell_graph_opt(new_leaf, m_g2, s2, g_score2, rhs2, pq2, heuristic2, came_from2; target_point_array=target_point_array)
                        
                    end
                    println("length of new leaves ", length(new_leaves))
                    for new_leaf in new_leaves
                        
                        target_point_array, current_contain_both = check_contain_start_and_goal(new_leaf, start, goal)
                        update_cell_graph_opt(new_leaf, m_g, s, g_score, rhs, pq, heuristic, came_from; target_point_array=target_point_array)
        
                    end


                    append!(fleaves, temp_fleaves)
                    filter!(e->e≠cell,mleaves)
                    append!(mleaves, temp_mleaves)
                    append!(bleaves, temp_bleaves)
                    if use_upper_change
                        filter!(e->e≠cell,free_mleaves) ##!!!!
                        free_mleaves = [free_mleaves..., temp_free_mleaves...]
                        filter!(e->e≠cell,n_free_mleaves) #!!!
                        n_free_mleaves = [n_free_mleaves..., temp_n_free_mleaves...]
                    end
                    if use_lower_change||block_mleaves_by_prob
                        filter!(e->e≠cell,block_mleaves)#!!!
                        block_mleaves = [block_mleaves..., temp_block_mleaves...]
                        filter!(e->e≠cell,n_block_mleaves)#!!! necessary to avoid duplicate
                        n_block_mleaves = [n_block_mleaves..., temp_n_block_mleaves...]
                    end
                    fmleaves = (use_lower_change||block_mleaves_by_prob) ? [fleaves..., n_block_mleaves...] : [fleaves..., mleaves...]
                    u_bleaves = [mleaves..., bleaves...]
                    fleaves_with_free_mleaves = use_upper_change ? [fleaves..., free_mleaves...] : [fleaves...]

                    fmpath_new, fmpath_dis_new = MetricGraphs.lp_astar_compute_shortest_path_opt(m_g, s, g, pq, g_score, came_from, heuristic, rhs) 
                    if abs(fmpath_dis_new-fmpath_dis) > 0.01
                        # was_changed = true
                    end

                    

                    # println("fleaves : ",length(fleaves)==length(unique(fleaves)))
                    # println("fmleaves : ",length(fmleaves)==length(unique(fmleaves)))
                    # println("bleaves : ",length(bleaves)==length(unique(bleaves)))
                    # println("u_bleaves : ",length(u_bleaves)==length(unique(u_bleaves)))
                    # println("fleaves_with_free_mleaves : ",length(fleaves_with_free_mleaves)==length(unique(fleaves_with_free_mleaves)))
                    # println("free_mleaves : ",length(free_mleaves)==length(unique(free_mleaves)))
                end
            end
            if was_changed
                println("path dist changed after sub-divide!!!!!!!")
                for cell in temp_bbbb_cells
                    target_point_array::Array{Point}, current_contain_both = check_contain_start_and_goal(cell, start, goal)
                    update_cell_graph_opt(cell, m_g, s, g_score, rhs, pq, heuristic, came_from; target_point_array=target_point_array)
                    push!(mleaves, cell)
                    filter!(e->e≠cell,bleaves)
                    ##TODO: need to re-organize the below 2 if statements
                    # if use_upper_change
                    #     filter!(e->e≠cell,free_mleaves) 
                    #     filter!(e->e≠cell,n_free_mleaves) 
                    # end 
                    # if use_lower_change||block_mleaves_by_prob
                    #     filter!(e->e≠cell,block_mleaves)
                    #     filter!(e->e≠cell,n_block_mleaves)
                    # end
                    fmleaves = (use_lower_change||block_mleaves_by_prob) ? [fleaves..., n_block_mleaves...] : [fleaves..., mleaves...]
                    u_bleaves = [mleaves..., bleaves...]
                    fleaves_with_free_mleaves = use_upper_change ? [fleaves..., free_mleaves...] : [fleaves...]
                end
                was_changed = false
                temp_bbbb_cells = []
            end
        else
            current_fleaves, current_mleaves, current_bleaves = [],[],[]
            current_free_mleaves::Array{MetricCell}, current_n_free_mleaves = [],[]
            current_block_mleaves::Array{MetricCell}, current_n_block_mleaves = [],[]
            ## Block combo_mvisited cells
            for cell in combo_mvisited
                temp_fleaves::Array{MetricCell}, temp_mleaves::Array{MetricCell} = [], []
                ccc += 1
                target_point_array::Array{Point}, current_contain_both = check_contain_start_and_goal(cell, start, goal)
                
                update_cell_graph_blocked_opt(cell, m_g, s, g_score, rhs, pq, heuristic, came_from; target_point_array=target_point_array)
                

                side_length = max_side_size(cell)
                epsilon = side_length/2.0
                temp_fleaves, temp_mleaves, temp_bleaves = construct!(space, cell; epsilon=epsilon, delta=delta)


                append!(current_bleaves, temp_bleaves)
                append!(current_mleaves, temp_mleaves)
                append!(current_fleaves, temp_fleaves)  
                (temp_free_mleaves::Array{MetricCell}, temp_n_free_mleaves::Array{MetricCell}) = use_upper_change ? classify_mcells(temp_mleaves) : ([],[])
                (temp_block_mleaves::Array{MetricCell}, temp_n_block_mleaves::Array{MetricCell}) = use_lower_change ? classify_mcells2(temp_mleaves) : ([],[])
                ##! But for block, if it's block_mleaves_by_prob, it will be done in the end
                append!(current_free_mleaves, temp_free_mleaves)
                append!(current_n_free_mleaves, temp_n_free_mleaves)
                append!(current_block_mleaves, temp_block_mleaves)
                append!(current_n_block_mleaves, temp_n_block_mleaves)

            end
            # update fleaves, mleaves, bleaves
            append!(fleaves, current_fleaves)         
            filter!(e->!(e in combo_mvisited), mleaves)  
            append!(mleaves, current_mleaves)
            append!(bleaves, current_bleaves)

            # println("test fleaves ", length(fleaves) == length(unique(fleaves)))
            # println("test mleaves ", length(mleaves) == length(unique(mleaves)))
            # println("test bleaves ", length(bleaves) == length(unique(bleaves)))

            ## update others for upper/lower/blockpart change
            ###?? TODO: optimize the code
            filter!(e->!(e in combo_mvisited), free_mleaves) 
            free_mleaves = [free_mleaves..., current_free_mleaves...]
            filter!(e->!(e in combo_mvisited), n_free_mleaves)  
            n_free_mleaves = [n_free_mleaves..., current_n_free_mleaves...]
            filter!(e->!(e in combo_mvisited), block_mleaves)   
            block_mleaves = [block_mleaves..., current_block_mleaves...] 
            filter!(e->!(e in combo_mvisited), n_block_mleaves)   
            n_block_mleaves = [n_block_mleaves..., current_n_block_mleaves...]



            # println("test free_mleaves ", length(free_mleaves) == length(unique(free_mleaves)))
            # println("test n_free_mleaves ", length(n_free_mleaves) == length(unique(n_free_mleaves)))
            # println("test block_mleaves ", length(block_mleaves) == length(unique(block_mleaves)))
            # println("test n_block_mleaves ", length(n_block_mleaves) == length(unique(n_block_mleaves)))
            fmleaves = (use_lower_change||block_mleaves_by_prob) ? [fleaves..., n_block_mleaves...] : [fleaves..., mleaves...]
            u_bleaves = [mleaves..., bleaves...]
            fleaves_with_free_mleaves = use_upper_change ? [fleaves..., free_mleaves...] : [fleaves...]

            
           

            block_to_unblock_leaves::Array{MetricCell} = []
            unblock_to_block_leaves::Array{MetricCell} = []
            ## choose the new 30% cells
            if block_mleaves_by_prob
                mleaves_prob = [leaf.block_prob for leaf in mleaves]
                new_block_mleaves = unique(sample(mleaves, Weights(mleaves_prob), round(Int, length(mleaves)*0.3), replace=true))
                new_n_block_mleaves =  [c for c in mleaves if !(c in new_block_mleaves)]
                for cell in block_mleaves
                    if !(cell in new_block_mleaves)
                        push!(block_to_unblock_leaves, cell)
                    end
                end
                for cell in new_block_mleaves
                    if cell in n_block_mleaves
                        push!(unblock_to_block_leaves, cell)  ## Wrong!!!
                    end
                end
            end

            new_leaves = use_lower_change ? [current_fleaves..., current_n_block_mleaves...] : [current_fleaves..., current_mleaves...]
            if block_mleaves_by_prob
                temp_leaves = [leaf for leaf in current_mleaves if !(leaf in new_block_mleaves)]
                new_leaves = [current_fleaves..., block_to_unblock_leaves..., temp_leaves...]
            end

            ## Update new free cells
            for new_leaf in [current_fleaves..., current_free_mleaves...] #for upper

                target_point_array, current_contain_both = check_contain_start_and_goal(new_leaf, start, goal)

                update_cell_graph_opt(new_leaf, m_g2, s2, g_score2, rhs2, pq2, heuristic2, came_from2; target_point_array=target_point_array)
            end

            for new_leaf in new_leaves 
                    
                target_point_array, current_contain_both = check_contain_start_and_goal(new_leaf, start, goal)
                update_cell_graph_opt(new_leaf, m_g, s, g_score, rhs, pq, heuristic, came_from; target_point_array=target_point_array)
            end

            if block_mleaves_by_prob
                ## block unblock_to_block_leaves
                for cell in unblock_to_block_leaves
                    target_point_array, current_contain_both = check_contain_start_and_goal(cell, start, goal)
                    update_cell_graph_blocked_opt(cell, m_g, s, g_score, rhs, pq, heuristic, came_from; target_point_array=target_point_array)
                end
                block_mleaves = new_block_mleaves
                n_block_mleaves = new_n_block_mleaves

            end



        end
    end
    # if ifplot
        # plot_upper_and_lower(fleaves, fmleaves, name1="Upper bound", name2="Lower bound", path1=fpath, path2=fmpath, bleaves=bleaves, u_bleaves=u_bleaves, serial=counter)
    # end
end



function main(;use_distance_field::Bool=true)


    space_names = ["world1-v2", "world-ball", "world-maze", "world-u_l", "wholeOB", "world2"]
    starts::Array{Point} = [[0.0,0.0], [0.0, 0.2], [0.5, 0.465], [0.0, 0.3], [0.0,0.0], [0.0,0.0]]
    goals::Array{Point} = [[1.0, 1.0], [1.0, 0.2], [0.75, 0.0], [1.0,0.3], [1.0,1.0], [1.0,1.0]]

    #****** choose the space **********#
    chosen::Integer = 2
    nx::Integer = 1000 #for distance field


    space_name = space_names[chosen]
    start::Point = starts[chosen]
    goal::Point = goals[chosen]

    distance_field::Array{Float64,2} = use_distance_field ? load("data/output/jld/"*space_name*"-"*string(nx)*".jld")["data"] : Array{Float64}(undef, 0, 2)

    space::Space = PlanarSpace(filename="data/input/spaces/planar/"*space_name*".json", 
                                triangulate=!use_distance_field,
                                sdf=distance_field)
    delta::Float64 = 0.01 #TODO: it was 0.1 before, I changed it to test
    epsilon::Float64 = 0.1 #max cell size


   
    @time test(space, start, goal, delta, epsilon; verbose=true, ifplot=true, use_upper_change=false, use_lower_change=false, block_mleaves_by_prob=false, method=1)


    # vg = VisibilityGraph(space)
    # path = plan(vg, start, goal)
    # display(path)

    # fig, ax = PyPlot.subplots()
    # plot(vg; ax=ax)
    # plot(path; ax=ax, ispath=true, color="red", zorder=4)
    # path_dis = metric(space, path)
    # println("path got from vg is: ", path_dis)
### optimal path got from vg is 1.077032961426901
end








# video2("Upper bound ", "Lower bound ")
main()
# video1()  
# # @suppress main()

