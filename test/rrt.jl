#=
 * File: rrt.jl
 * Project: test
 * File Created: Friday, 17th April 2020 1:19:21 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 26th April 2020 9:50:02 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using Test
import PyPlot

using MetricTools, MetricSpaces.Planar, MetricSpaces.Arms

using
    MotionPlanning,
    MotionPlanning.RRTs,
    MotionPlanning.VisibilityGraphs

function test_rrt()
    space::PlanarSpace = PlanarSpace(filename="data/input/spaces/planar/world-halfdoor.json")
    start::Point = [0, 0]
    goal::Point = [1, 0]
    rrt::RRTstar = RRTstar(space, start)
    path::Array{Point} = @time plan(rrt, start, goal; n=20^2)
    
    PyPlot.gca().set_aspect("equal")
    plot(rrt)
    plot(path; ispath=true, color="red", zorder=4)
    println(metric(space, path))
end

function test_rrt_arms2()
    space::ArmSpace = ArmSpace(
        "data/input/spaces/arms/arms2.json", 
        "data/input/spaces/planar/world2.json")

    start::Point = [0, 0]
    goal::Point = [pi/2, 0]
    rrt::RRTstar = RRTstar(space, start; step=0.3, radius=0.5)
    path::Array{Point} = plan(rrt, start, goal; n=25^2)
    println(space.bounds)
    display(path)
    
    PyPlot.gca().set_aspect("equal")
    plot(space, path; filename="prm2D-1")

    # plot(path; ispath=true, color="red", zorder=4)
    println(metric(space, path))
end


function test_rrt_arms3()
    space::Space = ArmSpace(
            "data/input/spaces/arms/arms3-2.json", 
            "data/input/spaces/planar/world-slot.json")

    start::Point = [pi/2, 0, 0]
    goal::Point = [pi/4, -pi/4, 0]
    rrt::RRTstar = RRTstar(space, start; step=0.3, radius=0.5)
    path::Array{Point} = plan(rrt, start, goal; n=30^3)
    println(space.bounds)
    display(path)
    
    PyPlot.gca().set_aspect("equal")
    plot(space, path; filename="x")

    # plot(path; ispath=true, color="red", zorder=4)
    println(metric(space, path))
end

# test_rrt()
# test_rrt_arms2()
test_rrt_arms3()
