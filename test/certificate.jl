#=
 * File: certificate.jl
 * Project: test
 * File Created: April, 3rd March 2020 9:05 am
 * Author: Luyang Zhao (luyang.zhao.gr@dartmouth.edu)
 * -----
 * Last Modified: April, 6th March 2020 5:18 pm
 * Modified By: Luyang Zhao (luyang.zhao.gr@dartmouth.edu)
=#

import PyPlot, LinearAlgebra
# using ProgressMeter
using DataStructures
# using Polyhedra
# using QHull
using LightGraphs, MetaGraphs

using MetricTools, MetricTools.Geometry, MetricTools.KNNs, MetricTools.MetricGraphs
using MetricSpaces, MetricSpaces.Samplers
using MetricSpaces.Planar
using HomotopyContinuation



mutable struct Eggplant
    p1::Point
    p2::Point
    r1::Float64
    r2::Float64
    function Eggplant(p1::Point, p2::Point, r1::Float64, r2::Float64)
        eggplant::Eggplant = new(
            p1,
            p2,
            r1,
            r2
        )
        return eggplant
    end
end

### certificate Complex
mutable struct CertiComplex
    space::Space
    max_n::Integer
    min_r::Float64
    points::Array{Point}
    radiuses::Array{Float64} #!todo: this is for Cϵ
    connected::Array{Array{Int64}}
    sample_points::Array{Array{Point}} #TODO: not efficient


    function CertiComplex(space::Space; max_n::Integer=6, min_r::Float64=0.02)
        cc::CertiComplex = new(
            space,
            max_n,
            min_r,
            [],
            [],
            [],
            [])
        construct!(cc)
        println("start to form eggplant")
        eggplants::Array{Eggplant} = form_eggplant!(cc)
        println("length of eggplant: ", length(eggplants))
        for ep in eggplants
            # println("eggplants: ", ep.p1, " ", ep.p2 )
        end
        plot(cc, eggplants)
        println("finished form eggplant")
        return cc
    end
end

### Question: do we need to set all things into polygons later

function random_a_free_point(cc::CertiComplex)
    bounds = cc.space.bounds
    while true
        point = [rand() * (b.hi - b.lo) + b.lo for b in bounds]
        if isfree(cc.space, point) return point end
    end
end

function q_inside_any_Cϵ(cc::CertiComplex, q::Point) ##!TODO: need to modify this
    for (i, point) in enumerate(cc.points)
        dis = metric(cc.space, q, point)
        ### TODO:question, if inside this radius, it's in certificate, then no need to do localplanner, right?
        # if dis <= cc.radiuses[i] && localplanner(cc.space, q, point)
        if dis <= cc.radiuses[i] 
            return true
        end
    end
    return false
end

function create_certificate_Cq(cc::CertiComplex, q::Point)
    d_safe::Float64, n_points::Array{Point} = cert(cc.space, q) 
    return d_safe/2.0, n_points
end
##!! Made change in this function, instead of return d_safe, also return a new q
# function create_certificate_Cq_and_adjust_q(cc::CertiComplex, q::Point)
#     ####!! Set number 100? If it's close enough, then it's fine?
#     d_safe::Float64, n_points::Array{Point} = create_certificate_Cq(cc, q)
#     # println("The nearest point of current point: ", q, " is: ", n_point)
#     obs_p::Point = first(n_points)
#     base_step_size::Float64 = 0.05
#     step_size = base_step_size/2.0
#     v = LinearAlgebra.normalize!(q - obs_p)
#     #### added #####
#     # first_pt = obs_p + step_size * v
#     # first_d = step_size/2.0
#     #### added ####
#     if length(n_points) > 1 return d_safe, q end



#     temp_points = n_points
#     left::Point = q
#     right::Point = q
#     temp_x = q
    
#     # Change to use binary search
#     while length(temp_points) == 1
#         step_size = step_size * 2
#         temp_x = temp_x + step_size * v ### TODO: check whether temp_x is free
#         while !isfree(cc.space, temp_x)
#             # Left is free, temp_x is not free
#             println("GOES IN HERE!!!!!")
#             step_size = step_size/2.0
#             temp_x = left + step_size * v
#         end
#         temp_d, temp_points = create_certificate_Cq(cc, temp_x)
#         if length(temp_points) > 1
#             return temp_d, temp_x
#         end
#         temp_obs_p = first(temp_points)
#         if isapprox(temp_obs_p, obs_p)
#             left = temp_x
#         else
#             ## Then the real p should between q and temp_x
#             right = temp_x
#             break
#         end
#     end
#     # Use bisection to help finding the medial axis between left(q) and right  ###TODO: rewrite this function to make it efficient
#     counter = 0
#     while length(temp_points) == 1
#         mid = (left + right) * 0.5
#         temp_d, temp_points = create_certificate_Cq(cc, mid)
#         if length(temp_points) > 1 
#             return temp_d, mid
#         end
#         temp_obs_p = first(temp_points)
#         if isapprox(temp_obs_p, obs_p)
#             left = mid
#         else
#             right = mid
#         end
#         if counter > 500 # already tried 500 times
#             println("already tried 500 times and cannot find best middle point")
#             return temp_d, mid
#         end
#         counter += 1
#     end
# end

function create_certificate_Cq_and_adjust_q(cc::CertiComplex, q::Point)
    ####!! Set number 100? If it's close enough, then it's fine?
    d_safes::Array{Float64}, qs::Array{Point} = [],[]
    d_safe::Float64, n_points::Array{Point} = create_certificate_Cq(cc, q)
    
    base_step_size::Float64 = 0.05
    step_size = base_step_size/2.0
    temp_p::Point = []


    function temp_func(all_points, c_q, c_d)
        for obs_pt in all_points
            temp_v = LinearAlgebra.normalize!(q - obs_pt)
            temp_p = obs_pt + step_size * temp_v
            temp_step_size = step_size
            for ii in 1:20
                if !isfree(cc.space, temp_p)
                    temp_step_size = temp_step_size/2.0
                    temp_p = obs_pt + temp_step_size * temp_v
                else
                    obs_d, nn = create_certificate_Cq(cc, temp_p)
                    push!(d_safes, obs_d)
                    push!(qs, temp_p)
                    break
                end
            end
        end 
        push!(d_safes, c_d)
        push!(qs, c_q)
        return d_safes, qs
    end

    if length(n_points) > 1 
        d_safes, qs = temp_func(n_points, q, d_safe)
        return d_safes, qs
    end

    obs_p::Point = first(n_points)
    v = LinearAlgebra.normalize!(q - obs_p)


    temp_points = n_points
    left::Point = q
    right::Point = q
    temp_x = q
    
    # Change to use binary search
    while length(temp_points) == 1
        step_size = step_size * 2
        temp_x = temp_x + step_size * v ### TODO: check whether temp_x is free
        while !isfree(cc.space, temp_x)
            # Left is free, temp_x is not free
            # println("GOES IN HERE!!!!!")
            step_size = step_size/2.0
            temp_x = left + step_size * v
        end
        temp_d, temp_points = create_certificate_Cq(cc, temp_x)
        if length(temp_points) > 1
            d_safes, qs = temp_func(temp_points, temp_x, temp_d)
            return d_safes, qs
            # return temp_d, temp_x
        end
        temp_obs_p = first(temp_points)
        if isapprox(temp_obs_p, obs_p)
            left = temp_x
        else
            ## Then the real p should between q and temp_x
            ## temp_obs_p is the second obs_p
            right = temp_x
            break
        end
    end
    # Use bisection to help finding the medial axis between left(q) and right  ###TODO: rewrite this function to make it efficient
    counter = 0
    while length(temp_points) == 1
        mid = (left + right) * 0.5
        temp_d, temp_points = create_certificate_Cq(cc, mid)
        if length(temp_points) > 1 
            d_safes, qs = temp_func(temp_points, mid, temp_d)
            return d_safes, qs
            # return temp_d, mid
        end
        temp_obs_p = first(temp_points)
        if isapprox(temp_obs_p, obs_p)
            left = mid
        else
            right = mid
        end
        if counter > 500 # already tried 500 times
            println("already tried 500 times and cannot find best middle point")
            ### For this case, do not add the second obs_pt into
            d_safes, qs = temp_func(temp_points, mid, temp_d)
            return d_safes, qs
            # return temp_d, mid
        end
        counter += 1
    end
end

function get_Cϵ_of_q(q::Point, d_safe::Float64) ###TODO: modify this
    return d_safe
end

# create connection of final point added into certificate complex
function create_connection(cc::CertiComplex)  ###TODO: Can combine with another function 
    c_i = length(cc.points)
    c_p = cc.points[c_i]
    c_radius = cc.radiuses[c_i]
    for i in 1:(c_i -1)
        dis = metric(cc.space, c_p, cc.points[i])
        ##!!!TODO: do we need to do localplanner  here??? does it mean it must be free???
        if dis < cc.radiuses[i]+c_radius # && localplanner(cc.space, c_p, cc.points[i])
            push!(cc.connected[i], c_i)
            push!(cc.connected[c_i], i)
        end
    end
end

function graph_is_connected(cc)
    visited = [false for i in 1:length(cc.points)]
    q = Queue{Int64}()
    enqueue!(q, 1)
    visited[1] = true
    while !isempty(q)
        i = dequeue!(q)
        for ele in cc.connected[i]
            if !visited[ele] 
                enqueue!(q, ele)
                visited[ele] = true
            end
        end
    end
    if sum(visited) == length(cc.points) #all true
        return true
    else
        return false
    end
end

function get_tangent_lines(ep::Eggplant)
    x1 = ep.p1
    x2 = ep.p2
    r1 = ep.r1
    r2 = ep.r2

    @polyvar t1[1:2] t2[1:2]
    F = [   (t2[1]-t1[1])*(t2[1]-x2[1])+(t2[2]-t1[2])*(t2[2]-x2[2]),
            (t2[1]-t1[1])*(t1[1]-x1[1])+(t2[2]-t1[2])*(t1[2]-x1[2]),  
            (t1[1]-x1[1])^2+(t1[2]-x1[2])^2-r1^2 ,
            (t2[1]-x2[1])^2+(t2[2]-x2[2])^2-r2^2 ]
    

    result = solve(F)
    solus = real_solutions(result)
    tangent_lines::Array{Line} = []
    if length(solus) == 4
        l1::Line = (x1, x2)
        for solu in solus
            l2::Line = ([solu[1], solu[2]], [solu[3],solu[4]])
            interp = intersection(l1, l2)
            if isnothing(interp)
                push!(tangent_lines, l2)
            end
        end
    else
        for solu in solus
            l2::Line = ([solu[1], solu[2]], [solu[3],solu[4]])
            push!(tangent_lines, l2)
        end
    end
    return tangent_lines
end

function ep_free_inside(cc, ep; num = 5)
    equal_r::Bool = false
    if isapprox(ep.r1, ep.r2)
        equal_r = true
    end
    d_12 = metric(cc.space, ep.p1, ep.p2)  
    xx::Float64 = ep.r1*d_12/abs(ep.r2-ep.r1)
    function calculated_d(q) # This function assume not equal r
        if ep.r1 < ep.r2
            d_13 = metric(cc.space, ep.p1, q)
            r3 = (xx + d_13)*ep.r1/xx
        else
            d_23 = metric(cc.space, ep.p2, q)
            r3 = (xx + d_23)*ep.r2/xx
        end
    end
# evenly get points inside
    x1 = range(ep.p1[1], stop=ep.p2[1], length = num)
    x2 = range(ep.p1[2], stop=ep.p2[2], length = num)
    for (x_1, x_2) in zip(x1, x2)
        ## find the distance of this points
        q::Point = [x_1, x_2]
        if q == ep.p1 || q == ep.p2 continue end
        if !isfree(cc.space, q) return false end
        d::Float64, n_points::Array{Point} = create_certificate_Cq(cc, q) 
        if equal_r
            if isapprox(d, ep.r1)
               continue 
            elseif d < ep.r1
                return false
            end
        else
            temp = calculated_d(q)
            if isapprox(d, temp)
                continue
            elseif d < temp
                # println(q)
                # println(d)
                # println(d-temp)
                return false
            end
        end
    end
    return true
end





function belongsto(cc, ep::Eggplant, p3::Point, r3::Float64)
    ## check whether p, p1, p2 are on the same line
    if (ep.p1 == p3 && ep.r1 == r3) || (ep.p2 == p3 && ep.r2 == r3) return true end
    p_23 = p3 - ep.p2
    p_13 = p3 - ep.p1
    # println("p_23: ", p_23)
    # println("p_13: ", p_13)
    LinearAlgebra.normalize!(p_23)
    LinearAlgebra.normalize!(p_13)
    for i in 1:length(p_23)
        if abs(p_23[i])<1e-6
            p_23[i] = 0.0
        end
    end
    for i in 1:length(p_13)
        if abs(p_13[i])<1e-6
            p_13[i] = 0.0
        end
    end
    # println("p_23: ", p_23)
    # println("p_13: ", p_13)
    if !isapprox(p_23, p_13)
        if !isapprox(p_23, -p_13)
            return false 
        end
    end
    # println("p_23: ", p_23)
    # println("p_13: ", p_13)
    ## check whether the radius is correct
    p_12 = ep.p2 - ep.p1
    LinearAlgebra.normalize!(p_12)
    d1::Float64 = 0 
    d2::Float64 = 0
    # ratio1::Float64 = 0  ##!! modified?
    # ratio2::Float64 = 0
    least_r3::Float64 = 0
    type::Integer = 0
    d_12 = metric(cc.space, ep.p1, ep.p2)  ##TODO: make modification to be more efficient
    d_23 = metric(cc.space, ep.p2, p3)
    d_13 = metric(cc.space, ep.p1, p3)
    if isapprox(ep.r1, ep.r2)
        if !isapprox(ep.r1, r3) return false end
        if isapprox(p_12, p_13)  # p2, p3 on the same side comparing to p1
            if isapprox(p_23, p_13)  # p2 is in the middle p1 -- p2 -- p3
                type = 1
                # println("p1 -- p2 -- p3")
            else # p3 is in the middle p1 -- p3 -- p2
                type = 2
                # println("p1 -- p3 -- p2")
            end
        else # p3 is on the other side of p1  p3---p1---p2
            type = 3
            # println("p3 -- p1 -- p2")
        end
    else
        xx::Float64 = ep.r1*d_12/abs(ep.r2-ep.r1) ## assume not equal distance
        if isapprox(p_12, p_13)  # p2, p3 on the same side comparing to p1
            if isapprox(p_23, p_13)  # p2 is in the middle p1 -- p2 -- p3
                if ep.r1 < ep.r2
                    # ratio1 = xx/(xx+d_13)
                    # ratio2 = ep.r1/r3
                    least_r3 = ep.r1/(xx/(xx+d_13))
                else
                    # ratio1 = (xx-d_23)/xx
                    # ratio2 = r3/ep.r2
                    least_r3 = ep.r2*(xx-d_23)/xx
                end
                type = 1
                # println("p1 -- p2 -- p3")
            else # p3 is in the middle p1 -- p3 -- p2
                if ep.r1 < ep.r2
                    # ratio1 = xx/(xx+d_13)
                    # ratio2 = ep.r1/r3
                    least_r3 = ep.r1/(xx/(xx+d_13))
                else
                    # ratio1 = xx/(xx+d_23)
                    # ratio2 = ep.r2/r3
                    least_r3 = ep.r2/( xx/(xx+d_23))
                end
                type = 2
                # println("p1 -- p3 -- p2")
            end
        else # p3 is on the other side of p1  p3---p1---p2
            if ep.r1 < ep.r2
                # ratio1 = (xx-d_13)/xx
                # ratio2 = r3/ep.r1
                least_r3 = ep.r1*(xx-d_13)/xx
            else
                # ratio1 = xx/(xx+d_23)
                # ratio2 = ep.r2/r3
                least_r3 = ep.r2/(xx/(xx+d_23))
            end
            type = 3
            # println("p3 -- p1 -- p2")
        end
        # if !isapprox(ratio1, ratio2) return false end
        if !isapprox(r3, least_r3)
            if r3 <= least_r3  ### !r3 is inside the eggplant, should include this case in
                ### For this case, change r3 to be least_r3
                # println("goes in side: ", r3, " < ", least_r3)
                # println("p3 is: ", p3)
                # println("p1 and p2: ", ep.p1, " ", ep.p2 )
                # println("type: ", type)
                r3 = least_r3
            else
                return false
            end
        end
    end
    if type == 1
        new_ep = Eggplant(ep.p1, p3, ep.r1, r3)
        if ep_free_inside(cc, new_ep; num=5)
            ep.p2 = p3
            ep.r2 = r3
            return true
        else
            return false
        end
    elseif type == 3
        new_ep = Eggplant(p3, ep.p2, r3, ep.r2)
        if ep_free_inside(cc, new_ep; num=5)
            ep.p1 = p3
            ep.r1 = r3 
            return true
        else
            return false
        end
    else
        return true
    end
end

function form_eggplant!(cc)  ### need to change it!!!
    eggplants = []
    finished = [false for i in 1:length(cc.points)]
    visited = [false for i in 1:length(cc.points)]
    q = Queue{Int64}()
    enqueue!(q, 1)
    while !isempty(q)
        i = dequeue!(q)
        c_p = cc.points[i]
        c_r = cc.radiuses[i]
        # println("current p is: ", c_p, " radius: ", c_r)
        for ele in cc.connected[i]
            if finished[ele] continue end
            p3 = cc.points[ele]
            r3 = cc.radiuses[ele]
            # println("current neighbor is: ", p3, " radius: ", r3)
            belongstoany = false
            for ep in eggplants  ###TODO: !!!  Need to modify, do not to check everyone, only check the one that are close
                if belongsto(cc, ep, p3, r3)
                    if belongsto(cc, ep, c_p, c_r)
                        # println("current neighbor and p bth belong to: ", ep.p1, " and ", ep.p2)
                        belongstoany = true
                    end
                end
            end
            if !belongstoany
                # println("current neighbor NOT belongs to any")
                new_ep = Eggplant(c_p, p3, c_r, r3)
                # push!(eggplants, new_ep)
                if ep_free_inside(cc, new_ep; num=5)
                    push!(eggplants, new_ep)
                else
                    # println("not free!!")
                end
            end
            if !visited[ele]
                enqueue!(q, ele)
                visited[ele] = true
            end
        end
        finished[i] = true # is true after all is visited
    end
    return eggplants
end

function add_certi_complex(cc::CertiComplex, q::Point, d_safe::Float64; allow_tiny::Bool=false)
    d_ϵ = get_Cϵ_of_q(q, d_safe)

    # if !allow_tiny
    #     if d_ϵ < cc.min_r return 0 end
    # end
    push!(cc.points, q)
    push!(cc.radiuses, d_ϵ)
    push!(cc.connected, [])
    create_connection(cc)
    return length(cc.points)
end




function construct!(cc::CertiComplex)
    println("start to construct...")
    counter::Integer = 0

    # while true
    for i in 1:10
        # println("counter is: ", counter)
        # 
        if counter > cc.max_n && graph_is_connected(cc) break end
        q::Point = random_a_free_point(cc)
        # q::Point = [0.01, 0.1]
        if q_inside_any_Cϵ(cc, q) continue end
        # d_safe, q_new = create_certificate_Cq_and_adjust_q(cc, q)
        # q = q_new
        # add_certi_complex(cc, q, d_safe)

        d_safes, q_news = create_certificate_Cq_and_adjust_q(cc, q)
        for (q, d_safe) in zip(q_news, d_safes)
            add_certi_complex(cc, q, d_safe)
        end
        # q = q_new
        # add_certi_complex(cc, q, d_safe)

        # position of this line matters
        counter += 1  
        # break
    end
    println("finish construct!")

    # q2::Point = [0.253, 0.05]
    # q3::Point = [0.208, 0.05]
    # q4::Point = [0.292, 0.01]

    # q5::Point = [0.18, 0.05]
    # q1::Point = [0.359, 0.05]
    # qq::Array{Point} = [q2, q3,q4,q5]
    # q1::Point = [0.8060915838788822, 0.8312409607438476] 
    # q2::Point = [0.7065189787547561, 0.8499291723398068]
    # # q3::Point = [0.20000000020012276, 0.6558563107320143]
    # qq::Array{Point} = [q1, q2]#, q3]
    # for q in qq
    #     # if q_inside_any_Cϵ(cc, q) continue end
    #     d_safe, q_new = create_certificate_Cq_and_adjust_q(cc, q)
    #     q = q_new
    #     add_certi_complex(cc, q, d_safe)
    # end
end

function refine!(cc::CertiComplex)
    println("start to refine...")
    while true
        if graph_is_connected(cc) break end
        q::Point = random_a_free_point(cc) ###TODO: should only add point near start/goal
        if q_inside_any_Cϵ(cc, q) continue end
        d_safe, q_new = create_certificate_Cq_and_adjust_q(cc, q) ##!!!: change it to be save as construct
        q = q_new
        add_certi_complex(cc, q, d_safe)  
    end
end




function add_edge_with_weight!(g::MetricGraph, point1::Point, point2::Point, dis::Float64)
    vertex1, vertex2 = get_vertex(g, point1), get_vertex(g, point2)
    edge = MetaGraphs.Edge(vertex1, vertex2)
    MetaGraphs.add_edge!(g.graph, edge)
    MetaGraphs.set_prop!(g.graph, edge, :weight, dis)  
end

function contains(p::Point, cc::CertiComplex)
    result = []
    for i in eachindex(cc.points)
        if metric(cc.space, p, cc.points[i]) < cc.radiuses[i] # should not include == #!todo:???
            push!(result, i)
        end
    end
    return result
end

function add_sample_points_and_edge_into_global_graph(global_graph, free_points, is_s_i, is_g_i, start, goal)
    free_points2::Array{Point} = [p for p in free_points if !MetricGraphs.contains(global_graph, p)]
    all_added = MetricGraphs.add!(global_graph, free_points2)
    for i in 1:length(free_points)
        for j in i:length(free_points)
            temp_p1 = free_points[i]
            temp_p2 = free_points[j]
            dis = MetricGraphs.metric(global_graph,temp_p1, temp_p2)
            add_edge_with_weight!(global_graph, temp_p1, temp_p2, dis)
        end
    end
    if is_s_i
        for p in free_points
            dis = MetricGraphs.metric(global_graph,p, start)
            add_edge_with_weight!(global_graph, p, start, dis)
        end
    end
    if is_g_i
        for p in free_points
            dis = MetricGraphs.metric(global_graph,p, goal)
            add_edge_with_weight!(global_graph, p, goal, dis)
        end
    end   
end


function get_sample_points(cc, i, ele; num=5)
    # circle 1: (x0, y0), radius r0
    # circle 2: (x1, y1), radius r1 ###TODO: make modification
    p1, p2, r0, r1 = cc.points[i], cc.points[ele], cc.radiuses[i], cc.radiuses[ele]
    d = metric(cc.space, p1, p2)
    x0 = p1[1]
    y0 = p1[2] 
    x1 = p2[1] 
    y1 = p2[2]


    a=(r0^2-r1^2+d^2)/(2*d)
    h=sqrt(r0^2-a^2)
    x2=x0+a*(x1-x0)/d   
    y2=y0+a*(y1-y0)/d   
    x3=x2+h*(y1-y0)/d     
    y3=y2-h*(x1-x0)/d 

    x4=x2-h*(y1-y0)/d
    y4=y2+h*(x1-x0)/d
    inter1::Point = [x3, y3]
    inter2::Point = [x4, y4]
    result::Array{Point} = []
    xs = []
    ys = []
    for i in range(x3, stop=x4, length=num)
        push!(xs, i)
    end
    for j in range(y3, stop=y4, length=num)
        push!(ys, j)
    end
    for ii in eachindex(xs)
        temp::Point = [xs[ii], ys[ii]]
        push!(result, temp)
    end
    # println("p1 is: ", p1)
    # println("p2 is: ", p2)
    # println("r0 is: ", r0)
    # println("r1 is: ", r1)
    # println("inter point1: ", inter1, " 2: ", inter2)
    return result

end

function plan2(cc::CertiComplex, start::Point, goal::Point)
    if !isfree(cc.space, start) || !isfree(cc.space, goal) 
        println("Wrong!!! Either start point or goal point is not free!")
    end      
    s_i = contains(start,cc)
    g_i = contains(goal,cc)
    if isempty(s_i)
        d_s, npts = create_certificate_Cq(cc, start)
        s_i = add_certi_complex(cc, start, d_s; allow_tiny=false)
    end
    if isempty(g_i)
        d_g, npts = create_certificate_Cq(cc, goal)
        g_i = add_certi_complex(cc, goal, d_g; allow_tiny=false)
    end
    println("s_i is: ", s_i)
    println("g_i is: ", g_i)

    refine!(cc) # make sure new graph is connected

    # if belongs_to_same_circle ### TODO: finish this
    # if s_i == g_i return [start, goal] end 
    global_graph::MetricGraph = MetricGraph(cc.space.dim)  ##TODO: where to construct this global graph matters
    visited = [false for i in 1:length(cc.points)]
    ### add start and goal
    free_points2::Array{Point} = [start, goal]
    all_added = MetricGraphs.add!(global_graph, free_points2)


    q = Queue{Int64}()
    enqueue!(q, 1)
    visited[1] = true
    while !isempty(q)
        i = dequeue!(q)
        sample_points_of_current_i = []
        for ele in cc.connected[i]
            sample_points = get_sample_points(cc, i, ele)
            append!(sample_points_of_current_i, sample_points)
            if !visited[ele]
                enqueue!(q, ele)
                visited[ele] = true
            end
        end
        is_s_i = false
        is_g_i = false
        if i in s_i
            is_s_i = true
        end
        if i in g_i
            is_g_i = true
        end
        add_sample_points_and_edge_into_global_graph(global_graph, sample_points_of_current_i, is_s_i, is_g_i, start, goal)
    end
    path, dis = find_path(start, goal, global_graph)
    println("path ", path)
    println("dis ", dis)
    # plot(global_graph)
    # fig, ax = PyPlot.subplots()
    return path, dis
end

function find_path(start::Point, goal::Point, global_graph)
    #plot(global_graph)
    s_i = global_graph.pointids[start]
    g_i = global_graph.pointids[goal]
    path_from_a_Star::Array{Point} = MetricGraphs.astar(global_graph, start, goal)
    # println("start is: ", start, " goal is: ", goal)
    # println("path is: ", path_from_a_Star)
    dis_from_a_Star::Union{Nothing,Float64} = distance(global_graph, path_from_a_Star)
    return path_from_a_Star, dis_from_a_Star
end




##!!!TODO: do we need to change the shape of the cell?

function MetricTools.plot(cc::CertiComplex)
    fig, ax = PyPlot.subplots()
    ax.set_aspect(aspect=1)
    plot(cc.space)
    for i in eachindex(cc.points)
        plot(cc, i)
    end

    # temp_p::Point  = [0.7563052813168192, 0.8405850665418272]
    # r = 0.07572061416605147
    # plot(temp_p, r; color="red", fill=false, alpha = 0.5)
    # plot(temp_p, r*2; color="red", fill=false, alpha = 0.5)
    return ax
end

function MetricTools.plot(cc::CertiComplex, i::Integer)
    colors::Array{String} = [
        "red", "purple", "indigo", "blue", "green", "yellow", "orange"
    ]
    color = colors[i % length(colors) + 1]
    plot(cc.points[i], cc.radiuses[i]; color=color, fill=false)
end
   
function MetricTools.plot(cc::CertiComplex, eggplants::Array{Eggplant})
    fig, ax = PyPlot.subplots()
    ax.set_aspect(aspect=1)
    plot(cc.space)
    for (i, ep) in enumerate(eggplants)
        plot(ep, i)
    end
    return ax
end

function MetricTools.plot(ep::Eggplant, i::Integer)
    colors::Array{String} = [
        "red", "purple", "indigo", "blue", "green", "yellow", "orange"
    ]
    color = colors[i % length(colors) + 1]
    plot(ep.p1, ep.r1; color=color, fill=false, alpha = 0.5)
    plot(ep.p2, ep.r2; color=color, fill=false, alpha = 0.5)
    tangent_lines::Array{Line} = get_tangent_lines(ep)
    plot(tangent_lines, color=color , zorder = 2, vertices = false, alpha = 1.0, linestyle = "solid")
    
end


#!TODO: sample all points firstly or sample some point firstly => which is more efficient


function test_cc()
    println("start...")
    # space::Space = PlanarSpace(; filename="data/input/spaces/planar/angled-doors.json")
    space::Space = PlanarSpace(; filename="data/input/spaces/planar/world1-bd.json")
    cc::CertiComplex = CertiComplex(space)
    start::Point = [0.1, 0.1] 
    goal::Point = [0.9, 0.9]
    
    
    println("start to plot CertiComplex")
    ax = plot(cc)

    println("start to plan the path")
    # path, dis = plan(cc, start, goal)
    # plot(path; ax=ax, ispath=true, color="red", zorder=4)
    # println("finish plotting")
    
end

test_cc()


