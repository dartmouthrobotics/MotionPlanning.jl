# Test penetration depth
using EnhancedGJK
import StaticArrays: SVector
import CoordinateTransformations: IdentityTransformation, Translation

# Construct two geometries: a simplex and a single point:
simplex0 = SVector{2}(SVector{2, Float64}[[0.0, 0.5], [1.0, 0.5]])
simplex1 = SVector{2}(SVector{2, Float64}[[1.0, 0.5], [1.0, 0.7]])
simplex2 = SVector{2}(SVector{2, Float64}[[1.0, 0.7], [0.0, 0.7]])
simplex3 = SVector{2}(SVector{2, Float64}[[0.0, 0.7], [0.0, 0.5]])
simplex = SVector{4}(SVector{2, Float64}[[0.0, 0.5], [1.0, 0.5], [1.0, 0.7], [0.0, 0.7]])

# pt = SVector(0.5, 0.6)
pt = SVector(0.75, 0.6)
line = [[0.5, 0.4], [0.5, 0.8]]
spoint= SVector{2}([SVector{2, Float64}(p) for p in line])
simplex111 = SVector{2}(SVector{2, Float64}[[0.5, 0.4], [0.5, 0.8]])
cache = CollisionCache(simplex, pt);
result = gjk!(cache, IdentityTransformation(), IdentityTransformation())
println(result.in_collision)
println(simplex_penetration_distance(result))
if !result.in_collision
    println("dis: ", separation_distance(result))
end 


println("the closest point is: ")
close_point = closest_point_in_world(result)
println(close_point)

# The CollisionCache stores both geometries and also remembers
# information about the GJK simplex used to check for collisions
# between them. Using the same cache later will make subsequent
# computations faster.


# Run the GJK algorithm. Each geometry can also be given a
# transformation to describe its position and orientation in the
# world frame.


# Check whether the geometries are in collision:

# If the geometries are not in collision, an accurate distance between
# # the objects can be computed using
# @show separation_distance(result)

# # If the geometries *are* in collision, the GJK algorithm by itself cannot
# # compute an accurate measure of penetration distance. However, we can at
# # least obtain the distance to the nearest face of the simplex used in the
# # GJK algorithm to prove penetration, which is an underestimate of penetration
# # distance and can in certain cases be used as a proxy for it. This value can
# # be obtained using
# # simplex_penetration_distance(result)

# # We can perturb one of the geometries by changing its transformation.
# # Reusing the same cache will make this computation faster, expecially
# # for complex geometries when the change in transformation is small.
# result = gjk!(cache, Translation(SVector(0.1, 0)), IdentityTransformation())

# @show result.in_collision
# @show separation_distance(result)
