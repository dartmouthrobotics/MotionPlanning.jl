#=
 * File: prm.jl
 * Project: planners
 * File Created: Tuesday, 13th August 2019 11:12:16 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Friday, 17th April 2020 3:27:12 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

using Test
import PyPlot

using MetricTools, MetricSpaces.Planar

using
    MotionPlanning,
    MotionPlanning.PRMs,
    MotionPlanning.VisibilityGraphs

function create_simple_prm()
    space::PlanarSpace = PlanarSpace(filename="data/input/spaces/planar/world-maze.json")
    n::Integer = 20000
    k::Integer = round(Int, log2(n))
    prm = PRM(space; k=k)
    grow!(prm, n)
    println("PRM Constructed.")
    println("Memcost (FPs): ", memcost(n, k; d=2))
    return space, prm
end

memcost(n::Integer, k::Integer; d::Integer=2)::Integer = n * d + n * k * 3/2.0

function test_prm()
    println("\nTesting PRM...")
    start::Point = [0.95, 0.95]
    goal::Point = [0, 0]
    space, prm = create_simple_prm()

    println("Planning using PRM...")
    path = plan(prm, start, goal)
    print("distance is: ", metric(space, path))
    println("Plotting...")
    # plot(prm; ax=ax)
    plot(space.obstacles)
    plot(path; ispath=true, color="red", zorder=4)
    plot(distfunc(prm, goal))
    return true
end

function test_prm_error()
    println("\nTesting PRM Error...")
    start::Point = [0.9, 0.9]
    goal::Point = [0, 0]
    space, prm = create_simple_prm()
    vg = VisibilityGraph(space)

    prm_distfunc = @time distfunc(prm, goal)
    vg_distfunc = @time distfunc(vg, goal)
    fig, ax = PyPlot.subplots()
    plot(prm_distfunc)
    fig, ax = PyPlot.subplots()
    plot(vg_distfunc)
    fig, ax = PyPlot.subplots()
    @time plot(prm_distfunc, vg_distfunc; ax=ax)
    plot(space; ax=ax)
    return true
end

# @testset "PRM Tests" begin
#     PyPlot.close("all")
#     @test test_prm()
#     # @test test_prm_error()
# end

test_prm()