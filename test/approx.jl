#=
 * File: regressors.jl
 * Project: common
 * File Created: Wednesday, 2nd October 2019 12:13:00 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Saturday, 25th April 2020 1:21:29 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#  

#region imports
using Test
using Flux
import JLD, PyPlot

using MetricTools, MetricRegressors
using
    MetricSpaces,
    MetricSpaces.Planar,
    MetricSpaces.Samplers,
    MetricSpaces.PLRs,
    MetricSpaces.Analysis

if false
    include("../src/MotionPlanning.jl")
    using
        .MotionPlanning.VisibilityGraphs
else
    using
    MotionPlanning.VisibilityGraphs
end
#endregion

function l2origin(point::Point)::Float64
    return sqrt(sum((point).^2))
end

function test_approxes()
    origin::Point = [0, 0]
    space::Space = PlanarSpace(filename="./data/input/spaces/planar/world1.json")
    points::Array{Point} = grid(space, 32^2)
    values::Array{Float64} = [l2origin(p) for p in points]
    avgerr::Float64 = 0
    
    vg::VisibilityGraph = VisibilityGraph(space)
    len::Integer = 100
    truedistf = distfunc(vg, [0., 0.]; precompute=true, length=len)
    metricf(p) = metric(space, origin, p)
    
    regressors::Array{Regressor} = [
        # LinearRegressor(),
        # GaussianRegressor(),
        # NeuralNetRegressor(;
        #     model=Chain(
        #         f64(Dense(2, 8, relu)),
        #         f64(Dense(8, 8, relu)),
        #         f64(Dense(8, 8, relu)),
        #         f64(Dense(8, 1))),
        #     epochs=150),
        PLR(space; maxdepth=12, maxerror=0.0001),
        # XGBoostRegressor(; epochs=20, maxdepth=3)
    ]

    for regressor in regressors
        println("Regressor: $(typeof(regressor))")
        println("    Training with $(length(points)) points and $(length(values)) values...")

        @time train!(regressor, points, values)
        PyPlot.subplots()
        plot(space)
        plot(truedistf, distfunc(regressor); length=len, cmin=0.0, cmax=0.25, cnuml=0)
        # plot(points)
        avgerr = avgerror(space, regressor, metricf)
        numparams = getnumparams(regressor)
        println("    Avg. Error = $avgerr")
        println("    Num params = $numparams")

        if regressor isa NeuralNetRegressor continue end
        dir::String = "./data/output/models/"
        mkpath(dir)
        file::String = "$(typeof(regressor)).jld"
        
        println("    Saving to $dir$file...")
        JLD.save(dir * file, "gp", regressor)
        println("    Saved.")
    end
end

PyPlot.close("all")
test_approxes()
