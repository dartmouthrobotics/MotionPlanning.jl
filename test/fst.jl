#=
 * File: fst.jl
 * Project: planners
 * File Created: Tuesday, 13th August 2019 11:12:16 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 22nd April 2020 8:58:23 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

#region imports
using Test, Revise
import PyPlot

using MetricTools, MetricRegressors
using MetricSpaces, MetricSpaces.Samplers, MetricSpaces.Planar, MetricSpaces.Arms

using
    MotionPlanning,
    MotionPlanning.FSTs,
    MotionPlanning.VisibilityGraphs
#endregion imports

fst_fps(n::Integer, d::Integer) = 2*n*(d+1)

function test_fst_path(fst::FST, space::Space, goal::Point; cmax::Float64=1.5)
    path = @time plan(fst, goal; useopt=true)
    display(path)

    if space isa ArmSpace
        plot(space, path)
    end

    if space isa PlanarSpace
        centers = plan(fst, goal; useopt=false)
        println("centers: ", metric(fst.space, centers))
        plot(fst; cmax=cmax)
        plot(path; ispath=true, color="red", zorder=4)
        plot(centers; ispath=true, color="blue", zorder=4)
        for center in centers plot(center, fst.fields[center].radius) end
    end
    
    println("metric: ", metric(fst.space, path))
    println("predict: ", predict(fst, goal))
end

function test_fst_error(fst::FST, space::Space, goal::Point)
    # for comparison
    length::Integer = 200
    vg = VisibilityGraph(space)
    
    fst_distfunc = distfunc(fst)
    vg_distfunc = distfunc(vg, fst.start; length=length)

    # Method to remove white blothes around obstacles
    prev = 0
    function cvg_distfunc(p::Point)
        val = vg_distfunc(p)
        if isinf(val) return prev end
        prev = val
        return val
    end

    println("Got vg distance function.")
    
    fig, ax = PyPlot.subplots()
    ax.set_aspect("equal")
    plot(space)
    plot(fst_distfunc, cvg_distfunc; length=length)

    fig, ax = PyPlot.subplots()
    ax.set_aspect("equal")
    plot(space)
    plot(fst_distfunc; length=length, cmax=1.45)

    fig, ax = PyPlot.subplots()
    ax.set_aspect("equal")
    plot(space)
    plot(cvg_distfunc; length=length, cmax=1.45)

    return true
end

function setup(;
        verbose=false::Bool,
        dirname="default"::String,
        space::Space,
        start::Point=Float64[0, 0],
        goal::Point=Float64[0.49, 0.49],
        n::Integer=50^2,
        minr::Float64=0.03,
        maxr::Float64=0.06,
        regressortype=:linear::Symbol)::Tuple{FST, Space, Point}
        
    # create the FST
    fst::FST = @time FST(space, n, start;
        minr=minr, maxr=maxr,
        sampler=grid, verbose=verbose)
    return fst, space, goal
end

maze_setup(; verbose::Bool=false) =
    setup(
        verbose=verbose,
        space=PlanarSpace(; filename="data/input/spaces/planar/world-maze.json"),
        start=Float64[0, 0],
        goal=Float64[0.49, 0.49],
        n=90^2,
        # best: maxr=0.04
        minr=0.04,
        maxr=0.06)

halfdoor_setup(; verbose::Bool=false) =
    setup(
        verbose=verbose,
        space=PlanarSpace(;
        # filename=""),
        filename="data/input/spaces/planar/world-halfdoor.json"),
        start=Float64[0, 0],
        goal=Float64[1, 0],
        n=30^2,
        minr=0.1,
        maxr=0.1)

split_setup(; verbose::Bool=false) =
    setup(
        verbose=verbose,
        space=PlanarSpace(; filename="data/input/spaces/planar/world-split.json"),
        start=Float64[0, 0],
        goal=Float64[1, 1],
        n=30^2,
        minr=0.02,
        maxr=0.08)

arm2_setup(; verbose::Bool=false) =
    setup(
        verbose=verbose,
        space=ArmSpace(
            "data/input/spaces/arms/arms2.json", 
            "data/input/spaces/planar/world2.json"),
        start=Float64[0, 0],
        goal=Float64[pi/2, 0],
        n=40^2,
        minr=0.1,
        maxr=0.25)

arm3_setup(; verbose::Bool=false) =
    setup(
        verbose=verbose,
        space=ArmSpace(
            "data/input/spaces/arms/arms3.json", 
            "data/input/spaces/planar/world4.json"),
        start=Float64[0, 0, 0],
        goal=Float64[pi/2, 0, 0],
        n=75^3,
        minr=0.2,
        maxr=0.2)

# test_fst_path(maze_setup(; verbose=false)...; cmax=4.5)
# test_fst_path(halfdoor_setup(; verbose=false)...; cmax=1.5)
# test_fst_path(split_setup(; verbose=false)...; cmax=1.5)
test_fst_path(arm2_setup(; verbose=false)...)
# test_fst_path(arm3_setup(; verbose=true)...)
