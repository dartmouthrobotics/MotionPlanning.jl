#=
 * File: arms.jl
 * Project: test
 * File Created: Sunday, 8th December 2019 4:05:45 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 26th April 2020 10:03:50 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#
using MetricTools, MetricRegressors
using
    MetricSpaces,
    MetricSpaces.Arms,
    MetricSpaces.Samplers,
    MetricSpaces.BinaryPartitions,
    MetricSpaces.PLRs
    

if false    # fix linter
    include("../src/MotionPlanning.jl")
    using
        .MotionPlanning,
        .MotionPlanning.PRMs,
        .MotionPlanning.SCDM
else
    using
        MotionPlanning,
        MotionPlanning.PRMs,
        MotionPlanning.SCDM
end


function testprm2D()
    space::ArmSpace = ArmSpace(
        "data/input/spaces/arms/arms2.json", 
        "data/input/spaces/planar/world2.json"
    )
    n::Integer = 40^2
    k::Integer = round(Int, log(2, n))
    prm::PRM = PRM(space, sampler=grid, k=k)
    start::Point = [0, 0]
    goal::Point = [pi/2, 0]
    
    grow!(prm, n)
    path::Array{Point} = plan(prm, start, goal)
    println(metric(space, path))
    # plot(prm)
    # plot(path; ispath=true, color="red", zorder=4)
    plot(space, path; filename="prm2D-1")
    
    total_FP_prm = total_FP_of_prm(n,space.dim)
    println("Total_FP of prm* is: ", total_FP_prm)
end

function testprm3D()
    space::ArmSpace = ArmSpace(
        "data/input/spaces/arms/arms3-2.json", 
        "data/input/spaces/planar/world-slot.json"
    )

#     path = [[1.5707963267948966, 0.0, 0.0], [1.4660765716752362, 0.2094395102393194, 0.2094395102393194], [1.0471975511965974, -0.20943951023931984, -0.20943951023931984], [1.4660765716752362, -0.6283185307179586, -0.6283185307179586], [1.8849555921538759, -1.0471975511965979, 
# -0.20943951023931984], [2.3038346126325147, -1.4660765716752369, -0.20943951023931984], [2.3038346126325147, -1.8849555921538759, -0.6283185307179586], [1.8849555921538759, -1.8849555921538759, -0.20943951023931984], [1.4660765716752362, -1.4660765716752369, -0.20943951023931984], [1.0471975511965974, -1.0471975511965979, -0.20943951023931984], [0.7853981633974483, -0.7853981633974483, 0.0]]

#     println(metric(space, path))
#     return
    
    n::Integer = 20^3
    k::Integer = round(Int, log(2, n))
    prm::PRM = PRM(space, sampler=grid, k=k)
    start::Point = [pi/2, 0, 0]
    goal::Point = [pi/4, -pi/4, 0]
    
    grow!(prm, n)
    path::Array{Point} = plan(prm, start, goal)

    plot(space, path)
    return
    
    # Saved path
    # space1::ArmSpace = ArmSpace(
    #     "data/input/spaces/arms/arms3.json", 
    #     "data/input/spaces/planar/world4.json"
    # )
    # path1::Array{Point} = [[0.0, 0.0, 0.0], [-0.20943951023931984, 0.2094395102393194, 0.2094395102393194], [0.2094395102393194, -0.20943951023931984, -0.20943951023931984], [0.2094395102393194, -0.6283185307179586, -0.6283185307179586], [0.6283185307179586, -1.0471975511965979, -0.6283185307179586], [1.0471975511965974, -1.4660765716752369, -0.6283185307179586], [1.4660765716752362, -1.4660765716752369, -1.0471975511965979], [1.8849555921538759, -1.4660765716752369, -1.4660765716752369], [2.3038346126325147, -1.0471975511965979, -1.4660765716752369], [2.3038346126325147, -0.6283185307179586, -1.0471975511965979], [1.8849555921538759, -0.20943951023931984, -1.0471975511965979], [1.4660765716752362, -0.20943951023931984, -0.6283185307179586], [1.4660765716752362, -0.20943951023931984, -0.20943951023931984], [1.4660765716752362, 0.2094395102393194, 0.2094395102393194], [1.5707963267948966, 0.0, 0.0]]

    # total_FP_prm = total_FP_of_prm(n,space.dim)
    # println("Total_FP of prm* is: ", total_FP_prm)
end

function testscdm2D()
    space::ArmSpace = ArmSpace(
        "data/input/spaces/arms/arms2.json", 
        "data/input/spaces/planar/world2.json"
    )
    # scdm::SCDM = SCDM()

    n_pts_per_edge::Integer = 40
    n_pts_per_edge_used_for_global_graph::Integer = 40
    depth_of_dividing_cell = 2 #depth of dividing cell

    n::Integer = n_pts_per_edge^2    # number of samples per cell
    start::Point = [0, 0]
    goal::Point = [pi/2, 0] 

    r_epoch = 1000  ## Used for xgboost regressor
    clf = XGBoostRegressor(; epochs=200, maxdepth=3)  ## Only xgboost is used for classifier
    
    use_2D_rplr = false
    use_nD_rplr = true

    reg = use_2D_rplr ? 
            PLR(
                [Bound(0, n_pts_per_edge*4-4+1) for _ in 1:2];
                maxerror=0.0000001,
                maxdepth=50,
                minsamples=5) :
            (use_nD_rplr ? 
                PLR(
                    [Bound(-pi, pi) for _ in 1:space.dim*2]; maxerror=0.0000001,
                    maxdepth=50,
                    minsamples=space.dim*2+2) :
                XGBoostRegressor(; epochs=r_epoch, maxdepth=3))
    leaves = construct_scdm(
            clf,
            reg,
            depth_of_dividing_cell,
            space,
            n;
            num_pts_per_edge=n_pts_per_edge_used_for_global_graph,
            use_vg_as_gt=false,
            use_localplanner=true,
            use_2D_rplr=use_2D_rplr,
            use_nD_rplr=use_nD_rplr,
            plot_rplr=false)

    global_graph = SCDM.construct_global_graph(leaves) #construct the global_graph
    path, path_dis, refined_path,refined_path_dis =  SCDM.query_scdm(start,goal,leaves,global_graph; if_refine=true)

    # path, path_dis, refined_path,refined_path_dis = SCDM.query_path_and_construct_global_graph_using_Astar(start, goal, leaves)

    push!(path, goal)
    refined_path = convert(Array{Point}, refined_path)
    plot(space, refined_path; filename="scdm2D-1")

    FP = get_memory_cost_of_SCDM(leaves, use_2D_rplr, use_nD_rplr; r_epoch=r_epoch, xgboost_tree_depth=3)
    println("total FP of scdm is: ", FP)
end

function testscdm3D()
    space::ArmSpace = ArmSpace(
        "data/input/spaces/arms/arms3.json", 
        "data/input/spaces/planar/world2.json"
    )
    # scdm::SCDM = SCDM()

    n_pts_per_edge::Integer = 40
    n_pts_per_edge_used_for_global_graph::Integer = 40
    depth_of_dividing_cell = 2 #depth of dividing cell

    n::Integer = n_pts_per_edge^3
    start::Point = [0, 0, 0]
    goal::Point = [pi/2, 0, 0]


    r_epoch = 1000  ## Used for xgboost regressor
    clf = XGBoostRegressor(; epochs=200, maxdepth=3)  ## Only xgboost is used for classifier
    
    use_2D_rplr = false
    use_nD_rplr = true

    reg = use_2D_rplr ? 
            PLR(
                [Bound(0, n_pts_per_edge*4-4+1) for _ in 1:2];
                maxerror=0.0000001,
                maxdepth=50,
                minsamples=5) :
            (use_nD_rplr ? 
                PLR(
                    [Bound(-pi, pi) for _ in 1:space.dim*2]; maxerror=0.0000001,
                    maxdepth=50,
                    minsamples=space.dim*2+2) :
                XGBoostRegressor(; epochs=r_epoch, maxdepth=3))
    leaves = construct_scdm(
            clf,
            reg,
            depth_of_dividing_cell,
            space,
            n;
            num_pts_per_edge=n_pts_per_edge_used_for_global_graph,
            use_vg_as_gt=false,
            use_localplanner=true,
            use_2D_rplr=use_2D_rplr,
            use_nD_rplr=use_nD_rplr,
            plot_rplr=false)

    # global_graph = SCDM.construct_global_graph(leaves) #construct the global_graph
    # path, path_dis, refined_path,refined_path_dis =  SCDM.query_scdm(start,goal,leaves,global_graph; if_refine=true)
    path, path_dis, refined_path,refined_path_dis = SCDM.query_path_and_construct_global_graph_using_Astar(start, goal, leaves)
    println("path is: ", path)
    println("path dis is: ", path_dis)
    println("refined_path is: ", refined_path)
    println("refined_path dis is: ", refined_path_dis)

    push!(path, goal)
    refined_path = convert(Array{Point}, refined_path)
    plot(space, refined_path; filename="scdm3D-1")

    FP = get_memory_cost_of_SCDM(leaves, use_2D_rplr, use_nD_rplr; r_epoch=r_epoch, xgboost_tree_depth=3)
    println("total FP of scdm is: ", FP)
end

# testprm2D()
testprm3D()
# testscdm2D()
# testscdm3D()
# @suppress begin testscdm3D() end

