#=
 * File: plr.jl
 * Project: test
 * File Created: Sunday, 8th December 2019 4:11:14 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 26th February 2020 4:15:46 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#
import JSON2
using MetricTools, MetricTools.MetricGraphs
using MetricRegressors
using MetricSpaces, MetricSpaces.Planar, MetricSpaces.Samplers, MetricSpaces.PLRs

if false    # fix linter
    include("../src/MotionPlanning.jl")
    using
        .MotionPlanning,
        .MotionPlanning.PRMs,
        .MotionPlanning.VisibilityGraphs
else
    using
        MotionPlanning,
        MotionPlanning.PRMs,
        MotionPlanning.VisibilityGraphs
end

function test_rplr()
    space::Space = PlanarSpace(
        ; filename="data/input/spaces/planar/world-maze.json"
        )
    prm::PRM = PRM(space)
    grow!(prm, 2000)
    vg::VisibilityGraph = VisibilityGraph(space)
    
    length::Integer = 128
    start::Point = [0, 0]
    distf::Function = distfunc(prm, start)
    tdistf::Function = distfunc(vg, start; length=length)
    X::Array{Point} = gridl(space, length)
    
    Y::Array{Float64} = [tdistf(x) for x in X]

    plr::PLR = PLR(space; maxdepth=50, maxerror=0.000001, minsamples=5)
    plrdistf::Function = distfunc(plr)
    
    println("Training...")
    train!(plr, X, Y)

    println("Plotting...")
    plot(plr; clevels=0, flevels=20, truedistf=tdistf, length=length, lines=true)
    plot(space)
end

function test_prm()
end

function test_vg()
end

test_rplr()
