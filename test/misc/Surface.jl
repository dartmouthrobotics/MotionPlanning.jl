#=
 * File: Surface.jl
 * Project: misc
 * File Created: Sunday, 22nd March 2020 2:07:17 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 22nd March 2020 6:03:44 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#
using MetricTools

using Polyhedra: HalfSpace
using QHull

struct Surface
    vertices::Array{Point}
    hs::HalfSpace
end
