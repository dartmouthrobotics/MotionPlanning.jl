#=
 * File: reedsshepp.jl
 * Project: test
 * File Created: Friday, 27th December 2019 5:43:21 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 26th April 2020 9:07:49 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

import PyPlot
using MetricTools, MetricRegressors
using
    MetricSpaces,
    MetricSpaces.ReedsShepp,
    MetricSpaces.Samplers,
    MetricSpaces.BinaryPartitions,
    MetricSpaces.PLRs
using
    MotionPlanning,
    MotionPlanning.PRMs,
    MotionPlanning.RRTs,
    MotionPlanning.FSTs,
    MotionPlanning.SCDM


function testfst()
    space::ReedsSheppSpace = ReedsSheppSpace(;
        filename="data/input/spaces/planar/angled-doors.json",
        r=0.5
    )
    start::Point = [0, 0, 0]
    goal::Point = [0.9, 0.9, pi/2]
    
    n::Integer=30^3
    
    fst::FST = FST(space, n, start;
        minr=0.35, maxr=0.35,
        sampler=grid, verbose=true)

    path = plan(fst, goal; useopt=true)
    display(path)
    
    plot(space, path)
    println("metric cost: ", metric(space, path))
    show(path)
    
    PyPlot.xlim(-0.1, 1.1)
    PyPlot.ylim(-0.1, 1.1)
    PyPlot.gca().set_aspect("equal")
    println("Done.")
end

function testrrt()
    space::ReedsSheppSpace = ReedsSheppSpace(;
        filename="data/input/spaces/planar/angled-doors.json",
        r=0.5
    )
    start::Point = [0, 0, 0]
    goal::Point = [0.9, 0.9, pi/2]
    
    rrt::RRTstar = RRTstar(space, start; step=0.3, radius=0.5)
    path::Array{Point} = plan(rrt, start, goal; n=25^3)
    
    plot(space, path)
    println("metric cost: ", metric(space, path))
    show(path)
    
    PyPlot.xlim(-0.1, 1.1)
    PyPlot.ylim(-0.1, 1.1)
    PyPlot.gca().set_aspect("equal")
    println("Done.")
end

function testprm()
    space::ReedsSheppSpace = ReedsSheppSpace(;
        filename="data/input/spaces/planar/angled-doors.json",
        r=0.5
    )
    n::Integer = 20^3
    k::Integer = round(Int, log(2, n))
    prm::PRM = PRM(space, sampler=uniform_random, k=10 * k)
    start::Point = [0, 0, 0]
    goal::Point = [0.9, 0.9, pi/2]
    
    @time grow!(prm, n)
    path::Array{Point} = @time plan(prm, start, goal)

    # plot(prm)
    # plot(path; ispath=true, color="red", zorder=4)
    plot(space, path)
    println("metric cost: ", metric(space, path))
    show(path)

    total_FP_prm = total_FP_of_prm(n, space.dim)
    println("Total_FP of prm* is: ", total_FP_prm)
    PyPlot.xlim(-0.1, 1.1)
    PyPlot.ylim(-0.1, 1.1)
    PyPlot.gca().set_aspect("equal")
    println("Done.")
end

function testrc()
end

testprm()
# testfst()
# testrrt()
# testscdm()
