#=
 * File: visibility.jl
 * Project: planners
 * File Created: Tuesday, 13th August 2019 11:12:16 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 20th April 2020 10:10:05 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

#region imports
using Test, Revise
import PyPlot
using MetricTools
using MetricSpaces.Planar

using
    MotionPlanning,
    MotionPlanning.VisibilityGraphs
#endregion imports

function testvg()
    space::PlanarSpace = PlanarSpace(;
        filename="data/input/spaces/planar/world-split.json", triangulate=false)
    vg = VisibilityGraph(space)
    
    start = [0.0, 0.0]
    goal = [1.0, 1.0]
    
    # start::Point = [0.5, 0.4375]
    # goal::Point = [0.5, 0.1875]
    path = plan(vg, start, goal)
    display(path)

    fig, ax = PyPlot.subplots()
    plot(vg; ax=ax)
    plot(path; ax=ax, ispath=true, color="red", zorder=4)
    
    # println("\nWith precompute:")
    # length::Integer = 100
    # @time dfo = distfunc(vg, goal; length=length)
    # @time plot(dfo; bounds=space.bounds, ax=ax, length=length, flevels=70, clevels=0)
    return true
end

function testvgbounds()
    space::PlanarSpace = PlanarSpace(;
        bounds=[Bound(0., 1.), Bound(0., 1.)],
        filename="data/input/spaces/planar/world-maze.json")
    vg = VisibilityGraph(space)

    start = [0.85, 0.7]
    goal = [0.75, 0.7]
    path = plan(vg, start, goal)

    fig, ax = PyPlot.subplots()
    plot(vg; ax=ax)
    plot(path; ax=ax, ispath=true, color="red", zorder=4)
    
    println("\nWith precompute:")
    length::Integer = 100
    @time dfo = distfunc(vg, goal; length=length)
    @time plot(dfo; bounds=space.bounds, ax=ax, length=length, flevels=70, clevels=Float64[])
    return true
end


@testset "Visibility Graph Tests" begin
    PyPlot.close("all")
    @test testvg()
    # @test testvgbounds()
end
