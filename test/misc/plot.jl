#=
 * File: plot.jl
 * Project: test
 * File Created: Monday, 6th January 2020 7:28:23 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 6th January 2020 8:02:46 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

import JSON2, PyCall, PyPlot
using MetricTools
using DataStructures

"""
Creates a bar plot using PyPlot
"""
function barplot(labels::Array{String}, values::Array{<:Real};
        filename="bar"::String)
    PyPlot.close("all")
    PyPlot.rc("font", family="serif", size=12)
    index = [i-1 for i in eachindex(labels)]
    PyPlot.bar(index, values, align="center", alpha=0.5)
    PyPlot.xticks(index, labels)
    PyPlot.ylabel("")
    PyPlot.title(filename)
    ax = PyPlot.gca()
    for (i, v) in enumerate(values)
        ax.text(v + 3, i + .25, "$v")
    end

    dir::String = "data/output/images/"
    mkpath(dir)
    PyPlot.tight_layout()
    PyPlot.savefig("$dir$filename.png")
end

function MetricTools.plot(filename::String)
    filestr::String = open("data/output/images/results.json") do file
        read(file, String)
    end
    results = JSON2.read(filestr)
    
    fielddict::DefaultDict = DefaultDict(() -> [])
    for (name, result) in zip(keys(results), results)
        for field in keys(result)
            push!(fielddict[string(field)], (string(name), getfield(result, field)))
        end
    end
    println(fielddict)
    
    for (field, tuples) in fielddict
        barplot(
            [tuple[1] for tuple in tuples],
            [tuple[2] for tuple in tuples];
            filename=field)
    end
end

plot("data/output/results.json")
