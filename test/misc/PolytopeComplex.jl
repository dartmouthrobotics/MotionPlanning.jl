#=
 * File: PolytopeComplex.jl
 * Project: misc
 * File Created: Sunday, 22nd March 2020 2:08:22 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 22nd March 2020 10:36:33 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

import PyPlot
using ProgressMeter
using Polyhedra
using QHull

using MetricTools, MetricTools.Geometry, MetricTools.KNNs
using MetricSpaces, MetricSpaces.Samplers

using MetricSpaces.Planar

include("Surface.jl")
include("./PolytopeCell.jl")

mutable struct PolytopeComplex
    space::Space
    cells::Array{PolytopeCell}
    vertices::Dict{Point, Array{PolytopeCell}}
    open::Set{Point}
    knn::BallTreeKNN
    radius::Float64

    function PolytopeComplex(space::Space; n::Integer=100, radius::Float64=0.2)
        points::Array{Point} = uniform_random(space, n) #grid(space, n)
        pc::PolytopeComplex = new(
            space,
            PolytopeCell[],
            Dict{Point, PolytopeCell}(),
            Set{Point}(points),
            BallTreeKNN(space.dim; points=points, metric=dmetric(space)),
            radius
        )
        construct!(pc)
        return pc
    end
end

@inline unassigned(pc::PolytopeComplex, p::Point) =
    !(p in pc.open || p in keys(pc.vertices))

"""
Checks if a point can join an entire clique
"""
function MetricSpaces.localplanner(pc::PolytopeComplex, c::Polygon, p::Point)::Bool
    for v in c
        if !localplanner(pc.space, p, v) return false end
    end
    
    return true
end

function MetricTools.plot(pc::PolytopeComplex, i::Integer)
    colors::Array{String} = [
        "red", "purple", "indigo", "blue", "green", "yellow", "orange"
    ]
    color = colors[i % length(colors) + 1]
    poly::Polygon = pc.cells[i].vertices
    polys::Array{Polygon} = [poly]
    plot(polys; facecolor=color, alpha=0.3, edgecolor=color, points=true)
end

function MetricTools.plot(pc::PolytopeComplex)
    PyPlot.clf()
    plot(pc.space)
    plot(collect(pc.open))
    plot(collect(keys(pc.vertices)))
    
    for i in eachindex(pc.cells)
        plot(pc, i)
    end
end

function Base.push!(pc::PolytopeComplex, cell::PolytopeCell)
    push!(pc.cells, cell)
    for p in cell.vertices
        if !(p in keys(pc.vertices)) pc.vertices[p] = PolytopeCell[] end
        push!(pc.vertices[p], cell)
    end
end

function buildcerts!(pc::PolytopeComplex; verbose::Bool=false)
    pointcerts::Array{Tuple} = [(p, cert(pc.space, p)) for p in pc.open]
    getcert(t) = t[2]
    sort!(pointcerts; by=getcert, rev=true)
    
    for (point, certif) in pointcerts
        if !(point in pc.open) continue end
        
        certified::Array{Point} = [
            p for p in inradius(pc.knn, point, certif)
            if p in pc.open]
        if length(certified) < pc.space.dim + 1 continue end

        # Check if this polyhedron collides with nearby ones
        cell::PolytopeCell = PolytopeCell(certified)
        cells::Set{PolytopeCell} = nearbycells(pc, point; verbose=verbose)
        if anyintersect(cell, cells) continue end
        
        push!(pc, cell)
        
        # Remove all interior points
        for p in certified
            pop!(pc.open, p)
        end
    end
end

function sortedv_inradius(pc::PolytopeComplex, point::Point)::Array{Point}
    neighbors::Array{Point} = [
        p for p in inradius(pc.knn, point, pc.radius)
        if p in keys(pc.vertices)]
    sort!(neighbors, by=(p) -> metric(pc.space, point, p), rev=false)
    return neighbors
end

function nearbycells(pc::PolytopeComplex, point::Point; verbose::Bool=false)::Set{PolytopeCell}
    vertices::Array{Point} = sortedv_inradius(pc, point)
        
    if verbose
        println("    Connecting $point...")
        println("    Got $(length(vertices)) neighboring vertices...")
        plot(pc)
        plot(point; color="red")
        plot(vertices; color="blue")
        if readline() == "x" return end
    end
    
    # Get all cells these vertices are in
    cells::Set{PolytopeCell} = Set{PolytopeCell}()
    for v in vertices
        push!(cells, pc.vertices[v]...)
    end

    return cells
end

function connectopen!(pc::PolytopeComplex; verbose::Bool=false)
    if verbose println("Connecting open!") end
    while !isempty(pc.open)
        point::Point = pop!(pc.open)
        cells::Set{PolytopeCell} = nearbycells(pc, point; verbose=verbose)
        
        # Try adding to each nearest cell
        if verbose println("    Trying to add each cell...") end
        for cell in cells
            if verbose println("        Trying cell $cell...") end
            if !localplanner(pc, cell.vertices, point) continue end
            
            testcell::PolytopeCell = PolytopeCell(Point[cell.vertices..., point])
            if anyintersect(testcell, cells; exclude=PolytopeCell[cell]) continue end
            
            if verbose println("       No intersections. Pushing point...") end
            # If no intersections, add self to cell and stop searching
            push!(pc, cell, point; removecovered=true)
            break
        end
    end
    
    if verbose println("Open connected!") end
end

function Base.push!(pc::PolytopeComplex, cell::PolytopeCell, p::Point; verbose::Bool=false, removecovered::Bool=true)
    verbose && println("        Merging into cell...")
    previous::Array{Point} = [cell.vertices..., p]
    push!(cell, p)

    verbose && display(previous)
    verbose && display(cell.vertices)

    if removecovered
        covered::Array{Point} = [p for p in setdiff(previous, cell.vertices)
            if !on_surface(cell, p)]

        verbose && println("        Vertices covered: $covered")
        for c in covered
            delete!(pc.vertices, c)
        end
    end

    if !(p in keys(pc.vertices)) pc.vertices[p] = PolytopeCell[] end
    push!(pc.vertices[p], cell)
end

"""
To connect the cells together
"""
function connectcells!(pc::PolytopeComplex; verbose::Bool=false)
    verbose && println("Connecting cells!")
    queue::Array{Tuple{PolytopeCell, Surface}} = []
    visited::Set{Array{Point}} = Set{Array{Point}}()

    @inline pushsurfaces!(
            queue::Array{Tuple{PolytopeCell, Surface}},
            cell::PolytopeCell) = 
        pushfirst!(queue, [(cell, surface) for surface in get_surfaces(cell) if !(surface.vertices in visited)]...)
    
    for cell in pc.cells
        pushsurfaces!(queue, cell)
    end

    while !isempty(queue)
        cell::PolytopeCell, surface::Surface = pop!(queue)
        if surface.vertices in visited continue end
        if !on_surface(cell, surface) continue end
        
        verbose && println("    Connecting surface $(surface.vertices)...")
        
        push!(visited, surface.vertices)

        if verbose
            plot(pc)
            plot(surface.vertices; color="red")
            if readline() == "x" return end
        end
        
        center::Point = sum(surface.vertices) ./ length(surface.vertices)
        neighbors::Array{Point} = sortedv_inradius(pc, center)
        for n in neighbors
            # if n in cell.vertices continue end
            if n in surface.hs continue end
            
            if verbose 
                plot(n; color="blue")
                println("        n: $n")
                if readline() == "x" return end
            end

            # If we can merge into current cell
            if localplanner(pc, cell.vertices, n)
                testcell::PolytopeCell = PolytopeCell(Point[cell.vertices..., n])
                if nearbyintersect(pc, cell, testcell, n) continue end
                
                push!(pc, cell, n; removecovered=true)
                pushsurfaces!(queue, cell)
                break

            # Otherwise we build a new cell
            elseif localplanner(pc, surface.vertices, n)
                testcell = PolytopeCell(Point[surface.vertices..., n])
                if nearbyintersect(pc, cell, testcell, n) continue end

                push!(pc, testcell)
                pushsurfaces!(queue, testcell)
                break
            end
        end
    end
end

function nearbyintersect(pc::PolytopeComplex, cell::PolytopeCell, testcell::PolytopeCell, p::Point;
        verbose::Bool=false)::Bool
    othercells::Set{PolytopeCell} = nearbycells(pc, p)
    return anyintersect(testcell, othercells;
        verbose=verbose, exclude=PolytopeCell[cell, testcell])
end

function construct!(pc::PolytopeComplex)
    buildcerts!(pc)
    plot(pc)
    readline()

    connectopen!(pc; verbose=false)
    plot(pc)
    readline()
    
    connectcells!(pc; verbose=false)
end

function test_pc()
    space::Space = PlanarSpace(; filename="data/input/spaces/planar/world1.json")
    pc::PolytopeComplex = PolytopeComplex(space; n=25^2, radius=0.3)
    plot(pc)
end

test_pc()
