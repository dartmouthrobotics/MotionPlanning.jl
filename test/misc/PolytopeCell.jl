#=
 * File: PolytopeCell.jl
 * Project: misc
 * File Created: Sunday, 22nd March 2020 1:48:26 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 22nd March 2020 10:03:03 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#
using CDDLib
using Polyhedra: removevredundancy!
using MetricTools

@inline validpolyhedron(c::Polygon)::Bool =
    !isempty(c) && length(c) > length(first(c))
@inline topolyhedron(p::Polygon)::Polyhedron =
    polyhedron(vrep(hcat(p...)'), CDDLib.Library())
    # polyhedron(vrep(hcat(p...)'))
@inline topointlist(p::Polyhedron)::Polygon =
    [pt for pt in points(vrep(p))]

mutable struct PolytopeCell
    vertices::Array{Point}
    polyhedron::Polyhedron

    function PolytopeCell(vertices::Array{Point})
        polyhedron::Polyhedron = topolyhedron(vertices)
        cell::PolytopeCell = new(topointlist(polyhedron), polyhedron)
        removevredundancy!(cell)
        return cell
    end
end

function Base.:(==)(cell1::PolytopeCell, cell2::PolytopeCell)::Bool
    if length(cell1.vertices) != length(cell2.vertices) return false end
    for i in eachindex(cell1.vertices)
        if cell1.vertices[i] != cell2.vertices[i] return false end
    end
    
    return true
end

function Base.string(pc::PolytopeCell)::String
    return "PolytopeCell($(pc.vertices))"
end

function Base.show(op::IO, pc::PolytopeCell)
    println(op, string(pc))
end

function Polyhedra.removevredundancy!(cell::PolytopeCell)
    # computehrep!(cell.polyhedron)
    removevredundancy!(cell.polyhedron)
    cell.vertices = topointlist(cell.polyhedron)
end

# TODO speed up
function Base.push!(cell::PolytopeCell, point::Point)
    push!(cell.vertices, point)
    cell.polyhedron = topolyhedron(cell.vertices)
    removevredundancy!(cell)
end

function hasintersect(cell1::PolytopeCell, cell2::PolytopeCell;
        verbose::Bool=false,
        exclude::Array{PolytopeCell}=PolytopeCell[])::Bool

    for x in exclude
        if cell2 == x return false end
    end

    ipolyhedron::Polyhedron = intersect(cell1.polyhedron, cell2.polyhedron)
    ipoints::Array{Point} = topointlist(ipolyhedron)

    if verbose
        plot(Polygon[cell1.vertices]; alpha=0.3)
        plot(Polygon[cell2.vertices]; alpha=0.3)
        readline()
    end


    return validpolyhedron(ipoints)
end

function anyintersect(cell::PolytopeCell,
        cells::Union{Set{PolytopeCell}, Array{PolytopeCell}};
        exclude::Array{PolytopeCell}=PolytopeCell[cell], verbose::Bool=false)::Bool
    for other in cells
        verbose && println("            Checking other cell...")
        
        if hasintersect(cell, other; exclude=exclude) 
            if verbose
                println("intersect between $cell and $other")
                plot(Polygon[cell.vertices]; alpha=0.2)
                plot(Polygon[other.vertices]; alpha=0.2)
                readline()
            end
            
            return true
        end
    end
    
    verbose && println("No intersections")
    return false
end

function get_surfaces(cell::PolytopeCell)::Array{Surface}
    surfaces::Array{Surface} = []

    for hs in halfspaces(hrep(cell.polyhedron))
        hp = HyperPlane(hs.a, hs.β)
        vertices::Polygon = Point[p for p in cell.vertices if p in hp]
        surface::Surface = Surface(vertices, hs)
        push!(surfaces, surface)
    end

    return surfaces
end

function on_surface(cell::PolytopeCell, p::Point)::Bool
    for hs in halfspaces(hrep(cell.polyhedron)) 
        hp = HyperPlane(hs.a, hs.β)
        if !(p in hp) return false end
    end

    return true
end

function on_surface(cell::PolytopeCell, s::Surface)::Bool
    for hs in halfspaces(hrep(cell.polyhedron))
        if s.hs.a == hs.a && s.hs.β == hs.β return true end
    end

    return false
end
