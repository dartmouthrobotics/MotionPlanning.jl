#=
 * File: simplex.jl
 * Project: test
 * File Created: Saturday, 21st March 2020 11:18:38 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 22nd March 2020 1:27:48 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

import PyPlot
using ProgressMeter
using Polyhedra
using QHull

using MetricTools, MetricTools.Geometry, MetricTools.KNNs
using MetricSpaces, MetricSpaces.Samplers

using MetricSpaces.Planar

const Clique = Array{Point}

@inline validpolyhedron(c::Clique)::Bool = length(c) > length(first(c))
# @inline topolyhedron(c::Clique)::Polyhedron = polyhedron(vrep(hcat(c...)'), QHull.Library())
@inline topolyhedron(p::Polygon)::Polyhedron = polyhedron(vrep(hcat(p...)'))
@inline topointlist(p::Polyhedron)::Polygon = [pt for pt in eachslice(vrep(p).V; dims=1)]

struct Surface
    vertices::Array{Point}
    hs::HalfSpace
end

Base.isequal(s1::Surface, s2::Surface)::Bool = s1.vertices == s2.vertices
function Base.:(==)(s1::Surface, s2::Surface)::Bool 
    println("surface == called!")
    return s1.vertices == s2.vertices
end

mutable struct CliqueComplex
    space::Space
    cliques::Array{Clique}
    points::Array{Point}
    surfaces::Dict{Point, Array{Surface}}
    contained::Dict{Point, Clique}
    covered::Set{Clique}
    knn::BallTreeKNN
    radius::Float64

    function CliqueComplex(space::Space; n::Integer=100, r::Float64=0.3)
        points::Array{Point} = grid(space, n)
        display(points)
        cc::CliqueComplex = new(space,
            Polygon[],
            points,
            Dict{Point, Array{Surface}}(p => Surface[] for p in points),
            Dict{Point, Clique}(),
            Set{Surface}(),
            BallTreeKNN(space.dim; points=points, metric=dmetric(space)),
            r)
        construct!(cc)
        return cc
    end
end

"""
Checks if a point can join an entire clique
"""
function MetricSpaces.localplanner(cc::CliqueComplex, c::Clique, p::Point)::Bool
    for v in c
        if !localplanner(cc.space, p, v) return false end
    end
    
    return true
end

function MetricTools.plot(cc::CliqueComplex, i::Integer)
    colors::Array{String} = [
        "red", "purple", "indigo", "blue", "green", "yellow", "orange"
    ]
    color = colors[i % length(colors) + 1]
    poly::Polygon = collect(cc.cliques[i])
    polys::Array{Polygon} = [poly]
    plot(polys; facecolor=color, alpha=0.3, edgecolor=color, points=true)
end

function MetricTools.plot(cc::CliqueComplex)
    PyPlot.clf()
    plot(cc.space)
    plot(Point[p for p in cc.points if !(p in keys(cc.contained))])
    
    for i in eachindex(cc.cliques)
        plot(cc, i)
    end
end

function get_surfaces(clique::Clique)::Array{Surface}
    surfaces::Array{Surface} = []
    
    # remove redundant points
    # if enough points to be redundant
    if !validpolyhedron(clique)
        println("INVALID CLIQUE! $(collect(clique))")
        return surfaces
    end

    p::Polyhedron = topolyhedron(clique)
    # removevredundancy!(p)
    # clique = Clique(topointlist(p))

    return get_surfaces(clique, p)
end

function get_surfaces(c::Clique, p::Polyhedron)::Array{Surface}
    surfaces::Array{Surface} = []

    println("Collecting surfaces...")
    
    for hs in halfspaces(hrep(p))
        hp = HyperPlane(hs.a, hs.β)
        vertices::Polygon = Point[p for p in c if in(p, hp)]
        surface::Surface = Surface(vertices, hs)
        push!(surfaces, surface)
    end

    return surfaces
end

function valid_neighbor(cc::CliqueComplex, surface::Surface, p::Point)::Bool
    if p in surface.hs return false end
    if !localplanner(cc, surface.vertices, p) return false end
    if p in keys(cc.contained) return false end

    return true
end

function connect_neighbors!(cc::CliqueComplex,
        surfaces::Array{Surface},
        clique::Clique,
        polyhedron::Polyhedron)
    println("Connecting neighbors")
    
    cliqueset::Set{Point} = Set{Point}(clique)
    # Check to see if each surface can be connected to previous neighboring points
    for surface in surfaces
        for p in surface.vertices
            # Check neighboring surfaces
            for nsurface in cc.surfaces[p]
                if nsurface.vertices in cc.covered continue end
                for np in nsurface.vertices
                    if np in cliqueset continue end

                    # If a point on a neighboring surface is connectable and doesn't intersect with this clique,
                    # Create a new clique between them
                    if !localplanner(cc, surface.vertices, np)
                        println("    np $np not connectable.")
                        continue
                    end
                    
                    println("    np $np is connectable to $(surface.vertices), testing clique.")
                    nclique::Clique = copy(surface.vertices)
                    push!(nclique, np)
                    npolyhedron::Polyhedron = topolyhedron(nclique)
                    ipolyhedron::Polyhedron = intersect(npolyhedron, polyhedron)
                    iclique::Clique = topointlist(ipolyhedron)
                    println("    intersecting clique is $iclique.")
                    if validpolyhedron(iclique) continue end

                    println("    adding clique $nclique")
                    push!(cc.cliques, nclique)

                    covered::Array{Clique} = [s.vertices for s in get_surfaces(nclique, npolyhedron)]
                    push!(cc.covered, covered...)

                    # Debug
                    println("covered $covered.")
                    println("total covered: $(length(cc.covered))")
                    plot(cc)
                    plot(np; color="red")
                    plot(surface.vertices; color="blue")
                    if readline() == "x" return end
                end
            end

            # Add these new surfaces to the point-surface map
            push!(cc.surfaces[p], surface)
        end
    end
end

function clique_from_surface(cc::CliqueComplex, surface::Surface, point::Point)::Clique
    clique::Clique = copy(surface.vertices)
    push!(clique, point)
    return clique
end

function get_neighbors(cc::CliqueComplex, surface::Surface)::Array{Point}
    center::Point = sum(surface.vertices) ./ length(surface.vertices)
    neighbors::Array{Point} = [p for p in inradius(cc.knn, center, cc.radius)
        if valid_neighbor(cc, surface, p)]
    sort!(neighbors; by=(n) -> metric(cc.space, center, n))
    return neighbors
end

function check_containment(cc::CliqueComplex, points::Array{Point}, clique::Clique, polyhedron::Polyhedron)
    # Consume contained points
    for point in points
        if point in polyhedron
            cc.contained[point] = clique
        end
    end
end

function process_surface!(cc::CliqueComplex, surface::Surface)::Array{Surface}
    # Chose a point
    neighbors::Array{Point} = get_neighbors(cc, surface)
    if isempty(neighbors) return [] end
    chosen::Point = first(neighbors)    # closest neighbor
    
    # Add the point to the clique
    clique::Clique = clique_from_surface(cc, surface, chosen)
    polyhedron::Polyhedron = topolyhedron(clique)
    
    check_containment(cc, neighbors, clique, polyhedron)
    
    # Add this clique
    push!(cc.cliques, clique)
    plot(cc)
    println("Added clique $clique")
    readline()

    # Processing on new simplex's surfaces
    surfaces::Array{Surface} = [s for s in get_surfaces(clique, polyhedron) if s != surface]
    println("Number of surfaces (not include original): $(length(surfaces))")
    connect_neighbors!(cc, surfaces, clique, polyhedron)
    
    println("cliques:")
    display(cc.cliques)
    # continue algorithm on the new surfaces
    return surfaces
end

"""
Using a queue of surfaces, construct cliques outward breadth first.
Points on surfaces will be contained in two cliques
"""
function construct!(cc::CliqueComplex)
    # Get a starting simplex
    start::Polygon = nearest(cc.knn, first(cc.points), cc.space.dim + 1)
    push!(cc.cliques, start)
    surfaces::Array{Surface} = get_surfaces(start)
    for surface in surfaces
        for p in surface.vertices
            push!(cc.surfaces[p], surface)
        end
    end
    queue::Array{Surface} = [surfaces...]

    # Debug
    plot(cc)
    println("Starting construction...")
    readline()
    
    while !isempty(queue)
        surface = pop!(queue)
        if surface.vertices in cc.covered continue end
        push!(cc.covered, surface.vertices)

        surfaces = process_surface!(cc, surface)
        pushfirst!(queue, surfaces...)
        
        # Debug
        plot(cc)
        if readline() == "x" return end
    end
end

function test_cc()
    space::Space = PlanarSpace(; filename="data/input/spaces/planar/world1.json")
    cc::CliqueComplex = CliqueComplex(space; n=12^2)
    plot(cc)
end

test_cc()
