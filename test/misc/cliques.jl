#=
 * File: cliques.jl
 * Project: test
 * File Created: Saturday, 21st March 2020 11:18:38 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Saturday, 21st March 2020 4:34:18 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

import PyPlot
using ProgressMeter
using Polyhedra
using QHull

using MetricTools, MetricTools.Geometry, MetricTools.KNNs
using MetricSpaces, MetricSpaces.Samplers

using MetricSpaces.Planar

@inline validpolyhedron(c::Set{Point})::Bool = length(c) > length(first(c))
@inline topolyhedron(c::Set{Point})::Polyhedron = polyhedron(vrep(hcat(c...)'), QHull.Library())
# @inline topolyhedron(p::Polygon)::Polyhedron = polyhedron(vrep(hcat(p...)'))
@inline topointlist(p::Polyhedron)::Polygon = [pt for pt in eachslice(vrep(p).V; dims=1)]

const Clique = Set{Point}

mutable struct CliqueComplex
    space::Space
    cliques::Array{Clique}
    points::Array{Point}
    assigned::Set{Point}
    vertices::Set{Point}
    knn::BallTreeKNN
    radius::Float64

    function CliqueComplex(space::Space; n::Integer=200, r::Float64=0.2)
        points::Array{Point} = uniform_random(space, 200)
        cc::CliqueComplex = new(space,
            Polygon[],
            points,
            Set{Point}(),
            Set{Point}(),
            BallTreeKNN(space.dim; points=points, metric=dmetric(space)),
            r)
        construct!(cc)
        # triangulate!(cc)
        return cc
    end
end

@inline assigned_nonvertex(cc::CliqueComplex, p::Point)::Bool = (p in cc.assigned)
function inhalfspace(p::Point, halfspace::Union{HalfSpace, Nothing})::Bool
    if isnothing(halfspace) return false end
    return p in halfspace
end

function MetricTools.plot(cc::CliqueComplex, i::Integer)
    colors::Array{String} = [
        "red", "purple", "indigo", "blue", "green", "yellow", "orange"
    ]
    color = colors[i % length(colors) + 1]
    poly::Polygon = collect(cc.cliques[i])
    polys::Array{Polygon} = [poly]
    plot(polys; facecolor=color, alpha=0.3, edgecolor=color, points=true)
end

function MetricTools.plot(cc::CliqueComplex)
    PyPlot.clf()
    plot(cc.space)
    plot(Point[p for p in cc.points if !(p in cc.assigned)])
    
    for i in eachindex(cc.cliques)
        plot(cc, i)
    end
end

"""
Returns all unvisited points within radius
"""
inradius_unassigned(cc::CliqueComplex, point::Point; radius::Float64=cc.radius)::Array{Point} =
    [p for p in inradius(cc.knn, point, radius) if !assigned_nonvertex(cc, p)]
inradius_cert(cc::CliqueComplex, point::Point)::Array{Point} =
    inradius_unassigned(cc, point; radius=cert(cc.space, point))

"""
Gets all certified points
"""
function cert_points(cc::CliqueComplex, surface::Polygon, halfspace::Union{Nothing, HalfSpace})::Array{Point}
    certpoints::Set{Point} = intersect([Set{Point}(inradius_cert(cc, p))
        for p in surface]...)
    
    certpoints = Set{Point}([p for p in certpoints if !inhalfspace(p, halfspace)])
    println("        Adding points from certificate...")
    display(collect(certpoints))
    return collect(certpoints)
end

"""
Checks if a point can join a clique
"""
function connects_all(cc::CliqueComplex, clique::Clique, point::Point)::Bool
    for cpoint in clique
        if !localplanner(cc.space, cpoint, point) return false end
    end
    
    return true
end


"""
Using a queue of points, grow the clique outward from the seed surface
We know that all points on the surface can reach each other.
First add all points in the certificate
Then grow outwards
"""
function construct_clique!(cc::CliqueComplex, surface::Polygon, halfspace::Union{Nothing, HalfSpace}
        )::Tuple{Array{Polygon}, Array{HalfSpace}}
    println("    Constructing clique from surface $surface...")
    plot(surface; color="red", zorder=4)
    clique::Clique = Clique(surface)

    # add all certified points
    certified::Array{Point} = cert_points(cc, surface, halfspace)
    if !isempty(certified) push!(clique, certified...) end

    # grow the clique
    queue::Array{Point} = collect(clique)
    while !isempty(queue)
        point = pop!(queue)
        if !(point in surface) && assigned_nonvertex(cc, point) continue end
        
        println("   Checking neighbors for $point...")
        neighbors::Array{Point} = inradius_unassigned(cc, point)
        for neighbor in neighbors
            if neighbor in cc.assigned continue end
            if inhalfspace(neighbor, halfspace) continue end
            if connects_all(cc, clique, neighbor)
                println("        Adding neighbor $neighbor")
                push!(clique, neighbor)
                pushfirst!(queue, neighbor)
                push!(cc.assigned, neighbor)
            end
        end
    end

    # Remove redundant points and return surfaces of the cell
    surfaces::Array{Polygon} = []
    hss::Array{HalfSpace} = []
    
    # remove redundant points
    # if enough points to be redundant
    if !validpolyhedron(clique)
        
        println("INVALID CLIQUE! $(collect(clique))")
        return surfaces, hss
    end

    p::Polyhedron = topolyhedron(clique)
    removevredundancy!(p)
    clique = Clique(topointlist(p))

    println("Collecting surfaces...")
    println(hrep(p))
    
    for hs in halfspaces(hrep(p))
        hp = HyperPlane(hs.a, hs.β)
        println(hp)
        surface::Polygon = Point[p for p in clique if in(p, hp)]
        println("    Surface: $surface")
        push!(surfaces, surface)
        push!(hss, hs)
        push!(cc.vertices, surface...)
    end

    # add the clique
    push!(cc.cliques, clique)

    return surfaces, hss
end

"""
Using a queue of surfaces, construct cliques outward breadth first.
Points on surfaces will be contained in two cliques
"""
function construct!(cc::CliqueComplex)
    start::Point = first(cc.points)
    sfqueue::Array{Polygon} = [[start]]
    hsqueue::Array{Union{Nothing, HalfSpace}} = [nothing]
    visited::Set{Polygon} = Set{Polygon}()
    
    while !isempty(sfqueue)
        surface::Polygon = pop!(sfqueue)
        halfspace::Union{Nothing, HalfSpace} = pop!(hsqueue)
        if surface in visited continue end
        
        surfaces, hss = construct_clique!(cc, surface, halfspace)
        push!(visited, surface)
        
        if !isempty(surfaces)
            pushfirst!(sfqueue, surfaces...)
            pushfirst!(hsqueue, hss...)
        end
        
        plot(cc)
        readline()
    end
end

function triangulate!(cc::CliqueComplex)
    start::Polygon = nearest(cc.knn, first(cc.points), cc.space.dim)
    queue::Array{Polygon} = [start]
    println(queue)
    
    push!(cc.cliques, Set{Point}(start))
end

function test_cc()
    space::Space = PlanarSpace(; filename="data/input/spaces/planar/world1.json")
    cc::CliqueComplex = CliqueComplex(space)
    plot(cc)
end

test_cc()
