
function test()
    children = Array{Float64}(undef, 2^10)

    function divide_mod_helper(i)
        children[i] = i
    end

    # Threads.@threads for (i,c) in enumerate(collect(Base.Iterators.product(bd_split_array...)))

        Threads.@threads for i in 1: 2^10
        @inbounds divide_mod_helper(i)
    end
    return children
    
end

c = test()
println(c)