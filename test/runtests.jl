#=
 * File: all.jl
 * Project: planners
 * File Created: Tuesday, 13th August 2019 11:12:16 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 11th November 2019 11:27:04 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

#region imports
using Test
#endregion imports

@testset "Planners Tests" begin
    println("Testing MotionPlanning...")
    include("$(@__DIR__)/visibility.jl")
    include("$(@__DIR__)/prm.jl")
    include("$(@__DIR__)/fst.jl")
    include("$(@__DIR__)/approx.jl")
end
