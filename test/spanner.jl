using MotionPlanning.WSSpanner

function allClose(a::Real, b::Real, tol=0.00001)
	if abs(a - b) < tol
		return true;
	end
	return false;
end

function testWeightedSpanner(n = 30)
	# create 30 to 50 vertices
	# randomize edge weights
	# m::Int, n::Int, epsilon::Float64, wMin::Float64, wMax::Float64
	m = 3
	# n = 30
	g = SpannerGraph(m, n, 0.1, 1.0, 100.0)
	spanner = SpannerGraph(m, n, 0.1, 1.0, 100.0)
	# assignR(g)
	assignR(spanner)

	# now, add edges, with probability
	# range in 1 to 100

	count = 0
	# now add edges, filter
	for i = 1:g.size
		for j = i+1:g.size
			# try every set of edges
			pb = Random.rand(rng, 1)[1]
			# 0.5 chance of add an edge
			# if pb > 0.5
			# first, test complete graph
			if true
				count += 1
				weight = 100 * Random.rand(rng, 1)[1];
				if weight < 1
					weight = 1 + weight
				end
				SimpleWeightedGraphs.add_edge!(g.graph, i, j, weight)
				# # # edge from i to j
				if retainEdge(i, j, weight, spanner)
					# LightGraphs.add_edge!(g.graph, i, j)
					SimpleWeightedGraphs.add_edge!(spanner.graph, i, j, weight)
				end
			end
		end
	end
	# println("limit: ", spanner.limit)
	# println(g.graph)
	println(spanner.graph)
	# println(SimpleWeightedGraphs.ne(spanner.graph) / count)
	println(SimpleWeightedGraphs.ne(spanner.graph) / SimpleWeightedGraphs.ne(g.graph))
	# filter

	# u = Random.rand(rng, 1:n, 1)[1]
	# v = Random.rand(rng, 1:n, 1)[1]
	# while v == u
	# 	v = Random.rand(rng, 1:n, 1)[1]
	# end
	# println(u, ", ", v)
	# d1 = SimpleWeightedGraphs.dijkstra_shortest_paths(g.graph, u)
	# d2 = SimpleWeightedGraphs.dijkstra_shortest_paths(spanner.graph, u)

	# println("dist: ", d1.dists[v], ", after: ", d2.dists[v], ", ratio: ", d2.dists[v] / d1.dists[v])
	maxR = 1
	for u = 1:n
		d1 = SimpleWeightedGraphs.dijkstra_shortest_paths(g.graph, u)
		d2 = SimpleWeightedGraphs.dijkstra_shortest_paths(spanner.graph, u)
		for i = 1:length(d1.dists)
			if !allClose(d1.dists[i], d2.dists[i])
				# println("u: ", u, ", v: ", i)
				ratio = d2.dists[i] / d1.dists[i]
				if ratio > maxR
					maxR = ratio
				end
				# println("dist: ", d1.dists[i], ", after: ", d2.dists[i], ", ratio: ", ratio)
			end
		end
	end
	println("maxR: ", maxR)

	# if fixed number ok, then test adding vertex on the fly

	# if all good, commit, then merge

end

function testWeightedIncremental(n = 100)
	# create 30 to 50 vertices
	# randomize edge weights
	# m::Int, n::Int, epsilon::Float64, wMin::Float64, wMax::Float64
	m = 3
	# n = 30
	g = SpannerGraph(m, 0.1, 1.0, 100.0)
	spanner = SpannerGraph(m, 0.1, 1.0, 100.0)
	# assignR(g)
	# assignR(spanner)

	# make this geometric graph? 
	println("limit: ", spanner.limit, ", layers: ", spanner.layers)
	# if manually set the limit to be small? 
	# add vertices
	count = 0
	# set max to be n
	for i = 1:n
		# add vertex
		addVertex(g)
		addVertex(spanner)
		
	end

	for i = 1:n
		for j = i+1:n
			# if j == i
			# 	continue
			# end

			pb = Random.rand(rng, 1)[1]
			# if pb > 0.7
			if true
				count += 1
				weight = 100 * Random.rand(rng, 1)[1];
				if weight < 1
					weight = 1 + weight
				end

				SimpleWeightedGraphs.add_edge!(g.graph, i, j, weight)
				# if retainEdge(i, j, weight, spanner)
				# 	SimpleWeightedGraphs.add_edge!(spanner.graph, i, j, weight)
				# end
			end
		end
	end

	# try loop over edges randomly

	for e in SimpleWeightedGraphs.edges(g.graph)
		i = SimpleWeightedGraphs.src(e)
		j = SimpleWeightedGraphs.dst(e)
		weight = SimpleWeightedGraphs.weight(e)

		# if retainEdge(i, j, weight, spanner)
		# 	SimpleWeightedGraphs.add_edge!(spanner.graph, i, j, weight)
		# end
		if retainEdge(j, i, weight, spanner)
			SimpleWeightedGraphs.add_edge!(spanner.graph, j, i, weight)
		end

	end


	
	# println("limit: ", spanner.limit)
	# println(g.graph)
	println(spanner.graph)
	# println(SimpleWeightedGraphs.ne(spanner.graph) / count)
	println(SimpleWeightedGraphs.ne(spanner.graph) / SimpleWeightedGraphs.ne(g.graph))
	# filter

	# # u = Random.rand(rng, 1:n, 1)[1]
	# # v = Random.rand(rng, 1:n, 1)[1]
	# # while v == u
	# # 	v = Random.rand(rng, 1:n, 1)[1]
	# # end
	# # println(u, ", ", v)
	# # d1 = SimpleWeightedGraphs.dijkstra_shortest_paths(g.graph, u)
	# # d2 = SimpleWeightedGraphs.dijkstra_shortest_paths(spanner.graph, u)

	# # println("dist: ", d1.dists[v], ", after: ", d2.dists[v], ", ratio: ", d2.dists[v] / d1.dists[v])
	maxR = 1
	for u = 1:n
		d1 = SimpleWeightedGraphs.dijkstra_shortest_paths(g.graph, u)
		d2 = SimpleWeightedGraphs.dijkstra_shortest_paths(spanner.graph, u)
		for i = 1:length(d1.dists)
			if !allClose(d1.dists[i], d2.dists[i])
				# println("u: ", u, ", v: ", i)
				ratio = d2.dists[i] / d1.dists[i]
				if ratio > maxR
					maxR = ratio
				end
				# println("dist: ", d1.dists[i], ", after: ", d2.dists[i], ", ratio: ", ratio)
			end
		end
	end
	println("maxR: ", maxR)

	# # if fixed number ok, then test adding vertex on the fly

	# # if all good, commit, then merge
end