#=
 * File: RRTs.jl
 * Project: planners
 * File Created: Tuesday, 13th August 2019 11:12:15 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Wednesday, 22nd April 2020 3:21:37 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module RRTs

#region imports
import PyCall, PyPlot
import LightGraphs
using ProgressMeter

using MetricTools, MetricTools.MetricGraphs
using MetricSpaces, MetricSpaces.Samplers
using ..MotionPlanning
#endregion imports

export RRTstar, plan

"""
RRT*, but 
"""
mutable struct RRTstar <: Planner
    space::Space
    start::Point
    sampler::Function
    radius::Float64
    bias::Float64
    step::Float64
    parents::Dict{Point, Point}
    costs::Dict{Point, Float64}
    graph::MetricGraph
    
    function RRTstar(space::Space, start::Point; sampler=uniform_random::Function, radius=0.2::Float64, bias::Float64=0.2, step::Float64=0.05)
        println("Constructing RRT*...")
        rrt::RRTstar = new(space, start, sampler, radius, bias, step,
            Dict{Point, Point}(),
            Dict{Point, Float64}(),
            MetricGraph(space.dim;
                metric=dmetric(space),
                knntype=KNNs.BallTreeKNN))

        return rrt
    end
end

"""
Returns the best nearest neighbor in the tree
"""
function steer(rrt::RRTstar, target::Point)::Tuple{Point, Point}
    nearest::Point = first(knn(rrt.graph, target, 1))
    diff::Point = target - nearest
    length::Float64 = metric(rrt.space, diff, Float64[0 for _ in target]) 
    new::Point = nearest + diff ./ length .* rrt.step
    return new, nearest
end

"""
Returns the path of a query to the RRT
"""
function MotionPlanning.plan(rrt::RRTstar, start::Point, goal::Point;
        n::Integer=100)::Array{Point}

    add!(rrt.graph, start)
    rrt.costs[start] = 0
    nnbuild!(rrt.graph)
    
    @showprogress for i in 1:n
        target::Point = (rand() > rrt.bias) ? first(rrt.sampler(rrt.space, 1)) : goal
        new, nearest = steer(rrt::RRTstar, target)
        
        if !localplanner(rrt.space, new, nearest) continue end
        
        add!(rrt.graph, new)
        nnbuild!(rrt.graph)
        
        # Instead of automatically connecting to nearest, check all nearby connectable neighbors
        minpoint::Point = nearest
        mincost::Float64 = rrt.costs[nearest] + rrt.step
        neighbors::Array{Point} = 
            [n for n in inradius(rrt.graph, new, rrt.radius)
            if localplanner(rrt.space, new, n) && n != new]
        
        for n in neighbors
            # If going through n point is faster than going through nearest
            ncost::Float64 = rrt.costs[n] + metric(rrt.space, new, n)
            if ncost < mincost
                mincost = ncost
                minpoint = n
            end
        end

        add!(rrt.graph, minpoint, new)
        rrt.parents[new] = minpoint
        rrt.costs[new] = mincost
        
        # Rewire the tree
        for n in neighbors            
            ncost::Float64 = rrt.costs[new] + metric(rrt.space, new, n)
            if ncost < rrt.costs[n]
                # Go through new instead of the unoptimal parent
                remove!(rrt.graph, rrt.parents[n], n)
                add!(rrt.graph, new, n)
                rrt.parents[n] = new
                rrt.costs[n] = ncost
            end
        end
    end
    
    # Backtrace for path
    path::Array{Point} = []
    ngoal::Point = first(knn(rrt.graph, goal, 1))
    point::Point = ngoal
    while point in keys(rrt.parents)
        println(point)
        push!(path, point)
        point = rrt.parents[point]
    end

    push!(path, rrt.start)
    return path
end

"""
Plots a fst
"""
function MetricTools.plot(rrt::RRTstar;
        ax=PyCall.PyNULL()::PyCall.PyObject,
        cbar=true::Bool)::PyCall.PyObject
    
    plot(rrt.graph; ax=ax, progress=false)
    plot(rrt.space; ax=ax)
    
    return ax
end
end
