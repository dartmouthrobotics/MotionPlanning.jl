module BaseCells
import JSON2
import PyCall, PyPlot
import LightGraphs

using MetricTools, MetricTools.MetricGraphs

using
    MetricSpaces,
    MetricSpaces.Planar,
    MetricSpaces.Samplers

export BaseCell, get_boundary_points, grid_all_points_with_bounds

"""Use basecell to determine whether inside_is_free firstly
"""


"""
Generates a grid of samples over the space, return points in free and in obstacle
Difference: with bounds!!!
"""
function grid_all_points_with_bounds(space::Space, n::Integer, cell_bounds)
    points::Array{Point} = []
    ob_points::Array{Point} = []
    total_points::Array{Point} = []
    m::Integer = floor(n ^ (1 / space.dim))
    for tup in Iterators.product(
            [range(b.lo; stop=b.hi, length=m)
            for b in cell_bounds]...)
        point::Point = [v for v in tup]
        if isfree(space, point) push!(points, point)
        else push!(ob_points, point) end
        push!(total_points, point)
    end
    return points, ob_points, total_points
end



mutable struct BaseCell
    space::Space
    bounds::Array{Bound}
    all_free_points::Array
    bd_points::Array
    bd_points_index::Array
    total_bd_points::Array
    total_bd_points_index::Array
    inside_is_free::Bool

    function BaseCell(
        space::Space,
        n::Integer, 
        bounds::Array{Bound},
        use_2D_rplr)
        all_free_points, ob_points, total_points = grid_all_points_with_bounds(space, n, bounds) #grid_all_points_with_bounds
        bd_points, bd_points_index = get_boundary_points(all_free_points, bounds; dim=space.dim, use_2D_rplr=use_2D_rplr)
        total_bd_points, total_bd_points_index = get_boundary_points(total_points, bounds; dim=space.dim, use_2D_rplr=use_2D_rplr)
        """The length of the space should be length(total_bd_points)+1"""


        inside_is_free = whether_inside_is_free(space, bd_points,bd_points_index) 

        basecell::BaseCell = new(
            space,
            bounds,
            all_free_points,
            bd_points,
            bd_points_index,
            total_bd_points,
            total_bd_points_index,
            inside_is_free
            )
        return basecell
    end
end



"""
Calculates the distances for all pairs for training the regression model
"""
function whether_inside_is_free(space, bd_points, bd_points_index)
    for i in 1:length(bd_points)
        i_point = bd_points[i]
        i_index = bd_points_index[i]
        for j in i+1:length(bd_points) # i or i+1 is a question
            """Whether put points_to_points(self to LA)"""
            j_point = bd_points[j]
            j_index = bd_points_index[j]
            # push!(pair_points, x1_y1_x2_y2)  # only push points when not using localplanner
            if !localplanner(space, i_point, j_point)
                return false
            end
        end
    end
    return true
end



"""
Helper function: get the point of the boundary
   *** Notice: duplicated point is includeded for nd_rplr and xgboost
   *** Notice: remove duplicate point for 2d plr
"""
##TODO: should add start point twice??.TODO: delete duplicated points already??
function get_boundary_points(all_free_points, bounds; dim::Integer=2,  use_2D_rplr=false)

    bd_points = []
    bd_points_index = []
    
    if !use_2D_rplr
        num_edges_per_cell = dim * 2^(dim -1)

        temp_array::Array{Array{Point}} = [[] for i in 1:num_edges_per_cell]
        temp_array_index::Array{Array{Integer}} = [[] for i in 1:num_edges_per_cell]

        bounds_array = [[b.lo,b.hi] for b in bounds]
        edge_array = [] 

        for i in 1: dim
            t_array = deepcopy(bounds_array)
            deleteat!(t_array, i)
            part_edges = [[comb...] for comb in collect(Base.Iterators.product(t_array...))]
            append!(edge_array,part_edges)
        end

        for p_i in 1:length(all_free_points)
            p = all_free_points[p_i]
            time_for_break = false
            for d in 1:dim  #e.g. 1:3 for 3d
                t_p = deepcopy(p)
                deleteat!(t_p, d)
                for j in 1:2^(dim-1) #e.g. 1:4 for 3d
                    temp_index = (d-1)*2^(dim-1)+j
                    if t_p == edge_array[temp_index]
                        push!(temp_array[temp_index], p)
                        push!(temp_array_index[temp_index], p_i)
                        time_for_break = true
                    end
                end
                if time_for_break
                    break
                end
            end
        end
        
        for i in 1: num_edges_per_cell
            append!(bd_points, temp_array[i])
            append!(bd_points_index, temp_array_index[i])
        end
    else

        left_edge = []
        right_edge = []
        bottom_edge = []
        top_edge = []
        
        left_edge_index = []
        right_edge_index = []
        bottom_edge_index = []
        top_edge_index = []
        for p_i in 1:length(all_free_points)
            # the order should be bottome -> right -> top -> left
            p = all_free_points[p_i]
            if p[2] == bounds[2].lo
                push!(bottom_edge, p)
                push!(bottom_edge_index, p_i)
            elseif p[1] == bounds[1].hi
                push!(right_edge, p)
                push!(right_edge_index, p_i)
            elseif p[2] == bounds[2].hi
                push!(top_edge, p)
                push!(top_edge_index, p_i)
            elseif p[1] == bounds[1].lo
                push!(left_edge, p)
                push!(left_edge_index, p_i)
            end
        end
        append!(bd_points,bottom_edge)
        append!(bd_points,right_edge)
        append!(bd_points,reverse(top_edge))
        append!(bd_points,reverse(left_edge))
        append!(bd_points_index,bottom_edge_index)
        append!(bd_points_index,right_edge_index)
        append!(bd_points_index,reverse(top_edge_index))
        append!(bd_points_index,reverse(left_edge_index))
    end

    return bd_points, bd_points_index
end

end
