module ShadowCells

import JSON2
import PyCall, PyPlot
import LightGraphs

using MetricTools, MetricTools.MetricGraphs, MetricRegressors

using
    MetricSpaces,
    MetricSpaces.Planar,
    MetricSpaces.Samplers,
    MetricSpaces.BinaryPartitions,
    MetricSpaces.PLRs

using ..PRMs, ..VisibilityGraphs, ..BaseCells

export ShadowCell

# const ensemble = PyCall.PyNULL()
# const np = PyCall.PyNULL()
# const joblib= PyCall.PyNULL()
# const lgb = PyCall.PyNULL()
# const pandas = PyCall.PyNULL()
# const pickle = PyCall.PyNULL()
# const svm = PyCall.PyNULL()
# const xgboost = PyCall.PyNULL()
# const gp = PyCall.PyNULL()


"""
Called when the module is loaded, makes sure all directories are created，allows PyPlot imports to be precompiled correctly
"""
function __init__()
    # copy!(ensemble, PyCall.pyimport("sklearn.ensemble"))
    # copy!(np, PyCall.pyimport("numpy"))
    # copy!(joblib, PyCall.pyimport("sklearn.externals.joblib"))
    # copy!(xgboost, PyCall.pyimport("xgboost"))
    # copy!(gp, PyCall.pyimport("sklearn.gaussian_process"))
end

mutable struct ShadowCell
    space::Space
    prm::Union{PRM, Nothing}
    bounds::Array{Bound}
    clf::Union{Regressor, PyCall.PyObject}
    reg::Union{Regressor, PyCall.PyObject}
    total_bd_points::Array
    all_free_points::Array
    predict::Function
    inside_is_free::Bool
    vg_array::Array
    use_2D_rplr::Bool
    use_localplanner::Bool #For ml model only for now
    use_real_value::Bool
    use_clf::Bool

    function ShadowCell(
        base_cell::Union{Nothing, BaseCells.BaseCell},
        space::Space,
        n::Integer, 
        bounds::Array{Bound};
        inside_is_free::Bool=false,
        clf::Union{Regressor, PyCall.PyObject}=XGBoostRegressor(; epochs=200, maxdepth=3),
        reg::Union{Regressor, PyCall.PyObject}=XGBoostRegressor(; epochs=200, maxdepth=3),
        use_2D_rplr::Bool=false, 
        use_localplanner::Bool=true,
        vg_array::Array=[],
        use_real_value::Bool=false,
        plot_rplr::Bool=false,
        pre_Xc=[],
        pre_Yc=[],
        pre_X=[],
        pre_Y=[],
        pre_use_clf::Bool=false
        )

        k::Integer = round(Int, log(2, n))
        #TODO: use the code from basecell to construct
        
        prm::Union{PRM, Nothing} = (isempty(vg_array) && isempty(pre_X)) ?
                PRM(space; sampler=grid_all_points_with_bounds, k=k) :
                nothing
        
        all_free_points = base_cell.all_free_points
        if !isnothing(prm)
            MetricGraphs.add!(prm.graph, all_free_points)
            MetricGraphs.nnbuild!(prm.graph)
            for point in all_free_points
                npoints = MetricGraphs.knn(prm.graph, point, k)
                for n_p in npoints
                    if localplanner(space, point, n_p) # Can only call localplanner once
                        MetricGraphs.add!(prm.graph, point, n_p)
                    end
                end
            end
            # fig, ax = PyPlot.subplots()
            # plot(prm.graph)
        end
        # Get bondary points
        bd_points, bd_points_index = base_cell.bd_points, base_cell.bd_points_index
        total_bd_points, total_bd_points_index = base_cell.total_bd_points, base_cell.total_bd_points_index
        """The length of the space should be length(total_bd_points)+1"""
        println("total bd points: ", length(total_bd_points))
        println("TIME TO SEE WHETHER IT USED NEW METHOD")
        # println("pre_x is: ", pre_X)
        function temp_helper_func()
            if inside_is_free || !isempty(pre_X)  
                println("USED NEW METHOD!!!!!!!!!!!!!######")
                return pre_Xc, pre_Yc, pre_X, pre_Y, pre_use_clf end
            if use_2D_rplr
                return get_training_data(total_bd_points, all_free_points, space, prm, vg_array; use_real_value=use_real_value) 
            else
                return calcdistances(prm, bd_points,bd_points_index, use_localplanner, vg_array,space) 
            end
        end  

        Xc::Array{Point}, Yc::Array{Int64}, X::Array{Point}, Y::Array{Float64}, use_clf::Bool= temp_helper_func()
        ###### If the length of X equal to Xc, it means nothing need to classify, it means only 1 class
        """
        if no ob pts on edge, we can still use localplanner to test later in predict
            i.e. length(all_free_points) == length(total_points) doesn't affect
        if no ob inside, we then cannot retest
            i.e. isempty(non_zero_pairs) would make dis of free points all be 0.0
        """
        if inside_is_free
            #todo: does it really mean this cell is free? just free for current samples
            #todo: given this, clf cannot train, what should we do? just use metric distance?
            nothing
        else
            if use_2D_rplr || use_localplanner
                if use_clf
                    train!(clf, Xc, Yc)
                end
            else # Not use localplanner
                # clf.fit(Xc_for_fit, Yc) #3 kinds of label: 0, 1, -2
                train!(clf, Xc, Yc) 
            end
            if !isempty(X) #non_zero_pairs
                if use_2D_rplr
                    train!(reg, X, Y)
                    if plot_rplr  
                        fig, ax = PyPlot.subplots()
                        plot(reg) 
                    end
                    # plot(X; ax=ax) #plot samples of plr
                else
                    train!(reg, X, Y)  ###Modified here for 4d plr
                end
            end
        end
        shadowcell::ShadowCell = new(
            space,
            prm,
            bounds,
            clf, ## Must have
            reg,
            total_bd_points,
            all_free_points,
            () -> nothing,
            inside_is_free,
            vg_array,
            use_2D_rplr,
            use_localplanner,
            use_real_value,
            use_clf
            )
        shadowcell.predict = predictfunc(shadowcell)
        println("finish build shadowcell")
        return shadowcell
    end
end

"""
Fuction for plotting the ground_truth
"""

function ground_truth(shadowcell::ShadowCell)::Function
    prm = shadowcell.prm
    vg_array = shadowcell.vg_array
    all_free_points = shadowcell.all_free_points
    total_bd_points = shadowcell.total_bd_points
    space = shadowcell.space
    temp_graph::MetricGraph = MetricGraph(space.dim)
    function gt_helper(x_i,y_i)
        i_point = total_bd_points[x_i]
        j_point = total_bd_points[y_i]
        if (!isfree(space, i_point)) || (!isfree(space, j_point))
            return -1.0
        else
            if localplanner(space, i_point, j_point)
                return 0.0
            else
                i_index = findall(x->x==i_point, all_free_points)[1]
                j_index = findall(x->x==j_point, all_free_points)[1]
                dis = 0.0
                if isempty(vg_array)
                    ds = LightGraphs.dijkstra_shortest_paths(prm.graph.graph, i_index) #only need to run one time for each i
                    dis = ds.dists[j_index]
                else
                    dis = VisibilityGraphs.metric(vg_array[1], i_point, j_point)
                end
                if dis == Inf
                    return -2.0
                else
                    metric_dis = MetricGraphs.metric(temp_graph, i_point, j_point)
                    return dis-metric_dis
                end
            end
        end
    end
    return (x, y) -> begin
        return gt_helper(x,y)
    end
end



function predict_for_plot(shadowcell::ShadowCell)::Function
    all_free_points = shadowcell.all_free_points
    prm = shadowcell.prm
    clf = shadowcell.clf
    reg = shadowcell.reg
    total_bd_points = shadowcell.total_bd_points
    function predict_for_plot_helper(x_i,y_i)
        i_point = total_bd_points[x_i]
        j_point = total_bd_points[y_i]
        return shadowcell.predict(i_point, j_point)
    end
    return (x, y) -> begin
        return predict_for_plot_helper(x,y)
    end
end


"""
Predicts the distance between two points using the trained models
return:
        -1  : either p1 or p2 is not free
        -2  : there is no path between p1 and p2
        0.0 : p1 and p2 can be connected directly
        normal value : p1 and p2 can be connect by some path
Thus, each time use predict, test the return value, i.e.

    temp =  shadow_cell.predict(pt1,pt2)  
    # temp = -1 or -2, do nothing
    if temp == -1 || temp == -2
        return temp
    else
        metric_dis = MetricGraphs.metric(shadow_cell.prm.graph, pt1, pt2)
        return temp + metric_dis
    end
"""
function convert_point_to_index(shadowcell::ShadowCell, p)
    bounds = shadowcell.bounds
    total_bd_points = shadowcell.total_bd_points
    n_bd_pts_per_edge = length(total_bd_points)/4  #TODO: for higher dimension, it should not be 4
    (x_min, x_max, y_min, y_max) = (bounds[1].lo, bounds[1].hi, bounds[2].lo, bounds[2].hi)
    interval = (x_max - x_min)/n_bd_pts_per_edge  #TODO: assume square cell now
    if p[2] == y_min
        temp_int = div(p[1] - x_min, interval)
        temp_float = (p[1] - x_min)/interval - temp_int
        return temp_int+1+temp_float
    elseif p[1] == x_min
        temp_int = div(p[2] - y_min,interval)
        temp_float = (p[2] - y_min)/interval - temp_int
        return n_bd_pts_per_edge*4+1-(temp_int+temp_float)
    elseif p[1] == x_max
        temp_int = div(p[2] - y_min,interval)
        temp_float = (p[2] - y_min)/interval - temp_int
        return n_bd_pts_per_edge + temp_int+1+temp_float
    else # inverse way
        temp_int = div(p[1] - x_min, interval)
        temp_float = (p[1] - x_min)/interval - temp_int
        return n_bd_pts_per_edge*3+1-(temp_int+temp_float)
    end
end

function predictfunc(shadowcell::ShadowCell)::Function
    all_free_points = shadowcell.all_free_points
    prm = shadowcell.prm
    clf = shadowcell.clf
    reg = shadowcell.reg
    inside_is_free = shadowcell.inside_is_free
    use_2D_rplr = shadowcell.use_2D_rplr
    vg_array = shadowcell.vg_array ######### added for test
    use_localplanner = true  ### Made change here
    # use_localplanner = shadowcell.use_localplanner
    use_real_value=shadowcell.use_real_value
    use_clf = shadowcell.use_clf
    space = shadowcell.space
    function predict_helper(i_point::Point, j_point::Point)
        #### OPEN THESE LINES FOR TEST - Ground Truth
        # if (!isfree(space, i_point))||(!isfree(space, j_point))
        #     return -1.0
        # else
        #     i_index = findall(x->x==i_point, all_free_points)[1]
        #     j_index = findall(x->x==j_point, all_free_points)[1]
        #     if localplanner(space, i_point,j_point)
        #         return 0.0
        #     else
        #         dis = 0.0
        #         if isempty(vg_array)
        #             ds = LightGraphs.dijkstra_shortest_paths(prm.graph.graph, i_index) #only need to run one time for each i
        #             dis = ds.dists[j_index]
        #         else
        #             dis = VisibilityGraphs.metric(vg_array[1], i_point, j_point)
        #         end
        #         if dis == Inf
        #             return -2.0
        #         else
        #             metric_dis = MetricGraphs.metric(prm.graph, i_point, j_point)
        #             return dis-metric_dis
        #         end
        #     end
        # end
        if (!isfree(space, i_point))||(!isfree(space, j_point)) #detect pts on bd is free or not
            return -1.0
        else
            if inside_is_free
                return 0.0
            else
                if use_2D_rplr 
                    if localplanner(space, i_point, j_point)
                        return 0.0
                    else
                        ###Classifier used 2d version now
                        i1 = convert_point_to_index(shadowcell::ShadowCell, i_point)
                        i2 = convert_point_to_index(shadowcell::ShadowCell, j_point)
                        if i1 == i2
                            return 0.0
                        else
                            temp_p::Point = i1 > i2 ? [i1,i2] : [i2,i1]
                            if use_clf
                                result = first(predict(clf, temp_p))  # result would only be -2 or 1
                                if result <= -0.5
                                    return -2.0
                                else
                                    if use_real_value
                                        return predict(reg, temp_p) - MetricGraphs.metric(prm.graph, i_point, j_point) ###Should comment this line if not testing plr using real value
                                    else
                                        return predict(reg, temp_p)
                                    end
                                end
                            else
                                if use_real_value
                                    return predict(reg, temp_p) - MetricGraphs.metric(prm.graph, i_point, j_point) ###Should comment this line if not testing plr using real value
                                else
                                    return predict(reg, temp_p)
                                end
                            end
                            #### just for test only --> test gt points using the nearest neighbor
                            # min_value = 100
                            # nnp::Point = [0.0, 0.0]
                            # nnpdis = 0
                            # for ii in 1:length(X)
                            #     tempp = X[ii]
                            #     if (tempp[1] - temp_p[1])^2 + (tempp[2] - temp_p[2])^2 < min_value
                            #         nnp = tempp
                            #         min_value = (tempp[1] - temp_p[1])^2 + (tempp[2] - temp_p[2])^2
                            #         nnpdis = Y[ii]
                            #     end
                            # end
                            # return nnpdis

                        end
                    end   
                else
                    new_combined_points::Point = []
                    append!(new_combined_points, i_point)
                    append!(new_combined_points, j_point)
                    temp::Array{Point} = [new_combined_points]
                    ## Got the avg?
                    new_combined_points2::Point = []
                    append!(new_combined_points2, j_point)
                    append!(new_combined_points2, i_point)
                    if use_localplanner
                        if localplanner(space, i_point, j_point)
                            return 0.0
                        else
                            if use_clf
                                result = first(predict(clf, temp))  # result would only be -2 or 1
                                if result <= -0.5
                                    return -2.0
                                else
                                    return (predict(reg, new_combined_points)+predict(reg, new_combined_points2))/2.0## Changed here for test 4d plr
                                    # return (first(predict(reg, new_combined_points))+ first(predict(reg, new_combined_points2)))/2.0
                                    # return first(predict(reg, new_combined_points))
                                end
                            else
                                return (predict(reg, new_combined_points)+predict(reg, new_combined_points2))/2.0## Changed here for test 4d plr
                            end
                        end
                    else
                        # Notice: if not use localplanner, then clf must be used
                        result = first(predict(clf, temp))
                        if result > 0.5
                            return (first(predict(reg, new_combined_points))+ first(predict(reg, new_combined_points2)))/2.0
                            # return first(predict(reg, new_combined_points))
                        elseif result <= 0.5 && result >= -1
                            return 0.0
                        else
                            return -2.0
                        end
                    end
                end
            end
        end
    end
    return (p1::Point, p2::Point) -> predict_helper(p1, p2)
end


"""
Should not (train the pts with dis = 0) or (train the pts without dis = 0)
should (train the pts that does not be recognized by localplanner)
    Calculates the distances for all pairs for training the regression model
"""
# function calcdistances(prm::PRM, bd_points, bd_points_index, use_localplanner, vg_array)
#     println("Calculating distances...")
#     use_clf = false
#     pair_points::Array{Point}= []
#     diff_distances::Array{Float64}= []  #Important: must be defined specifically here
#     # use_localplanner = false ######Made change here
#     for i in 1:length(bd_points)
#         i_point = bd_points[i]
#         i_index = bd_points_index[i]
#         ds = LightGraphs.dijkstra_shortest_paths(prm.graph.graph, i_index)
#         for j in 1:length(bd_points) # i or i+1 is a question
#             """Whether put points_to_points(self to LA)"""
#             j_point = bd_points[j]
#             j_index = bd_points_index[j]
#             x1_y1_x2_y2 = []
#             append!(x1_y1_x2_y2, i_point)
#             append!(x1_y1_x2_y2, j_point)
#             # push!(pair_points, x1_y1_x2_y2)  # only push points when not using localplanner
#             if localplanner(space, i_point, j_point)
#                 if !use_localplanner
#                     push!(pair_points, x1_y1_x2_y2)
#                     push!(diff_distances,0.0)
#                 end
#             else
#                 dis = 0.0
#                 if isempty(vg_array)
#                     dis = ds.dists[j_index]
#                 else
#                     dis = VisibilityGraphs.metric(vg_array[1], i_point, j_point)
#                 end
#                 if dis == Inf
#                     push!(pair_points, x1_y1_x2_y2)
#                     push!(diff_distances, -2.0)
#                 else
#                     metric_dis = MetricGraphs.metric(prm.graph, i_point, j_point)
#                     push!(pair_points, x1_y1_x2_y2)
#                     push!(diff_distances, dis-metric_dis)

#                     ## Test that some pair of points' dis is 0, but localplanner=false
#                     # if dis-metric_dis == 0
#                     #     println("Wrong!!!")
#                     #     println("The distance between these 2 pts are 0: ", i_point, " and ", j_point)
#                     #     VisibilityGraphs.metric(vg_array[1], i_point, j_point; verbose=true)
#                     #     println("Let's see the local planner: ", localplanner(space, i_point, j_point))
#                     # end
#                 end
#             end
#         end
#     end
#     non_zero_pairs::Array{Array{Float64}} =[]
#     non_zero_pair_distances::Array{Float64} = []
#     Overall_label::Array{Int8} = []
#     for i in 1:length(pair_points)
#         if diff_distances[i] == 0.0
#             push!(Overall_label,0)
#         elseif diff_distances[i] == -2.0
#             use_clf = true
#             push!(Overall_label,-2)
#         else
#             push!(non_zero_pairs, pair_points[i])
#             push!(non_zero_pair_distances, diff_distances[i])
#             push!(Overall_label,1)
#         end
#         # if diff_distances[i] == -2.0
#         #     use_clf = true
#         #     push!(Overall_label, -2)
#         # else
#         #     push!(Overall_label, 1) 
#         #     push!(non_zero_pairs, pair_points[i])
#         #     push!(non_zero_pair_distances, diff_distances[i])
#         #     #Notice! ## this point can have distances 0 or 1, not just 1
#         # end
#     end
#     return pair_points, Overall_label, non_zero_pairs, non_zero_pair_distances, use_clf
# end

function calcdistances(prm::Union{PRM, Nothing} , bd_points, bd_points_index, use_localplanner, vg_array, space)
    println("Calculating distances...")
    use_clf = false
    pair_points::Array{Point}= []
    non_zero_pairs::Array{Array{Float64}} =[]
    non_zero_pair_distances::Array{Float64} = []
    Overall_label::Array{Int8} = []
    temp_graph::MetricGraph = MetricGraph(space.dim)
    # vg_test = VisibilityGraphs.VisibilityGraph(space)
    for i in 1:length(bd_points)
        i_point = bd_points[i]
        i_index = bd_points_index[i]
        ds = nothing
        if isempty(vg_array)
            ds = LightGraphs.dijkstra_shortest_paths(prm.graph.graph, i_index)
        end
        for j in 1:length(bd_points) # i or i+1 is a question
            """Whether put points_to_points(self to LA)"""
            j_point = bd_points[j]
            j_index = bd_points_index[j]
            x1_y1_x2_y2 = []
            append!(x1_y1_x2_y2, i_point)
            append!(x1_y1_x2_y2, j_point)
            # push!(pair_points, x1_y1_x2_y2)  # only push points when not using localplanner
            if localplanner(space, i_point, j_point)
                if !use_localplanner
                    push!(pair_points, x1_y1_x2_y2)
                    push!(Overall_label,0)
                end
            else
                dis = 0.0
                if isempty(vg_array)
                    dis = ds.dists[j_index]
                else
                    dis = VisibilityGraphs.metric(vg_array[1], i_point, j_point)
                end
                if dis == Inf
                    push!(pair_points, x1_y1_x2_y2)
                    push!(Overall_label,-2)
                    use_clf = true
                else
                    metric_dis = MetricGraphs.metric(temp_graph, i_point, j_point)
                    push!(pair_points, x1_y1_x2_y2)
                    push!(non_zero_pairs, x1_y1_x2_y2)
                    push!(non_zero_pair_distances, dis-metric_dis)
                    if dis-metric_dis == 0
                        if use_localplanner
                            push!(Overall_label, 1) #Made change
                        else
                            push!(Overall_label, 0) #Made change
                        end
                    else
                        push!(Overall_label,1)
                    end
                    ## Test that some pair of points' dis is 0, but localplanner=false
                    # if dis-metric_dis == 0
                    #     println("Wrong!!!")
                    #     println("The distance between these 2 pts are 0: ", i_point, " and ", j_point)
                    #     VisibilityGraphs.metric(vg_array[1], i_point, j_point; verbose=true)
                    #     println("Let's see the local planner: ", localplanner(space, i_point, j_point))
                    # end
                end
            end
        end
    end
    return pair_points, Overall_label, non_zero_pairs, non_zero_pair_distances, use_clf
end

"""
Get bot the traing data for classifier and regressor
    Label = 1 means pt_to_pt is non-reachable
    Label = 0 means pt_to_pt is reachable 
"""
function get_training_data(total_bd_points, all_free_points, space, prm::Union{PRM, Nothing} , vg_array; use_real_value=false)
    X, Y = [],[]
    Xc, Yc::Array{Int64} = [],[]
    use_clf = false
    temp_graph::MetricGraph = MetricGraph(space.dim)
    for ((i1,p1),(i2,p2)) in Iterators.product(enumerate(total_bd_points),enumerate(total_bd_points))
        if i1 <= i2  continue end # Only put half triangle's point in TODO: do we need to put equal
        if !isfree(space, p1) || !isfree(space, p2) continue end
        if localplanner(space, p1, p2) continue end
        dis = 0.0
        if isempty(vg_array)
             # dis = ds.dists[total_bd_points_index[i2]]
            i_index = findall(x->x==p1, all_free_points)[1]
            j_index = findall(x->x==p2, all_free_points)[1]
            ds = LightGraphs.dijkstra_shortest_paths(prm.graph.graph, i_index) #only need to run one time for each i
            dis = ds.dists[j_index]
        else
            dis = VisibilityGraphs.metric(vg_array[1], p1, p2)
        end
        if dis != Inf  
            push!(X, [i1,i2])
            if use_real_value
                push!(Y, dis)
            else
                metric_dis = MetricGraphs.metric(temp_graph, p1, p2)  
                push!(Y, dis - metric_dis) 
            end
            push!(Yc, 1) #distance can be 0 or 1
        else
            use_clf = true
            push!(Yc, -2)
        end
        push!(Xc, [i1,i2])
        # push!(Yc, dis == Inf)
    end
    return Xc, Yc, X, Y, use_clf
end


#region functions


end
