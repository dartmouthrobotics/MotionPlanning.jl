#=
 * File: PRMs.jl
 * Project: planners
 * File Created: Tuesday, 13th August 2019 11:12:15 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Friday, 17th April 2020 1:17:27 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module PRMs

#region imports
import PyCall, PyPlot
import LightGraphs

using MetricTools, MetricTools.MetricGraphs
using MetricSpaces, MetricSpaces.Samplers
using ..MotionPlanning
#endregion imports
# import Distances
export PRM, nn, grow!, get_points

"""
PRM (Probabilistic Roadmap)
"""
mutable struct PRM <: Planner
    space::Space
    sampler::Function
    k::Integer
    graph::MetricGraph
    
    function PRM(space::Space; sampler=uniform_random::Function, k=5::Integer)
        println("Constructing PRM...")
        return new(space, sampler, k, MetricGraph(space.dim;
                metric=dmetric(space),
                knntype=KNNs.BallTreeKNN))
    end
end

"""
Gets the nearest neighbors from the roadmap (using pre-set k)
"""
function nn(prm::PRM, point::Point)::Array{Point}
    nnbuild!(prm.graph)     # rebuild knn if it is out of date
    return knn(prm.graph, point, min(prm.k + 1, nv(prm.graph)))
end

"""
Gets the nearest neighbor index from the roadmap (using pre-set k)
"""
function nnids(prm::PRM, point::Point)::Array{Integer}
    return knnids(prm.graph, point, prm.k + 1)
end

"""
Adds a single point to the PRM
"""
function MetricTools.add!(prm::PRM, point::Point)
    add!(prm.graph, point)
    connect_point!(prm, point)
end

"""
Adds a point to the PRM
"""
function connect_point!(prm::PRM, point::Point)
    nneighbors::Array{Point} = nn(prm, point)
    popfirst!(nneighbors)   # don't connect this point to itself
    # filter out all invalid neighbors
    nneighbors = [n for n in nneighbors if localplanner( prm.space, point, n)]
    edges::Array{Line} = [(point, neighbor) for neighbor in nneighbors]
    add!(prm.graph, edges)
end

"""
Grows the roadmap, adding n samples
"""
function grow!(prm::PRM, n::Integer)
    points::Array{Point} = prm.sampler(prm.space, n)
    @time add!(prm.graph, points)
    @time nnbuild!(prm.graph)
    println("    PRM built with $(length(points)) points.")
    @time for point in points
        connect_point!(prm, point)
    end
end

"""
Grows the roadmap for t seconds
"""
function grow!(prm::PRM, t::AbstractFloat)
    # sample the space randomly
    # points::Array{Point} = prm.sampler(prm.space, 2)
end

"""
Returns the path of a query to the prm
Start and goal are temporarily added
"""
function MotionPlanning.plan(prm::PRM, start::Point, goal::Point)::Array{Point}
    startn = first(nn(prm, start))
    goaln = first(nn(prm, goal))

    path::Array{Point} = astar(prm.graph, startn, goaln)
    if isempty(path) return path end

    pushfirst!(path, start)
    push!(path, goal)
    return path
end

"""
Returns the distance of a path
"""
function MetricTools.distance(prm::PRM, path::Array{Line})::Float64
    return distance(prm.graph, path)
end

"""
Returns the distance of a query to the PRM
"""
function MetricTools.distance(prm::PRM, start::Point, goal::Point)::Union{Float64, Nothing}
    path::Array{Point} = plan(prm, start, goal)
    return distance(prm, path)
end

"""
Returns the distance function to a specified goal
"""
function MetricTools.distfunc(prm::PRM, goal::Point)::Function
    # precompute all distances to goal with djikstras
    goalnv::Integer = first(knnids(prm.graph, goal, 1))
    ds::LightGraphs.DijkstraState = dijkstras(prm.graph, goalnv)
    return (p::Point) -> ds.dists[first(nnids(prm, p))]
end

"""
Plots the PRM
"""
function MetricTools.plot(prm::PRM;
        linecolor="grey"::String,
        ax=PyCall.PyNULL()::PyCall.PyObject)
    plot(prm.graph; linecolor="black", ax=ax)
end
end
