#=
 * File: VisibilityGraphs.jl
 * Project: planners
 * File Created: Tuesday, 13th August 2019 11:12:16 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 13th April 2020 10:25:35 am
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module VisibilityGraphs

#region imports
import PyCall, PyPlot, LightGraphs
using ProgressMeter

using
    MetricTools,
    MetricTools.MetricGraphs,
    MetricTools.Geometry,
    MetricTools.Visualization

using MetricSpaces
using ..MotionPlanning
#endregion imports

export VisibilityGraph

struct VisibilityGraph <: Planner
    graph::MetricGraph
    space::Space
    waypoints::Set{Point}
    
    function VisibilityGraph(space::Space)
        graph = MetricGraph(2)
        vg = new(graph, space, Set{Point}())
        for polygon in space.obstacles
            for point in polygon
                addwaypoint!(vg, point)
            end
        end

        return vg
    end
end

"""
Adds a point to the visibility graph
Only attempts to connect to the waypoints, so if adding multiple query points they
will not be connected together
"""
function MetricTools.add!(vg::VisibilityGraph, point::Point)::Bool
    if !contains(vg.space, point) return false end
    if contains(vg.graph, point) return false end

    if !add!(vg.graph, point; usenn=false) return false end
    for waypoint in vg.waypoints
        line = (point, waypoint)
        if !intersects(vg.space.obstacles, line; onedge=false)
            add!(vg.graph, line)
        end
    end
    
    return true
end

"""
Adds a goal, which becomes a waypoint
"""
function addwaypoint!(vg::VisibilityGraph, waypoint::Point)::Bool
    # if !contains(vg.space, waypoint) return false end
    added::Bool = add!(vg, waypoint)
    if added push!(vg.waypoints, waypoint) end
    return added
end

"""
Removes a point from the visibility graph
"""
MetricTools.remove!(vg::VisibilityGraph, point::Point)::Bool =
    remove!(vg.graph, point)

"""
Removes a goal, also removing it as a waypoint
"""
function removewaypoint!(vg::VisibilityGraph, waypoint::Point)::Bool
    removed::Bool = remove!(vg, waypoint)
    if removed pop!(vg.waypoints, waypoint) end
    return removed
end

"""
Returns the path of a query to the visiblity graph
Start and goal are temporarily added
"""
function MotionPlanning.plan(
        vg::VisibilityGraph,
        start::Point,
        goal::Point
        )::Array{Point}
    
    goaladded::Bool = addwaypoint!(vg, goal)
    startadded::Bool = add!(vg, start)
    
    path::Array{Point} = astar(vg.graph, start, goal)

    if startadded remove!(vg, start) end
    if goaladded removewaypoint!(vg, goal) end
    
    return path
end


"""
Returns the distance function to a specified goal.
If precompute is set to true, then all distances are computed first and saved in a dictionary
Make sure that the length is set correctly when using precompute (must be consistent)
"""
function MetricTools.distfunc(vg::VisibilityGraph, goal::Point;
        precompute::Bool=true,
        length::Integer=100,
        bounds::Array{Bound}=vg.space.bounds
        )::Function
        
    if !precompute return (p::Point) -> metric(vg, p, goal) end
    
    println("Building distance cache...")
    points::Array{Point} = build_matrix_pts(length, bounds)
    
    addwaypoint!(vg, goal)
    @showprogress for point in points
        add!(vg, point)
    end
    
    println("Running Dijkstras...")
    ds::LightGraphs.DijkstraState = dijkstras(vg.graph, goal)
    dists::Array{Float64} = ds.dists
    pointdists::Dict{Point, Float64} = Dict{Point, Float64}(
        p => dijkstradist(vg.graph, p, dists)
        for p in points
    )
    
    removewaypoint!(vg, goal)
    
    println("Distance cache completed.")
    return (p::Point) -> pointdists[p]
end

"""
Plots the Visibility graph
"""
function MetricTools.plot(vg::VisibilityGraph; ax=PyCall.PyNULL()::PyCall.PyObject)
    plot(vg.graph; ax=ax, linecolor="black")
    plot(vg.space.obstacles; ax=ax)
end
end
