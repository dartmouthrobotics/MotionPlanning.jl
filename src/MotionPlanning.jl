#=
 * File: MotionPlanning.jl
 * Project: planners
 * File Created: Tuesday, 13th August 2019 11:12:15 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Saturday, 21st March 2020 6:26:53 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module MotionPlanning
using MetricTools
export
    PRMs, RRTs, VisibilityGraphs, 
    BaseCells, ShadowCells, SCDM, MetricCells,
    Planner, plan

abstract type Planner end
@mustimplement plan(planner::Planner, start::Point, goal::Point)
MetricTools.metric(planner::Planner, path::Array{Point}) =
    metric(planner.space, path)
MetricTools.metric(planner::Planner, start::Point, goal::Point) =
    metric(planner, plan(planner, start, goal))

include("./PRMs.jl")
include("./VisibilityGraphs.jl")
include("./FSTs.jl")
include("./RRTs.jl")
include("./BaseCells.jl")
include("./ShadowCells.jl")
include("./SCDM.jl")
include("./MetricCells.jl")

include("./RegressionGraphs/Connectors.jl")
include("./RegressionGraphs/SampleCells.jl")
include("./RegressionGraphs/RegressionCells.jl")
include("./RegressionGraphs/RecursiveCellRoadmaps.jl")
include("./RegressionGraphs/RegressionGraphs.jl")

# include("./WSSpanner.jl")
end
