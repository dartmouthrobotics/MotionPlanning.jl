#=
File: MetricCells.jl
Project: MotionPlanning
Author: Luyang Zhao (luyang.zhao.gr@dartmouth.edu)
-----
Last Modified: 3rd Nov 2020 
Modified By: Luyang Zhao (luyang.zhao.gr@dartmouth.com)
=#

module MetricCells

using PyCall
# using Base.Threads
import Base.Threads.@spawn
# using Distributed
import PyPlot, LinearAlgebra
import Distances
# using ProgressMeter
using DataStructures
# using SharedArrays
# using Polyhedra
# using QHull
using MetricTools, MetricTools.Geometry, MetricTools.KNNs, MetricTools.MetricGraphs, MetricTools.Visualization
using MetricSpaces, MetricSpaces.Samplers
using MetricSpaces.Planar
using LightGraphs, MetaGraphs, SimpleWeightedGraphs
using ..MotionPlanning
const Use_center_point = false
const Use_penalty_function = false
# const metric = Distances.Chebyshev()
# const metric=Distances.Euclidean()



export MetricCell, construct!, construct_global_graph, query, plot_env_and_paths, label_visited_mix_cells, inside_bounds, query_path_and_construct_global_graph_using_Astar,
       video1, video2, plot_upper_and_lower, plot_cells_traversed, max_side_size, construct_global_graph_with_s_g, update_cell_graph_blocked, update_cell_graph,
       check_contain_start_and_goal, video3, update_cell_graph_blocked_opt, update_cell_graph_opt, video_arm, classify_mcells, classify_mcells2, generate_points,
       update_points_and_edges_opt, construct_global_graph_with_s_g2, construct_global_graph_with_s_g3, astar_diy,a_star_diy, lp_astar_initialize_diy, construct_global_graph_for_lower_case,
       lp_astar_compute_shortest_path_opt_diy, update_cell_graph_blocked_opt3!, update_points_and_edges_opt3!

function add_edge_with_weight!(g::MetricGraph, v1::Integer, v2::Integer, dis::Float64)
    edge = MetaGraphs.Edge(v1, v2)
    MetaGraphs.add_edge!(g.graph, edge)
    MetaGraphs.set_prop!(g.graph, edge, :weight, dis)  
end



mutable struct MetricCell
    space::Space
    bounds::Array{Bound}
    center::Point
    inner_r::Float64
    side_length::Float64
    children::Array{MetricCell}
    depth::Int64
    n_pts::Integer
    is_free::Bool
    is_free_not_conv::Bool
    mix_blocked::Bool
    mixed::Bool
    added_to_global_graph::Bool
    block_prob::Float64
    points::Array{Point}

    function MetricCell(space::Space, bounds::Array{Bound}, depth::Int64, center::Point, inner_r::Float64, side_length::Float64; n_pts::Integer=20, points::Array{Point}=[])
        cell::MetricCell = new(
            space,
            bounds, 
            center,
            inner_r,
            side_length,
            [], 
            depth,
            n_pts,
            false,
            false,
            false,
            false,
            false,
            0.0,
            points
            )
        return cell
    end
    # MetricCell() = new()
end


"""
check whether a point inside the cell
"""
function contains(cell::MetricCell, point::Point)

    function contains_helper(b::Bound, x)
        return b.lo <= x <= b.hi
    end
    dim = cell.space.dim
    result = true
    for i in 1:dim
        @inbounds result *= contains_helper(cell.bounds[i], point[i])
    end
    return result
end

"""
Return two new bounds objects that each contain half of the input bounds.
"""
function split(b::Bound)
    middle::Float64 = (b.lo + b.hi)/2.0
    return (Bound(b.lo, middle), Bound(middle, b.hi))
end

function cell_dis_to_obs(cell::MetricCell)
    # if !isfree(cell.space, center) return -0.1 end ###TODO: made modification here
    # d_safe::Float64, n_points::Array{Point} = cert(cell.space, center) 
    d_safe::Float64 = cert(cell.space, cell.center)
    return d_safe
end


function sub_convex(cell::MetricCell, d_safe::Float64) # if the outer cell is free, then the current cell is free
    ###TODO: set it to equal now, it's wrong!!!!
    # if d_safe >= cell.inner_r*2 || isapprox(d_safe, cell.inner_r*2) ## the radius of the outer cell
    if d_safe >= cell.inner_r || isapprox(d_safe, cell.inner_r) ## the radius of the outer cell
        cell.is_free = true 
        return true
    else
        if d_safe >= cell.inner_r || isapprox(d_safe, cell.inner_r)
            cell.is_free_not_conv = true
        end
        return false
    end
end

function inside_bounds(cell::MetricCell, polygon::Array{Point})
    return MetricTools.contains(polygon, cell.center)
end



function is_mix(cell::MetricCell, delta::Float64) # todo: modify to fit for high dimension
    # All center point goes in this function much not free
    penetrate_depth::Float64 = obscert(cell.space, cell.center; delta=delta)
    use_bruteforce = false
    if use_bruteforce
        result = false
        for i in range(cell.bounds[1].lo, length = 5, stop = cell.bounds[1].hi)
            for j in range(cell.bounds[2].lo, length = 5, stop = cell.bounds[2].hi)
                @inbounds pt::Point = [i, j]
                if isfree(cell.space, pt) # if cell is small and there are pts free, then it's mix
                    result =  true
                    break
                end
            end
        end
        return result
    else
        if cell.inner_r <= penetrate_depth 
            return false
        else
            if penetrate_depth >= 0.5*cell.inner_r
                cell.mix_blocked = true
            end
            cell.block_prob = penetrate_depth/cell.inner_r
            return true
        end
    end
end


function divide_metric_cells(root_cell::MetricCell, epsilon::Float64, delta::Float64, start::Point, goal::Point; dim::Integer=2)  ## can depends on depth, or depends on size 
    s = Stack{MetricCell}()   
    push!(s, root_cell)
    while !isempty(s)
        cell = pop!(s)
        d_safe = cell_dis_to_obs(cell)
        if sub_convex(cell, d_safe) continue end
        if d_safe < delta/2.0 && cell.side_length <= delta/(sqrt(dim)) continue end # set this kind of cell to be blocked
        if is_mix(cell, delta)  ## if d_safe < 0 and !is_mix()  then just end as blocked cell
            if cell.side_length <= epsilon/(sqrt(dim)) #epsilon 
                cell.mixed = true 
            else
                divide_mod(cell, dim)
                for sub_cell in cell.children
                    push!(s, sub_cell)
                end
            end
        end
    end
end

# function divide_metric_cells2(cell::MetricCell, epsilon::Float64, delta::Float64, start::Point, goal::Point; dim::Integer=2)  
#     function divide_helper(cell)
#         if cell.side_length <= epsilon
#             ## stop
#             d_safe = cell_dis_to_obs(cell)
#             if sub_convex(cell, d_safe) 
#                 return
#             elseif  d_safe > 0  || is_mix(cell, delta) 
#                 cell.mixed = true
#                 return
#             end
#             ## default to be blocked
#         else 
#             divide_mod(cell, dim)
#             for sub_cell in cell.children
#                 divide_helper(sub_cell)
#             end
#         end
#     end
#     divide_helper(cell)
# end 

# function divide_metric_cells(cell::MetricCell, epsilon::Float64, delta::Float64, start::Point, goal::Point; dim::Integer=2)  ## can depends on depth, or depends on size 
#     function divide_helper(cell)
#         d_safe = cell_dis_to_obs(cell)
#         if sub_convex(cell, d_safe) return 0 end
#         if d_safe <= delta/2.0 && cell.side_length <= delta/(sqrt(dim)) return 0 end # set this kind of cell to be blocked
#         if d_safe > 0  || is_mix(cell, delta)  ## if d_safe < 0 and !is_mix()  then just end as blocked cell
#             if cell.side_length <= epsilon/(sqrt(dim)) #epsilon 
#                 cell.mixed = true
#             else
#                 divide_mod(cell, dim)
#                 for sub_cell in cell.children
#                     divide_helper(sub_cell)
#                 end
#             end
#         end
#         return 0
#     end
#     divide_helper(cell)
# end

# function generate_points2(c_center, half_side, dim)
#     # println("center points is: ", c_center)
#     new_points::Array{Point} = []
#     for i in 1:dim
#         new_p1::Point = deepcopy(c_center)
#         new_p2::Point = deepcopy(c_center)
#         new_p1[i] -= new_p1[i]
#         new_p2[i] += new_p2[i]
#         push!(new_points, new_p1)
#         push!(new_points, new_p2)
#     end
#     # println("half side is: ", half_side)
#     # println("new_points are: ", new_points)
#     # println("length: ", length(new_points))
#     return new_points
# end

        


function generate_points(bounds)
    # println("bounds goes inside this func: ", bounds)
    bds = [[b.lo, b.hi] for b in bounds]
    comb_bds = Base.Iterators.product(bds...)
    points::Array{Point} = []
    for i in comb_bds
        @inbounds push!(points, [i...])
    end
    return points
end
    
function divide_mod(cell, dim)
    bd_split_array = []
    for i in 1:dim
        @inbounds i_th_bounds= split(cell.bounds[i])
        push!(bd_split_array, i_th_bounds)
    end
    results = deepcopy(collect(Base.Iterators.product(bd_split_array...)))  ###!!!!
    
    
    children = Array{MetricCell}(undef, 2^dim)
    # for res in results
    #     println("res is: ", res)
    # end
    function divide_mod_helper(i,c)
        c_bounds::Array{Bound} = [c...]
        c_center::Point = [(bd.hi+bd.lo)/2.0 for bd in c_bounds]
        points::Array{Point} = generate_points(c_bounds)
        # points::Array{Point} = generate_points2(c_center, cell.side_length/4.0, dim)
        child_cell = MetricCell(cell.space, c_bounds, cell.depth+1, c_center, cell.inner_r/2.0, cell.side_length/2.0; n_pts = cell.n_pts, points=points) ## depth+1 here #!!!!!!!! TODO, IF USE DEPTH, may need to change
        children[i] = child_cell
    end

    Threads.@threads for i in 1: 2^dim
        @inbounds divide_mod_helper(i,results[i])
    end

    cell.children = children
    # for cell in cell.children
    #     println(cell.bounds)
    # end

end
        

function get_all_leaves(cell) ### Get all type of leaves
    q = Queue{MetricCell}()
    enqueue!(q, cell)
    fleaves::Array{MetricCell} = []
    mleaves::Array{MetricCell} = []
    bleaves::Array{MetricCell} = []
    while !isempty(q)
        node = dequeue!(q)
        if !isempty(node.children)
            for child in node.children
                enqueue!(q, child)
            end
        else
            if node.is_free 
                push!(fleaves,node) 
            else
                if node.mixed
                    push!(mleaves,node) 
                else
                    push!(bleaves,node)
                end
            end
        end
    end
    return fleaves, mleaves, bleaves
end

function classify_mcells(mleaves::Array{MetricCell})
    free_mcells::Array{MetricCell} = []
    n_free_mcells::Array{MetricCell} = []
    for cell in mleaves
        if cell.is_free_not_conv
            push!(free_mcells, cell)
        else
            push!(n_free_mcells, cell)
        end
    end
    return free_mcells, n_free_mcells
end

function classify_mcells2(mleaves::Array{MetricCell})
    block_mcells::Array{MetricCell} = []
    n_block_mcells::Array{MetricCell} = []
    for cell in mleaves
        if cell.mix_blocked
            push!(block_mcells, cell)
        else
            push!(n_block_mcells, cell)
        end
    end
    return block_mcells, n_block_mcells
end


function construct!(space, root_cell, start::Point, goal::Point; epsilon::Float64=0.05, delta::Float64=0.05)
    divide_metric_cells(root_cell, epsilon, delta, start, goal; dim=space.dim)              
    # divide_metric_cells2(root_cell, epsilon, delta, start, goal; dim=space.dim)                                  
    fleaves, mleaves, bleaves = get_all_leaves(root_cell)
    return fleaves, mleaves, bleaves
end

"""
Construct global graph without start and goal => add start and goal later
"""
function construct_global_graph(leaves)  ###New version 
    ### Create a global_graph
    space = leaves[1].space
    global_graph::MetricGraph = MetricGraph(space.dim)
    for leaf in leaves
        update_cell_graph(leaf, global_graph)
    end
    # plot(global_graph)
    return global_graph
end

function check_contain_start_and_goal(cell::MetricCell, start::Point, goal::Point)
    contains_both::Array{Bool} = [false, false]
    target_point_array::Array{Point} = []
    if contains(cell, start)
        push!(target_point_array, start)
        contains_both[1] = true
    end
    if contains(cell, goal)
        push!(target_point_array, goal)
        contains_both[2] = true
    end
    return target_point_array, contains_both
end
# function add_mleaves_to_global_graph(start::Point, goal::Point, mleaves, global_graph)

function construct_global_graph_with_s_g(start::Point, goal::Point, leaves)
    ## The error is that should add all goal cells' points in beforehand
    ## otherwise, those points will not set up connection with goal point
    space = leaves[1].space
    if !isfree(space, start) || !isfree(space, goal)
        println("WRONG!!! either start or goal are not free")
    end
    global_graph::MetricGraph = MetricGraph(space.dim)
    contains_both::Array{Bool} = [false, false]
    target_point_array::Array{Point} = []
    # Threads.@threads for leaf in leaves # add all points into global graph
    for leaf in leaves
        target_point_array, current_contain_both = check_contain_start_and_goal(leaf, start, goal)
        update_cell_graph(leaf, global_graph, target_point_array = target_point_array)
        contains_both = [i || j for (i, j) in zip(contains_both, current_contain_both)]
    end
    # println("contains_both: ", contains_both)
    if !contains_both[1] MetricGraphs.add!(global_graph, start) end
    if !contains_both[2] MetricGraphs.add!(global_graph, goal)  end

    return global_graph

end

function construct_global_graph_for_lower_case(start::Point, goal::Point, mleaves::Array{MetricCell}, global_graph2, pointids2, all_points2, final_id)
    m_g = deepcopy(global_graph2)
    pointids = deepcopy(pointids2)
    all_points = deepcopy(all_points2)
    for cell in mleaves
        ## add all points
        new_points::Array{Point} = []
        target_point_array, current_contain_both = check_contain_start_and_goal(cell, start, goal)
        for target_point in target_point_array
            if !(target_point in cell.points)
                push!(cell.points, target_point)
            end
        end
        for p in cell.points
            if !haskey(pointids, p)
                final_id += 1
                pointids[p] = final_id
                m_g[final_id] = Dict()
                push!(all_points, p)
                

            end
        end
        if Use_center_point 
            final_id += 1
            pointids[cell.center] = final_id
            m_g[final_id] = Dict()
            push!(all_points, cell.center)
        end
        ### extract all edges
        n = length(cell.points)
        if Use_center_point
            v1 = pointids[cell.center] 
            for p in cell.points
                v2 = pointids[p] 
                dis = metric(cell.space, cell.center, p)
                #!!!MADE CHANGE HERE
                dis += Use_penalty_function ? 1.0/(dis*10) : 0
                # dis += 1.0/(dis*5)
                m_g[v1][v2] = dis
                m_g[v2][v1] = dis  #find a way to make it more efficent?
            end
        else
            for p_i in 1:n
                for p_j in (p_i+1):n
                    @inbounds temp_p1 = cell.points[p_i]
                    @inbounds temp_p2 = cell.points[p_j]
                    v1 = pointids[temp_p1] 
                    v2 = pointids[temp_p2] 
                    # println("m_g: ", m_g)
                    if !haskey(m_g[v1], v2)          
                        dis = metric(cell.space, temp_p1, temp_p2)
                        #!!!MADE CHANGE HERE
                        dis += Use_penalty_function ? 1.0/(dis*10) : 0
                        # dis += 1.0/(dis*5)
                        m_g[v1][v2] = dis
                        m_g[v2][v1] = dis  #find a way to make it more efficent?
                    end
                end
            end
        end
    end
    return m_g, pointids, all_points, final_id
end
    


"""For this version, not use metagraph, but just use simpleweightedgraph """
function construct_global_graph_with_s_g3(start::Point, goal::Point, leaves::Array{MetricCell})
   
    # println("the value of Use_center_point: ", Use_center_point)
    # Use_center_point = true
    all_points::Array{Point} = []
    pointids::Dict{Point, Integer} = Dict()
    final_id = 0
    TIME1 = 0
    TIME2 = 0
    if isempty(leaves)
        println("wrong!!! empty leaves")
        return
    end
    space = leaves[1].space
    if !isfree(space, start) || !isfree(space, goal)
        println("WRONG!!! either start or goal are not free")
    end
    m_g::Dict{Integer, Dict{Integer, Float64}} = Dict()
    # m_g = Dict()
    contains_both::Array{Bool} = [false, false]
    target_point_array::Array{Point} = []
    for cell in leaves
        ## add all points
        new_points::Array{Point} = []
        target_point_array, current_contain_both = check_contain_start_and_goal(cell, start, goal)
        for target_point in target_point_array
            if !(target_point in cell.points)
                push!(cell.points, target_point)
            end
        end
        for p in cell.points
            if !haskey(pointids, p)
                final_id += 1
                cur = @elapsed pointids[p] = final_id
                cur1 = @elapsed m_g[final_id] = Dict()
                cur2 = @elapsed push!(all_points, p)
                TIME2 += cur
                TIME2 += cur1
            end
        end
        if Use_center_point 
            final_id += 1
            pointids[cell.center] = final_id
            m_g[final_id] = Dict()
            push!(all_points, cell.center)
            
        end
        ### extract all edges
        n = length(cell.points)
        if Use_center_point
            v1 = pointids[cell.center] 
            for p in cell.points
                v2 = pointids[p] 
                dis = metric(cell.space, cell.center, p)
                cur_time = @elapsed m_g[v1][v2] = dis
                cur_time2 = @elapsed m_g[v2][v1] = dis  #find a way to make it more efficent?
                TIME1 += cur_time
                TIME1 += cur_time2
            end
        else
            for p_i in 1:n
                for p_j in (p_i+1):n
                    @inbounds temp_p1 = cell.points[p_i]
                    @inbounds temp_p2 = cell.points[p_j]
                    v1 = pointids[temp_p1] 
                    v2 = pointids[temp_p2] 
                    # println("m_g: ", m_g)
                    if !haskey(m_g[v1], v2)          
                        dis = metric(cell.space, temp_p1, temp_p2)
                        cur_time = @elapsed m_g[v1][v2] = dis
                        cur_time2 = @elapsed m_g[v2][v1] = dis  #find a way to make it more efficent?
                        TIME1 += cur_time
                        TIME1 += cur_time2
                    end
                end
            end
        end
        contains_both = [i || j for (i, j) in zip(contains_both, current_contain_both)]
    end
    # println("contains_both: ", contains_both)
    if !contains_both[1]
        final_id += 1
        pointids[start] = final_id
        m_g[final_id] = Dict()
        push!(all_points, start)
        
    end
    if !contains_both[2]
        final_id += 1
        pointids[goal] = final_id
        m_g[final_id] = Dict()
        push!(all_points, goal)
    end

    ### add all edges
    # edges = keys(edges_dis) 
    # for e in edges
    #     add_edge_with_weight!(m_g, e[1], e[2], edges_dis[e])
    # end
    println("the total time cost on add vertices is: ", TIME2)
    println("the total time cost on add edge is: ", TIME1)
    println("the num of vertices: ", length(all_points))
    # count = 0
    # for key in keys(m_g)
    #     count += length(keys(m_g[key]))
    # end
    # println("the number of double edges: ", count, " single: ", count/2)

    return m_g, pointids, all_points, final_id
   

end



function construct_global_graph_with_s_g2(start::Point, goal::Point, leaves)
    # println("the value of Use_center_point: ", Use_center_point)
    # Use_center_point = true
    TIME1 = 0
    TIME2 = 0
    if isempty(leaves)
        println("wrong!!! empty leaves")
        return
    end
    space = leaves[1].space
    if !isfree(space, start) || !isfree(space, goal)
        println("WRONG!!! either start or goal are not free")
    end

    m_g::MetricGraph = MetricGraph(space.dim)
    contains_both::Array{Bool} = [false, false]
    target_point_array::Array{Point} = []
    # edges_dis = Dict()
    weights::MetaGraphs.MetaWeights = MetaGraphs.weights(m_g.graph)
    for cell in leaves
        ## add all points
        new_points::Array{Point} = []
        target_point_array, current_contain_both = check_contain_start_and_goal(cell, start, goal)
        for target_point in target_point_array
            if !(target_point in cell.points)
                push!(cell.points, target_point)
            end
        end
        for p in cell.points
            if !MetricGraphs.contains(m_g, p)
                push!(new_points, p)
            end
        end
        curr = @elapsed all_added = MetricGraphs.add!(m_g, new_points)
        if Use_center_point MetricGraphs.add!(m_g, cell.center) end
        TIME2 += curr
        ### extract all edges
        n = length(cell.points)
        if Use_center_point
            v1 = get_vertex(m_g, cell.center) 
            for p in cell.points
                v2 = get_vertex(m_g, p) 
                dis = metric(cell.space, cell.center, p)
                cur_time = @elapsed add_edge_with_weight!(m_g, v1, v2, dis)
                TIME1 += cur_time
            end
        else
            for p_i in 1:n
                for p_j in (p_i+1):n
                    @inbounds temp_p1 = cell.points[p_i]
                    @inbounds temp_p2 = cell.points[p_j]
                    v1 = get_vertex(m_g, temp_p1) 
                    v2 = get_vertex(m_g, temp_p2) 
                    pre_dis = weights[v1, v2]
                    if pre_dis == Inf
                        dis = metric(cell.space, temp_p1, temp_p2)
                        cur_time = @elapsed add_edge_with_weight!(m_g, v1, v2, dis)
                        TIME1 += cur_time
                    end
                end
            end
        end
        contains_both = [i || j for (i, j) in zip(contains_both, current_contain_both)]
    end
    # println("contains_both: ", contains_both)
    if !contains_both[1] MetricGraphs.add!(m_g, start) end  ### TODO: could use this to see start/goal are in mix cells
    if !contains_both[2] MetricGraphs.add!(m_g, goal)  end

    ### add all edges
    # edges = keys(edges_dis) 
    # for e in edges
    #     add_edge_with_weight!(m_g, e[1], e[2], edges_dis[e])
    # end
    println("the total time cost on add edge is: ", TIME1)
    println("the total time cost on add vertices is: ", TIME2)
    println("num of vertices2: " , LightGraphs.nv(m_g.graph))
    println("num of edges: " , LightGraphs.ne(m_g.graph))
    return m_g
   

end



function query_path_and_construct_global_graph_using_Astar(start::Point, goal::Point, leaves)::Tuple{Array{Point}, Float64}
    ### The error is that should add all goal cells' points in beforehand
    ### otherwise, those points will not set up connection with goal point
    space = leaves[1].space
    if !isfree(space, start) || !isfree(space, goal)
        return [], Inf
    end
    global_graph::MetricGraph = MetricGraph(space.dim)
    target_point_array::Array{Point} = []
    for leaf in leaves
        if contains(leaf, start) push!(target_point_array, start) end
        if contains(leaf, goal) push!(target_point_array, goal) end
        if !isempty(target_point_array)
            update_cell_graph(leaf, global_graph, target_point_array = target_point_array)
            leaf.added_to_global_graph = true
            add_points_into_global_graph(target_point_array,leaves, global_graph)
        end
    end

    function add_all_cells_contain_p(p_i::Integer, global_graph::MetricGraph)
        p::Point = get_point(global_graph, p_i) 
        # println("the point is: ", p)
        for leaf in leaves
            if contains(leaf, p)
                if !leaf.added_to_global_graph
                    update_cell_graph(leaf, global_graph)
                    leaf.added_to_global_graph = true
                end
            end
        end
    end
   
    if !MetricGraphs.contains(global_graph, goal)
        MetricGraphs.add!(global_graph, goal)
    end
    path::Array{Point} = MetricGraphs.changed_astar(global_graph, start, goal, add_all_cells_contain_p, length(leaves))
    path_dis::Union{Nothing,Float64} = distance(global_graph, path)
    if isnothing(path_dis) ## or isempty(path)
        return [], Inf
    else
        return path, path_dis
    end

end


function update_cell_graph_blocked_opt3!(cell, m_g, pointids, all_points::Array{Point}, s, g_score, rhs, pq, heuristic, came_from; target_point_array::Array{Point}=[])
    # Use_center_point = true

    function update_edge_block(v1, v2)
        if v1 != s && came_from[v1] == v2
            rhs[v1] = Inf
            came_from[v1] = -1 ###!!!TODO: DO we need to change came_from?
            for nn in keys(m_g[v1]) #LightGraphs.outneighbors(m_g.graph, v1)
                if nn == v1 continue end
                temp = g_score[nn]+ m_g[nn][v1]
                # println("pt: ", nn , " g_score[nn]: ", g_score[nn], " weights[nn, v1]: ",weights[nn, v1])
                if temp < rhs[v1]
                    came_from[v1] = nn
                    rhs[v1] = temp
                end
            end
            updateNode_opt_diy(v1, g_score, rhs, pq, heuristic, all_points)
        end
        if v2 != s && came_from[v2] == v1
            rhs[v2] = Inf
            came_from[v2] = -1
            for nn in keys(m_g[v2]) #LightGraphs.outneighbors(m_g.graph, v2)
                if nn == v2 continue end
                temp = g_score[nn]+ m_g[nn][v2]
                if temp < rhs[v2]
                    came_from[v2] = nn
                    rhs[v2] = temp
                end
            end
            updateNode_opt_diy(v2,g_score, rhs, pq, heuristic, all_points)
        end
    end

    ### remove all edges
    if Use_center_point
        v1 = pointids[cell.center]
        for p in cell.points
            v2 = pointids[p]
            # m_g[v1][v2] = Inf
            # m_g[v2][v1] = Inf
            # println("v1 is: ", v1)
            # println("v2 is: ", v2)
            # println(m_g[v1])
            # println(m_g[v2])
            delete!(m_g[v1], v2)
            delete!(m_g[v2], v1)
            # println("after delete")
            # println(m_g[v1])
            # println(m_g[v2])
            ##TODO: Not sure which one is faster: delete or change to Inf
            update_edge_block(v1, v2)
        end
    else
        n = length(cell.points)
        for p_i in 1:n
            for p_j in (p_i+1):n
                @inbounds temp_p1 = cell.points[p_i]
                @inbounds temp_p2 = cell.points[p_j]
                v1 = pointids[temp_p1] 
                v2 = pointids[temp_p2] 
                delete!(m_g[v1], v2)
                delete!(m_g[v2], v1)
                update_edge_block(v1, v2)
            end
        end
    end

end



function update_cell_graph_blocked_opt(cell, m_g::MetricGraph, s, g_score, rhs, pq, heuristic, came_from; target_point_array::Array{Point}=[])
    # Use_center_point = true
    
    weights::MetaGraphs.MetaWeights = MetaGraphs.weights(m_g.graph)

    function update_edge_block(v1, v2)
        if v1 != s && came_from[v1] == v2
            rhs[v1] = Inf
            came_from[v1] = -1 ###!!!TODO: DO we need to change came_from?
            for nn in LightGraphs.outneighbors(m_g.graph, v1)
                if nn == v1 continue end
                temp = g_score[nn]+ weights[nn, v1]
                # println("pt: ", nn , " g_score[nn]: ", g_score[nn], " weights[nn, v1]: ",weights[nn, v1])
                if temp < rhs[v1]
                    came_from[v1] = nn
                    rhs[v1] = temp
                end
            end
            updateNode_opt(v1,s, g_score, rhs, pq, heuristic, m_g, came_from)
        end
        if v2 != s && came_from[v2] == v1
            rhs[v2] = Inf
            came_from[v2] = -1
            for nn in LightGraphs.outneighbors(m_g.graph, v2)
                if nn == v2 continue end
                temp = g_score[nn]+ weights[nn, v2]
                if temp < rhs[v2]
                    came_from[v2] = nn
                    rhs[v2] = temp
                end
            end
            updateNode_opt(v2,s, g_score, rhs, pq, heuristic, m_g, came_from)
        end
    end

    # for cell in cells  ##TODO: include all cells??
        ### remove all edges
        if Use_center_point
            v1 = get_vertex(m_g, cell.center) 
            for p in cell.points
                v2 = get_vertex(m_g, p) 
                # remove!(m_g, temp_p1, temp_p2) 
                set_weight!(m_g, cell.center, p, Inf)  #!!!! Which one to use
                update_edge_block(v1, v2)
            end
        else
            n = length(cell.points)
            for p_i in 1:n
                for p_j in (p_i+1):n
                    @inbounds temp_p1 = cell.points[p_i]
                    @inbounds temp_p2 = cell.points[p_j]
                    v1 = get_vertex(m_g, temp_p1) 
                    v2 = get_vertex(m_g, temp_p2) 
                    set_weight!(m_g, temp_p1, temp_p2, Inf)  #!!!! Which one to use
                    update_edge_block(v1, v2)
                end
            end
        end
    # end
end


function update_cell_graph_blocked(cell, m_g::MetricGraph; target_point_array::Array{Point}=[])
    weights::MetaGraphs.MetaWeights = MetaGraphs.weights(m_g.graph)
    for target_point in target_point_array
        if !(target_point in points)
            push!(cell.points, target_point)
        end
    end
    n = length(cell.points)
    for p_i in 1:n
        for p_j in (p_i+1):n
            @inbounds temp_p1 = cell.points[p_i]
            @inbounds temp_p2 = cell.points[p_j]
            set_weight!(m_g, temp_p1, temp_p2, Inf)  ### TODO: need to change to remove the edge
            # remove!(m_g, temp_p1, temp_p2) 
        end
    end
    return points
end

function update_points_and_edges_opt3!(cells, m_g, pointids, all_points, start, goal, s, g_score, rhs, pq, heuristic, came_from, final_id)
    # Use_center_point = true
    
    if isempty(cells) return final_id end

    function update_edge(v1, v2, dis)
        # pre_dis = weights[v1, v2]
        if !haskey(m_g[v1], v2)
        # if pre_dis > dis
            m_g[v1][v2] = dis
            m_g[v2][v1] = dis  #find a way to make it more efficent?
            if rhs[v1] > g_score[v2] + dis
                came_from[v1] = v2
                rhs[v1] = g_score[v2] + dis
                updateNode_opt_diy(v1, g_score, rhs, pq, heuristic, all_points)
            end
            if rhs[v2] > g_score[v1] + dis
                came_from[v2] = v1
                rhs[v2] = g_score[v1] + dis
                updateNode_opt_diy(v2, g_score, rhs, pq, heuristic, all_points)
            end
        else
            println("ges in here????????")  ##todo: if not goes in here, just delte?
            if v1 != s && came_from[v1] == v2
                # println("before: ", g_score[v1], " ", rhs[v1] )
                rhs[v1] = Inf
                came_from[v1] = -1 ###!!!TODO: DO we need to change came_from?
                for nn in keys(m_g[v1])#LightGraphs.outneighbors(m_g.graph, v1)
                    if nn == v1 continue end
                    temp = g_score[nn]+ m_g[nn][v1]
                    # println("pt: ", nn , " g_score[nn]: ", g_score[nn], " weights[nn, v1]: ",weights[nn, v1])
                    if temp < rhs[v1]
                        came_from[v1] = nn
                        rhs[v1] = temp
                    end
                end
                # if came_from[v1] == v2 came_from[v1]=-1 end
                updateNode_opt_diy(v1,g_score, rhs, pq, heuristic, all_points)
            end
            if v2 != s && came_from[v2] == v1
                rhs[v2] = Inf
                came_from[v2] = -1
                for nn in keys(m_g[v2]) #LightGraphs.outneighbors(m_g.graph, v2)
                    if nn == v2 continue end
                    temp = g_score[nn]+ m_g[nn][v2]
                    if temp < rhs[v2]
                        came_from[v2] = nn
                        rhs[v2] = temp
                    end
                end
                # if came_from[v1] == v2 came_from[v1]=-1 end
                updateNode_opt_diy(v2, g_score, rhs, pq, heuristic, all_points)
            end
        end
    end

    for cell in cells
        #!!!MADE CHANGE HERE
        k = cell.mixed ? 1 : 0
        ## add all points
        new_points::Array{Point} = []
        target_point_array, current_contain_both = check_contain_start_and_goal(cell, start, goal)
        for target_point in target_point_array
            if !(target_point in cell.points)
                push!(cell.points, target_point)
            end
        end
        for p in cell.points
            if !haskey(pointids, p)
                final_id += 1
                pointids[p] = final_id
                m_g[final_id] = Dict()
                push!(all_points, p)
                
            end
        end
        if Use_center_point 
            final_id += 1
            pointids[cell.center] = final_id
            m_g[final_id] = Dict()
            push!(all_points, cell.center)
            
        end
        ### extract all edges
        n = length(cell.points)
        if Use_center_point
            v1 = pointids[cell.center] 
            for p in cell.points
                v2 = pointids[p] 
                dis = metric(cell.space, cell.center, p)
                #!!!MADE CHANGE HERE
                dis += Use_penalty_function ? k*1.0/(dis*10) : 0
                # dis += k * (1.0/(dis*5))
                update_edge(v1, v2, dis)
            end
        else
            for p_i in 1:n
                for p_j in (p_i+1):n
                    @inbounds temp_p1 = cell.points[p_i]
                    @inbounds temp_p2 = cell.points[p_j]
                    v1 = pointids[temp_p1] 
                    v2 = pointids[temp_p2] 
                    # println("m_g: ", m_g)
                    if !haskey(m_g[v1], v2)          
                        dis = metric(cell.space, temp_p1, temp_p2)
                        #!!!MADE CHANGE HERE
                        dis += Use_penalty_function ? 1.0/(dis*10) : 0
                        # dis += k * (1.0/(dis*5))
                        update_edge(v1, v2, dis)
                    end
                end
            end
        end
    end
    return final_id
end

function update_points_and_edges_opt(cells, m_g::MetricGraph, start, goal, s, g_score, rhs, pq, heuristic, came_from)
    # Use_center_point = true
    
    if isempty(cells) return end
    weights::MetaGraphs.MetaWeights = MetaGraphs.weights(m_g.graph)

    function update_edge(v1, v2, dis)
        pre_dis = weights[v1, v2]
        if pre_dis > dis
            add_edge_with_weight!(m_g, v1, v2, dis)
            if rhs[v1] > g_score[v2] + dis
                came_from[v1] = v2
                rhs[v1] = g_score[v2] + dis
                updateNode_opt(v1,s, g_score, rhs, pq, heuristic, m_g, came_from)
            end
            if rhs[v2] > g_score[v1] + dis
                came_from[v2] = v1
                rhs[v2] = g_score[v1] + dis
                updateNode_opt(v2,s, g_score, rhs, pq, heuristic, m_g, came_from)
            end
        else
            if v1 != s && came_from[v1] == v2
                # println("before: ", g_score[v1], " ", rhs[v1] )
                rhs[v1] = Inf
                came_from[v1] = -1 ###!!!TODO: DO we need to change came_from?
                for nn in LightGraphs.outneighbors(m_g.graph, v1)
                    if nn == v1 continue end
                    temp = g_score[nn]+ weights[nn, v1]
                    # println("pt: ", nn , " g_score[nn]: ", g_score[nn], " weights[nn, v1]: ",weights[nn, v1])
                    if temp < rhs[v1]
                        came_from[v1] = nn
                        rhs[v1] = temp
                    end
                end
                # if came_from[v1] == v2 came_from[v1]=-1 end
                updateNode_opt(v1,s, g_score, rhs, pq, heuristic, m_g, came_from)
            end
            if v2 != s && came_from[v2] == v1
                rhs[v2] = Inf
                came_from[v2] = -1
                for nn in LightGraphs.outneighbors(m_g.graph, v2)
                    if nn == v2 continue end
                    temp = g_score[nn]+ weights[nn, v2]
                    if temp < rhs[v2]
                        came_from[v2] = nn
                        rhs[v2] = temp
                    end
                end
                # if came_from[v1] == v2 came_from[v1]=-1 end
                updateNode_opt(v2,s, g_score, rhs, pq, heuristic, m_g, came_from)
            end
        end
    end

    for cell in cells
        ## add all points
        new_points::Array{Point} = []
        target_point_array, current_contain_both = check_contain_start_and_goal(cell, start, goal)
        for target_point in target_point_array
            if !(target_point in cell.points)
                push!(cell.points, target_point)
            end
        end
        for p in cell.points
            if !MetricGraphs.contains(m_g, p)
                push!(new_points, p)
            end
        end
        all_added = MetricGraphs.add!(m_g, new_points)
        if Use_center_point MetricGraphs.add!(m_g, cell.center) end
        ### extract all edges
        if Use_center_point
            v1 = get_vertex(m_g, cell.center) 
            for p in cell.points
                v2 = get_vertex(m_g, p) 
                dis = metric(cell.space, cell.center, p)
                update_edge(v1, v2, dis)
            end
        else
            n = length(cell.points)
            for p_i in 1:n
                for p_j in (p_i+1):n
                    @inbounds temp_p1 = cell.points[p_i]
                    @inbounds temp_p2 = cell.points[p_j]
                    v1 = get_vertex(m_g, temp_p1) 
                    v2 = get_vertex(m_g, temp_p2) 
                    dis = metric(cell.space, temp_p1, temp_p2)
                    update_edge(v1, v2, dis)
                end
            end
        end
    end
end


function update_cell_graph_opt(cell, m_g::MetricGraph, s, g_score, rhs, pq, heuristic, came_from; target_point_array=target_point_array)
    # println("current new cells is: ", cell.bounds)
    for target_point in target_point_array
        if !(target_point in cell.points)
            push!(cell.points, target_point)
        end
    end
    new_points::Array{Point} = []
    for p in cell.points
        if !MetricGraphs.contains(m_g, p)
            push!(new_points, p)
        end
    end
    # new_points::Array{Point} = [p for p in cell.points if !MetricGraphs.contains(m_g, p) ]
    all_added = MetricGraphs.add!(m_g, new_points)
    weights::MetaGraphs.MetaWeights = MetaGraphs.weights(m_g.graph)
    n = length(cell.points)
    for p_i in 1:n
        for p_j in (p_i+1):n
            @inbounds temp_p1 = cell.points[p_i]
            @inbounds temp_p2 = cell.points[p_j]
            v1 = get_vertex(m_g, temp_p1) 
            v2 = get_vertex(m_g, temp_p2) 
            pre_dis = weights[v1, v2]
            dis = metric(cell.space, temp_p1, temp_p2)
            if pre_dis > dis
                add_edge_with_weight!(m_g, v1, v2, dis)
                if rhs[v1] > g_score[v2] + dis
                    came_from[v1] = v2
                    rhs[v1] = g_score[v2] + dis
                    updateNode_opt(v1,s, g_score, rhs, pq, heuristic, m_g, came_from)
                end
                if rhs[v2] > g_score[v1] + dis
                    came_from[v2] = v1
                    rhs[v2] = g_score[v1] + dis
                    updateNode_opt(v2,s, g_score, rhs, pq, heuristic, m_g, came_from)
                end
            else
                if v1 != s && came_from[v1] == v2
                    # println("before: ", g_score[v1], " ", rhs[v1] )
                    rhs[v1] = Inf
                    came_from[v1] = -1 ###!!!TODO: DO we need to change came_from?
                    for nn in LightGraphs.outneighbors(m_g.graph, v1)
                        if nn == v1 continue end
                        temp = g_score[nn]+ weights[nn, v1]
                        # println("pt: ", nn , " g_score[nn]: ", g_score[nn], " weights[nn, v1]: ",weights[nn, v1])
                        if temp < rhs[v1]
                            came_from[v1] = nn
                            rhs[v1] = temp
                        end
                    end
                    # if came_from[v1] == v2 came_from[v1]=-1 end
                    updateNode_opt(v1,s, g_score, rhs, pq, heuristic, m_g, came_from)
                end
                if v2 != s && came_from[v2] == v1
                    rhs[v2] = Inf
                    came_from[v2] = -1
                    for nn in LightGraphs.outneighbors(m_g.graph, v2)
                        if nn == v2 continue end
                        temp = g_score[nn]+ weights[nn, v2]
                        if temp < rhs[v2]
                            came_from[v2] = nn
                            rhs[v2] = temp
                        end
                    end
                    # if came_from[v1] == v2 came_from[v1]=-1 end
                    updateNode_opt(v2,s, g_score, rhs, pq, heuristic, m_g, came_from)
                end
            end
        end
    end
end


function update_cell_graph(cell, global_graph::MetricGraph; target_point_array=[])
    for target_point in target_point_array
        if !(target_point in cell.points)
            push!(cell.points, target_point)
        end
    end
    new_points::Array{Point} = []
    for p in cell.points
        if !MetricGraphs.contains(global_graph, p)
            push!(new_points, p)
        end
    end
    # new_points::Array{Point} = [p for p in cell.points if !MetricGraphs.contains(global_graph, p)]
    all_added = MetricGraphs.add!(global_graph, new_points)
    n = length(cell.points)
    weights::MetaGraphs.MetaWeights = MetaGraphs.weights(global_graph.graph)
    for p_i in 1:n
        for p_j in (p_i+1):n
            @inbounds temp_p1 = cell.points[p_i]
            @inbounds temp_p2 = cell.points[p_j]
            v1 = get_vertex(global_graph, temp_p1) 
            v2 = get_vertex(global_graph, temp_p2) 
            pre_dis = weights[v1, v2]
            if pre_dis == Inf
                dis = metric(cell.space, temp_p1, temp_p2)
                add_edge_with_weight!(global_graph, v1, v2, dis)
            end
        end
    end
end



function update_pt_into_global_graph(global_graph, cell, pt::Point)
    if isfree(cell.space, pt)
        if !MetricGraphs.contains(global_graph, pt)
            all_added = MetricGraphs.add!(global_graph, pt) #add this pt
            for b_p in cell.points
                dis = metric(cell.space, b_p, pt)
                v1, v2 = get_vertex(g, b_p), get_vertex(g, pt)
                add_edge_with_weight!(global_graph, v1, v2, dis) #add edges
            end
        else
            # println("This point is already contained in graph, not need to add again")
        end
    else
        println("This point is not free!!! Shoudn't be added")
    end
end

function add_points_into_global_graph(pts, leaves, global_graph)
    for p in pts
        added = add_point_into_global_graph(p,leaves, global_graph)
        if !added
            return false
        end
    end
    return true
end

function add_point_into_global_graph(pt, leaves, global_graph)
    added = false
    for leaf in leaves
        if contains(leaf, pt) 
            update_pt_into_global_graph(global_graph, leaf, pt)
            added = true
        end
    end
    return added
end

function find_path(start::Point, goal::Point, global_graph)
    #plot(global_graph)
    s_i = global_graph.pointids[start]
    g_i = global_graph.pointids[goal]
    path_from_a_Star::Array{Point} = MetricGraphs.astar(global_graph, start, goal)
    dis_from_a_Star::Union{Nothing,Float64} = distance(global_graph, path_from_a_Star)
    return path_from_a_Star, dis_from_a_Star
end

function label_visited_mix_cells(path::Array{Point}, leaves::Array{MetricCell}; interpolate=false) ### TODO: may not need to set visited, instead, just pick those cells out
    if interpolate
        temp_path::Array{Point} = []
        for i in 1:length(path) - 1
            @inbounds append!(temp_path, [path[i], (path[i]+path[i+1])/2, path[i+1]])
        end
        path = temp_path
    end
    mvisited = []
    # for i in 1:length(path)-1
    #     for cell in leaves
    #         if !(cell in mvisited) #avoid duplication
    #             if contains(cell, path[i]) && contains(cell, path[i+1])
    #                 if cell.mixed 
    #                     push!(mvisited, cell)
    #                 end
    #             end
    #         end
    #     end
    # end
    ##!!!! The ordinary used one
    for p in path
        for cell in leaves
            if !(cell in mvisited) #avoid duplication
                if contains(cell, p) 
                    if cell.mixed 
                        push!(mvisited, cell)
                    end
                end
            end
        end
    end
    return mvisited
end
"""
Query: Find a path and refine the path
"""
function query(start::Point,goal::Point,leaves,global_graph)
    space = leaves[1].space
    if isfree(space, start) && isfree(space, goal)
        added = add_points_into_global_graph([start, goal],leaves, global_graph)
        if !added return [], Inf end
        path::Array{Point}, path_dis = find_path(start, goal, global_graph)
        if isnothing(path_dis) ## or isempty(path)
            return [], Inf
        else
            return path, path_dis
        end
    else
        return [], Inf
    end 
end


function is_legal_path(path::Array{Point}, space)
    for p in path
        if !isfree(space, p)
            return false
        end
    end
    for i in 1:length(path)-1
        if !localplanner(space, path[i], path[i+1])
            return false
        end
    end
end


function plot_cell_rec(bounds, ax::PyObject; facecolor::String="grey", zorder::Integer=2)

    x_bounds =  bounds[1]
    y_bounds =  bounds[2]
    poly::Polygon = Polygon([
        [x_bounds.lo, y_bounds.lo],
        [x_bounds.hi, y_bounds.lo],
        [x_bounds.hi, y_bounds.hi],
        [x_bounds.lo, y_bounds.hi]
    ])
    poly_array::Array{Polygon} = [poly]
    plot(poly_array; ax=ax, facecolor=facecolor, zorder=zorder, alpha=0.3)

end

function plot_real_env(leaves,ax; title="Environment and paths", plot_obstacle::Bool=true)
    ax.title.set_text(title)
    # plot cells division in workspace
    for leaf in leaves
        plot_cell_rec(leaf.bounds, ax, facecolor="w", zorder=1)
        # MetricGraphs.plot(leaf.shadow_cell.prm.graph; fig=fig, ax=ax) ### plot the graph -for generate all-pairs
    end
    if plot_obstacle plot(leaves[1].space.obstacles; ax=ax, alpha=0.5) end
end




function plot_env_and_paths(
    ;
    free_leaves::Array{MetricCell}=[],
    bleaves::Array{MetricCell}=[],
    mleaves::Array{MetricCell}=[],
    ax=PyCall.PyNULL()::PyCall.PyObject,
    type::String = "",
    name::String = "Path and Environment",
    path::Union{Array{Point}, Array}=[],
    if_plot_env::Bool=true,
    if_plot_path::Bool=false,
    serial::Integer=1,
    plot_obstacle::Bool = true,
    save::Bool = true
    )
    if ax == PyCall.PyNULL() ax = PyPlot.gca() end
    ax.set_aspect("equal")
    if if_plot_env plot_real_env(mleaves,ax; title = name, plot_obstacle=plot_obstacle) end
    ax.set_ylim(ymin=free_leaves[1].space.bounds[2].lo, ymax=free_leaves[1].space.bounds[2].hi)
    ax.set_xlim(xmin=free_leaves[1].space.bounds[1].lo, xmax=free_leaves[1].space.bounds[1].hi)

    if !isempty(free_leaves)
        for leaf in free_leaves
            plot_cell_rec(leaf.bounds, ax, facecolor="green", zorder=2)
        end
    end
    if !isempty(bleaves)
        for leaf in bleaves
            # println("leaf bounds: ", leaf.bounds)
            plot_cell_rec(leaf.bounds, ax, facecolor="yellow", zorder=2)
            # MetricGraphs.plot(leaf.shadow_cell.prm.graph; fig=fig, ax=ax) ### plot the graph -for generate all-pairs
        end
    end
    if (if_plot_path && !isempty(path))  plot(path; ax=ax, ispath=true, color="red", zorder=4) end
    if save PyPlot.savefig("data/output/images/"*string(type,name," ",serial)) end
    return ax
end

function plot_cells_traversed(leaves, ax)
    for leaf in leaves
        if leaf.added_to_global_graph 
            plot_cell_rec(leaf.bounds, ax; facecolor="r", zorder=1)
        end
        # MetricGraphs.plot(leaf.shadow_cell.prm.graph; fig=fig, ax=ax) ### plot the graph -for generate all-pairs
    end
end


function plot_upper_and_lower(
    fleaves,
    fmleaves;
    dirname::String = "ul",
    name1::String = "Path and Environment",
    name2::String = "Path and Environment",
    path1::Union{Array{Point}, Array}=[],
    path2::Union{Array{Point}, Array}=[],
    if_plot_env::Bool=true,
    if_plot_path::Bool=true,
    serial::Integer=1,
    bleaves=[],
    u_bleaves=[]
    )
    fig, axes = PyPlot.subplots(1,2)
    axes[1].set_aspect("equal")
    axes[2].set_aspect("equal")
    if if_plot_env plot_real_env(fleaves,axes[1]; title = name1) end
    if if_plot_env plot_real_env(fmleaves,axes[2]; title = name2) end
    
    if !isempty(u_bleaves)
        for leaf in u_bleaves
            plot_cell_rec(leaf.bounds, axes[1], facecolor="yellow", zorder=2)
        end
    end
    if !isempty(bleaves)
        for leaf in bleaves
            plot_cell_rec(leaf.bounds, axes[2], facecolor="yellow", zorder=2)
        end
    end

    if (if_plot_path && !isempty(path1))  plot(path1; ax=axes[1], ispath=true, color="red", zorder=4) end
    if (if_plot_path && !isempty(path2))  plot(path2; ax=axes[2], ispath=true, color="red", zorder=4) end

    PyPlot.savefig("data/output/images/upperlower/"*string(dirname,serial))
    # return ax
end

function video2(uppername::String, lowername::String)
    println("goes in side")
    compilemp4("data/output/images/upper/";
    outdir="data/output/videos/upper/", 
    iformat=uppername*"%d.png", 
    outfile="upper-maze.mp4")

    compilemp4("data/output/images/lower/";
    outdir="data/output/videos/lower/", 
    iformat=lowername*"%d.png",
    outfile="lower-maze.mp4") 
end

function video1(; dirname::String = "ul")
    compilemp4("data/output/images/upperlower/";
    outdir="data/output/videos/upperlower/", 
    framerate=1,
    iformat=dirname*"%d.png",
    outfile="lower-maze.mp4") 
end
function video3(; dirname::String = "ul")
    compilemp4("data/output/images/arm-lower/";
    outdir="data/output/videos/", 
    framerate=1,
    iformat="2DArm%d.png",
    outfile="arm-lower.mp4") 
end

function reconstruct_path_diy!(total_path, # a vector to be filled with the shortest path
    came_from, # a vector holding the parent of each node in the A* exploration
    end_idx, # the end vertex
    g) # the graph

    curr_idx = end_idx
    while came_from[curr_idx] != curr_idx
        pushfirst!(total_path, (came_from[curr_idx], curr_idx))
        curr_idx = came_from[curr_idx]
    end
end


function a_star_impl_diy!(g, # the graph
    goal, # the end vertex
    open_set, # an initialized heap containing the active vertices
    closed_set, # an (initialized) color-map to indicate status of vertices
    g_score, # a vector holding g scores for each node
    f_score, # a vector holding f scores for each node
    came_from, # a vector holding the parent of each node in the A* exploration
    # distmx,
    heuristic)

    # E = edgetype(g)
    # total_path = Vector{E}()
    total_path = []
    @inbounds while !isempty(open_set)
        current = dequeue!(open_set)

        if current == goal
            println("got dis: ", g_score[goal])
            reconstruct_path_diy!(total_path, came_from, current, g)
            return total_path
        end

        closed_set[current] = true

        for neighbor in keys(g[current]) #LightGraphs.outneighbors(g, current)
            closed_set[neighbor] && continue

            tentative_g_score = g_score[current] + g[current][neighbor] #distmx[current, neighbor]

            if tentative_g_score < g_score[neighbor]
                g_score[neighbor] = tentative_g_score
                priority = tentative_g_score + heuristic(neighbor)
                open_set[neighbor] = priority
                came_from[neighbor] = current
            end
        end
    end
    return total_path
end

function a_star_diy(g, s, t, nv, heuristic::Function)

# function a_star_diy(g, # the g
#     s::Integer,                       # the start vertex
#     t::Integer, 
#     nv::Interger,                     # the end vertex
#     # distmx::AbstractMatrix{T}=weights(g),
#     heuristic::Function=n -> zero(T)) where {T}

 

    # E = Edge{eltype(g)}

    # if we do checkbounds here, we can use @inbounds in a_star_impl!
    # checkbounds(distmx, Base.OneTo(nv(g)), Base.OneTo(nv(g)))

    # open_set = PriorityQueue{Integer, T}() ##todo: what is t
    open_set = PriorityQueue()
    enqueue!(open_set, s, 0)

    closed_set = zeros(Bool, nv)

    g_score = fill(Inf, nv)
    g_score[s] = 0

    f_score = fill(Inf, nv)
    f_score[s] = heuristic(s)

    came_from = -ones(Integer, nv)
    came_from[s] = s

    a_star_impl_diy!(g, t, open_set, closed_set, g_score, f_score, came_from, heuristic)
end


"""
Runs an A* Search over the graph and returns the path
"""
function astar_diy(m_g, pointids, all_points::Array{Point}, start::Point, goal::Point, space)

    vstart, vgoal = pointids[start], pointids[goal]
    heuristic::Function = (v)-> metric(space, all_points[v], goal)  ### shouldn't pre-compute
    nv::Integer = length(all_points)
    # println("what is nv: !!! ", nv)
    total_paths = a_star_diy(m_g, vstart, vgoal, nv,  heuristic)
    # weights::MetaGraphs.MetaWeights = MetaGraphs.weights(g.graph)

    # # preccompute heuristic values
    # heuristics::Dict{Integer, Float64} = Dict(
    #     v => metric(get_point(g, v), goal) for v in get_vertices(g))

    # heuristic::Function = (v)->heuristics[v]
    # edges::Array{MetaGraphs.Edge} = MetaGraphs.a_star(g.graph, vstart, vgoal, weights, heuristic)




    # heuristic::Function = (v)-> g.metric(get_point(g, v), goal)  ### shouldn't pre-compute, 
    #                                                          ### initial state doesn't have all points
    # edges::Array{MetaGraphs.Edge} = changed_a_star(g, vstart, vgoal, n_leaves, add_all_cells_contain_p,weights, heuristic)
    # println("the total path we got is: ", total_paths)
    if isempty(total_paths) return [], Inf end
    final_path = []
    dis = 0
    p1::Point = all_points[first(total_paths)[1]]
    push!(final_path, p1)
    for e in total_paths
        p2 = all_points[e[2]]
        dis += metric(space, p1, p2)
        push!(final_path, p2)
        p1 = p2
    end
    # println("final_path is: ", final_path)
    return final_path, dis
    # return vcat([all_points[first(path_edges)[1]]], [all_points[e[2]] for e in path_edges])
end

function calculateKey_diy(node, g_score, rhs, heuristic, all_points)
    return [min(g_score[node], rhs[node])+ heuristic(all_points[node]), min(g_score[node], rhs[node])]
end

function lp_astar_initialize_diy(pointids, all_points::Array{Point}, start::Point, goal::Point, space)
    Incremental_factor = 2000
    s, g = pointids[start], pointids[goal]
    heuristic::Function = (p)-> metric(space, p, goal) 
    # heuristic::Function = (v)-> metric(space, all_points[v], goal)  ### shouldn't pre-compute
    # heuristic::Function = (v)-> m_g.metric(get_point(m_g, v), goal)  ###TODO:!!!!  get_point(g::MetricGraph, v::Integer) = MetaGraphs.get_prop(g.graph, v, :point)

    nv::Integer = length(all_points)
    pq = PriorityQueue{Integer, Array{Float64}}() # change priority to be 2d 
    g_score = fill(Inf, nv*Incremental_factor)
    rhs = fill(Inf, nv*Incremental_factor) ## add rhs
    rhs[s] = 0 # start is the only one inconsistant initally
    came_from = -ones(Integer, nv*Incremental_factor)
    came_from[s] = s
    enqueue!(pq, s, calculateKey_diy(s, g_score, rhs, heuristic, all_points))
    return s, g, pq, g_score, came_from, heuristic, rhs
end

function get_top_key_diy(pq)
    if isempty(pq) return [Inf, Inf]  end
    # return peek(pq)[2]
    return peek(pq)[2]+[-0.00001, 0]
end

function updateNode_opt_diy(node, g_score, rhs, pq, heuristic, all_points)
    if (g_score[node] != rhs[node]) 
        # println("add")
        pq[node] = calculateKey_diy(node, g_score, rhs, heuristic, all_points)
    else
        if node in keys(pq) 
            delete!(pq, node)
        end
    end
end

function lp_astar_compute_shortest_path_opt_diy(
    m_g, # the graph
    start::Integer,
    goal::Integer, # the end vertex
    pq, # an initialized heap containing the active vertices
    g_score, # a vector holding g scores for each node
    came_from, # a vector holding the parent of each node in the A* exploration
    heuristic,
    rhs,
    all_points::Array{Point},
    space;
    verbose=false
    )::Tuple{Array{Point}, Float64}

    # weights::MetaGraphs.MetaWeights = MetaGraphs.weights(m_g.graph)
    # E = LightGraphs.edgetype(m_g.graph)
    # total_path::Array{MetaGraphs.Edge}  = Vector{E}()
    total_paths = []

    @inbounds while true

        if  (get_top_key_diy(pq) < calculateKey_diy(goal,g_score, rhs, heuristic, all_points)) || (rhs[goal] > g_score[goal])
            nothing
        else
            curr_idx = goal
            restart = false
            while came_from[curr_idx] != curr_idx
                # pushfirst!(total_path, E(came_from[curr_idx], curr_idx))
                curr_idx = came_from[curr_idx]
                if g_score[curr_idx] != rhs[curr_idx] || curr_idx == -1
                    # g_score[goal] = Inf
                    # rhs[goal] = Inf
                    restart = true
                    break
                end
            end
            if !restart break end
        end

    # @inbounds while (get_top_key(pq) < calculateKey(goal,g_score, rhs, heuristic)) || (rhs[goal] > g_score[goal])
        if isempty(pq) break end ##!add this line
        current = peek(pq)[1]
        if g_score[current] > rhs[current]
            g_score[current] = rhs[current]
            delete!(pq, current)
            ##Todo: Instead of using the neighbor, should use successor

            for neighbor in keys(m_g[current]) #LightGraphs.outneighbors(m_g.graph, current) 
                if neighbor == current continue end       
                temp = g_score[current]+ m_g[current][neighbor] 
                if temp < rhs[neighbor]
                    came_from[neighbor] = current
                    rhs[neighbor] = temp
                    updateNode_opt_diy(neighbor, g_score, rhs, pq, heuristic, all_points)
                end
            end
        else
            g_score[current] = Inf
            if current != start && came_from[current] == current
                println("wrong!!!!")
            end
            updateNode_opt_diy(current, g_score, rhs, pq, heuristic, all_points)
            ##Todo: Instead of using the neighbor, should use successor
            for neighbor in keys(m_g[current]) #LightGraphs.outneighbors(m_g.graph, current) 
                if neighbor == current continue end
                if neighbor != start && came_from[neighbor] == current
                    rhs[neighbor] = Inf
                    came_from[neighbor] = -1
                    for nn in keys(m_g[neighbor]) #LightGraphs.outneighbors(m_g.graph, neighbor)
                        if nn == neighbor continue end
                        temp = g_score[nn]+ m_g[nn][neighbor] 
                        if temp < rhs[neighbor]
                            came_from[neighbor] = nn
                            rhs[neighbor] = temp
                        end
                    end
                end
                updateNode_opt_diy(neighbor, g_score, rhs, pq, heuristic, all_points)
            end
        end
    end

    if verbose
        println("the num of calling update node is: ", counter)
        println("goal is: ", goal)
        println("came from goal" , came_from[goal])
        println(g_score[goal])
        println("rhs: ", rhs[goal])
    end


    if came_from[goal] != -1 && rhs[goal] != Inf
        reconstruct_path_diy!(total_paths, came_from, goal, m_g)
    else
        return [], Inf
    end
    if verbose println("total path is: ", total_paths) end

    if isempty(total_paths) return [], Inf end
    final_path = []
    dis = 0
    p1::Point = all_points[first(total_paths)[1]]
    push!(final_path, p1)
    for e in total_paths
        p2 = all_points[e[2]]
        dis += metric(space, p1, p2)
        push!(final_path, p2)
        p1 = p2
    end
    if dis == rhs[goal]
        println("dis equal to rhs[goal, no need to calculate dis")
    else
        println("Error!!!, dis: ", dis, " , rhs[goal]: ", rhs[goal])
    end
    # println("final_path is: ", final_path)
    return final_path, rhs[goal]
end




end