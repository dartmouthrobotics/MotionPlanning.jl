module WSSpanner

#region imports
import PyCall, PyPlot, LightGraphs, SimpleWeightedGraphs, GraphRecipes, Plots, GraphPlot, Compose
import Cairo.libpango, Fontconfig
using ProgressMeter, Plots.PlotMeasures

import Dates, Random

export SpannerGraph, SpannerGraphUW

rng = Random.seed!(Dates.millisecond(Dates.now()))

# now, the goal is, first
# construct spanner for unweighted graph
# then, create spanner for weighted graph
# but the construction of spanner
# is tightly coupled with the construction of PRM*

"""
flow: income an edge; check label; 
if u is selected, then v is updated; retain edge
	otherwise, if have not connected from u to this new cluster before, retain

	first need to implement a fixed version: i.e. know the graph, number of vertices; 
	then, anytime graph, 
"""

# unweighted spanner graph
struct SpannerGraphUW
	size::Int
	stretch::Int # computes as 2m - 1
	m::Int # user input number, which will yield a stretch of 2m-1
	graph::LightGraphs.SimpleGraph
	
	# set of clusters a vertex is connected to
	M::Dict
	# radius assignment for each vertex
	R::Array{Int}
	# label value for vertex
	P::Array{Int}
	# define initialize function with fixed n
	function SpannerGraphUW(m::Int, n::Int)
		# m is the stretch number, n is the number of vertices
		graph = LightGraphs.SimpleGraph(n)
		M = Dict()
		R = Array{Int}(undef, n)
		P = Array{Int}(undef, n)
		for i = 1:n
			M[i] = Array{Int}(undef, 0)
			R[i] = 0
			P[i] = i
		end
		g = new(n, 2*m-1, m, graph, M, R, P)
		return g
	end
    
    # define initialize function with not known n
    function SpannerGraphUW(m::Int)
    	n = 0
    	graph = LightGraphs.SimpleGraph(n)
		g = new(n, 2*m-1, m, graph, Dict(), Array{Int}(undef, n), Array{Int}(undef, n))
		return g
    end
    
end

# # weighted spanner graph
mutable struct SpannerGraph
	size::Int
	stretch::Int # computes as 2m - 1
	m::Int # user input number, which will yield a stretch of 2m-1
	# user input value of how close the spanner should be to perfect stretch
	# actual stretch will be (1+epsilon)(2m-1)
	epsilon::Float64 
	# number of layers
	layers::Int
	# graph itself
	graph::SimpleWeightedGraphs.SimpleWeightedGraph

	# below three structures are array of the same collection of data in unweighted graph
	# the first dimension corresponds to the number of layers in the weighted graph
	# partitioned based on epsilon
	# this, however, requires the estimate of min edge length and maximum edge length
	
	# set of clusters a vertex is connected to
	# this needs to exist in all layers
	M::Array{Dict}

	# radius assignment for each vertex
	R::Array{Int}
	# label value for vertex
	P::Array{Array{Int}}
	limit::Int

	# define initialize function with fixed n
	function SpannerGraph(m::Int, n::Int, epsilon::Float64, wMin::Float64, wMax::Float64)
		# m is the stretch number, n is the number of vertices
		# epsilon is the approximation factor, wMin/wMax is the min/max length of edge
		if (wMin < 1)
			layers = ceil(Int, log(wMax) / log(1+epsilon)) + ceil(Int, -log(wMin) / log(1+epsilon))
		else
			layers = ceil(Int, log(wMax/wMin) / log(1+epsilon))
		end
		graph = SimpleWeightedGraphs.SimpleWeightedGraph(n)
		M = Array{Dict}(undef, layers)
		R = Array{Int}(undef, n)
		P = Array{Array{Int}}(undef, layers)
		for i = 1:layers
			M[i] = Dict()
			
			P[i] = Array{Int}(undef, n)
			for j = 1:n
				M[i][j] = Array{Int}(undef, 0)
				
				P[i][j] = j
			end
		end
		limit = n
		g = new(n, 2*m-1, m, epsilon, layers, graph, M, R, P, limit)
		return g
	end
    
    # define initialize function with not known n
    function SpannerGraph(m::Int, epsilon::Float64, wMin::Float64, wMax::Float64)
		# m is the stretch number, n is the number of vertices
		# epsilon is the approximation factor, wMin/wMax is the min/max length of edge
		if (wMin < 1)
			layers = ceil(Int, log(wMax) / log(1+epsilon)) + ceil(Int, -log(wMin) / log(1+epsilon))
		else
			layers = ceil(Int, log(wMax/wMin) / log(1+epsilon))
		end
		n = 0
		graph = SimpleWeightedGraphs.SimpleWeightedGraph(n)
		M = Array{Dict}(undef, layers)
		R = Array{Int}(undef, layers)
		P = Array{Array{Int}}(undef, layers)
		for i = 1:layers
			M[i] = Dict()
			P[i] = Array{Int}(undef, n)
			# for j = 1:n
			# 	M[i][j] = Array{Int}(undef, 0)
			# 	R[i][j] = 0
			# 	P[i][j] = j
			# end
		end

		# all vertices have the same radii: m-1

		# set the limit to be a huge number, currently hard code to 10M, may change to auto large 
		limit = 10000000
		g = new(n, 2*m-1, m, epsilon, layers, graph, M, R, P, limit)
		return g
	end
end


# assign R for all vertices of g
function assignR(g::SpannerGraphUW)
	n = g.size
	p = (log(n) / n) ^ (1 / g.m)
	# construct an array, corresponding to radius 0 to m-1
	# relation: rs[i] is of radius i-1
	rs = Array{Float64}(undef, g.m)

	for i = 0:g.m-2
		rs[i+1] = p^i * (1-p)
		if i > 0
			rs[i+1] = rs[i+1] + rs[i]
		end
	end
	rs[g.m] = p^(g.m-1) + rs[g.m-1]
	# now loop over each vertex, based on the value, assign to one of these bins, 
	for i = 1:g.size
		pb = Random.rand(rng, 1)[1]
		for j = 1:g.m
			if j == g.m 
				g.R[i] = g.m-1
				break
			elseif pb < rs[j]	
				g.R[i] = j-1
				break
			end
		end
	end
end

function getBase(v::Int, g::SpannerGraphUW)
	result = mod(g.P[v], g.size);
	if result == 0
		result = g.size
	end
	return result
end

function getLevel(v::Int, g::SpannerGraphUW)
	return floor(Int, (g.P[v] - 1) / g.size)
end

# compare order of vertices
function uGv(u::Int, v::Int, g::SpannerGraphUW)
	if g.P[u] > g.P[v]
		return true
	end

	if g.P[u] == g.P[v] && u > v
		return true
	end

	return false
end



function isSelected(v::Int, g::SpannerGraphUW)
	zc = getBase(v, g)
	if getLevel(v, g) < g.R[zc]
		return true
	end
	return false
end

# detect if B(P(u))\in M(v)
function inM(b::Int, v::Int, g::SpannerGraphUW)
	l = length(g.M[v])
	for i = 1:l
		if (g.M[v])[i] == b
			return true
		end
	end

	return false
end

function retainEdge(u::Int, v::Int, g::SpannerGraphUW)
	if uGv(u, v, g)
		if isSelected(u, g)
			g.P[v] = g.P[u] + g.size
			return true
		elseif !inM(getBase(u, g), v, g)
			push!(g.M[v], getBase(u, g))
			return true
		end
	else
		# swap u and v
		if isSelected(v, g)
			g.P[u] = g.P[v] + g.size
			return true
		elseif !inM(getBase(v, g), u, g)
			push!(g.M[u], getBase(v, g))
			return true
		end
	end
	return false
end



function testSpannerUW()

	# test a spanner with m = 2, i.e. 3 spanner, and 30 vertices
	g = SpannerGraphUW(3, 30)

	assignR(g)

	# println(g.R)

	count = 0
	# now add edges, filter
	for i = 1:g.size
		for j = i+1:g.size
			# try every set of edges
			pb = Random.rand(rng, 1)[1]
			# 0.5 chance of add an edge
			# if pb > 0.5
			if true
				count += 1
				# edge from i to j
				if retainEdge(i, j, g)
					LightGraphs.add_edge!(g.graph, i, j)
				end
			end
		end
	end

	println(g.graph.ne, ", ", count)

	
	# GraphRecipes.graphplot(g.graph)
	Compose.draw(Compose.PDF(".test.pdf", 16cm, 16cm), GraphPlot.gplot(g.graph))
	# GraphPlot.gplot(g.graph)
	

	# now test the query of path length
	# ds = LightGraphs.dijkstra_shortest_paths(g.graph, 6)
	# println(ds.dists, ", ", length(ds.dists))
	for i = 1:g.size
		ds = LightGraphs.dijkstra_shortest_paths(g.graph, i)
		for j = 1:length(ds.dists)
			if ds.dists[j] > 2
				println("dist from ", i, "->", j, ": ", ds.dists[j])
			end
		end

	end
end

# function vertexAssignR(v::Int, g::SpannerGraphUW)

# end

# all above are for unweighted, test out first

# then, implement the weighted version, amortized

#######################

#######################

# below are functions designed for weighted graphs
# first, radius assignment for vertices

function assignR(g::SpannerGraph)
	n = g.size
	p = (log(n) / n) ^ (1 / g.m)
	# construct an array, corresponding to radius 0 to m-1
	# relation: rs[i] is of radius i-1
	rs = Array{Float64}(undef, g.m)

	for i = 0:g.m-2
		rs[i+1] = p^i * (1-p)
		if i > 0
			rs[i+1] = rs[i+1] + rs[i]
		end
	end
	rs[g.m] = p^(g.m-1) + rs[g.m-1]
	# now loop over each vertex, based on the value, assign to one of these bins, 
	for i = 1:g.size
		pb = Random.rand(rng, 1)[1]
		for j = 1:g.m
			if j == g.m 
				g.R[i] = g.m-1
				break
			elseif pb < rs[j]	
				g.R[i] = j-1
				break
			end
		end
	end
end


# this function adds a new vertex to the spanner graph
# in this version, all vertices have same radii: m-1
function addVertex(g::SpannerGraph)
	# first, increment graph size
	g.size += 1
	# properties to main: M, R, P

	# R
	push!(g.R, g.m-1)
	# M & P, all layers
	for i = 1:g.layers
		# new key for this vertex, initial label g.size
		g.M[i][g.size] = Array{Int}(undef, 0)
		# 
		push!(g.P[i], g.size)
	end

	# add vertex to the maintained graph
	SimpleWeightedGraphs.add_vertex!(g.graph)

	# anything else? seems no
end


function getBase(v::Int, layer::Int, g::SpannerGraph)
	result = mod(g.P[layer][v], g.limit);
	if result == 0
		result = g.limit
	end
	return result
end

function getLevel(v::Int, layer::Int, g::SpannerGraph)
	return floor(Int, (g.P[layer][v] - 1) / g.limit)
end

# compare order of vertices
function uGv(u::Int, v::Int, layer::Int, g::SpannerGraph)
	if g.P[layer][u] > g.P[layer][v]
		return true
	end

	if g.P[layer][u] == g.P[layer][v] && u > v
		return true
	end

	return false
end



function isSelected(v::Int, layer::Int, g::SpannerGraph)
	zc = getBase(v, layer, g)
	if getLevel(v, layer, g) < g.R[zc]
		return true
	end
	return false
end

# detect if B(P(u))\in M(v)
function inM(b::Int, v::Int, layer::Int, g::SpannerGraph)
	l = length(g.M[layer][v])
	for i = 1:l
		if (g.M[layer][v])[i] == b
			return true
		end
	end

	return false
end

function retainEdge(u::Int, v::Int, weight::Float64, g::SpannerGraph)
	# layer, denoted as q here: 
	q = 1 + floor(Int, log(weight)/log(1+g.epsilon))


	if uGv(u, v, q, g)
		# println("in if!")
		if isSelected(u, q, g)
			for i = q : g.layers
				if uGv(u, v, i, g)
					if getLevel(u, i, g) < g.m-1
						g.P[i][v] = g.P[i][u] + g.limit
					end
				else
					if getLevel(v, i, g) < g.m-1
						g.P[i][u] = g.P[i][v] + g.limit
					end
				end
			end
			return true
		elseif !inM(getBase(u, q, g), v, q, g)
			push!(g.M[q][v], getBase(u, q, g))
			return true
		end
	else
		# println("in else!")
		# first, test else case
		# println("in else, v: ", v, ", q: ", q, ", label: ", g.P[q][v], 
		# 	", base: ", getBase(v, q, g), ", q: ", q, ", ", inM(getBase(v, q, g), u, q, g))

		if isSelected(v, q, g)
			# println("in else, selected: ", q, ", ", g.layers)
			for i = q : g.layers
				if uGv(v, u, i, g)
					if getLevel(v, i, g) < g.m-1
						g.P[i][u] = g.P[i][v] + g.limit
					end
				else
					if getLevel(u, i, g) < g.m-1
						g.P[i][v] = g.P[i][u] + g.limit
					end
				end
			end
			return true
		elseif !inM(getBase(v, q, g), u, q, g)
			push!(g.M[q][u], getBase(v, q, g))
			return true
		end
	end
	return false
end

function allClose(a::Real, b::Real, tol=0.00001)
	if abs(a - b) < tol
		return true;
	end
	return false;
end

function testWeightedSpanner(n = 30)
	# create 30 to 50 vertices
	# randomize edge weights
	# m::Int, n::Int, epsilon::Float64, wMin::Float64, wMax::Float64
	m = 3
	# n = 30
	g = SpannerGraph(m, n, 0.1, 1.0, 100.0)
	spanner = SpannerGraph(m, n, 0.1, 1.0, 100.0)
	# assignR(g)
	assignR(spanner)

	# now, add edges, with probability
	# range in 1 to 100

	count = 0
	# now add edges, filter
	for i = 1:g.size
		for j = i+1:g.size
			# try every set of edges
			pb = Random.rand(rng, 1)[1]
			# 0.5 chance of add an edge
			# if pb > 0.5
			# first, test complete graph
			if true
				count += 1
				weight = 100 * Random.rand(rng, 1)[1];
				if weight < 1
					weight = 1 + weight
				end
				SimpleWeightedGraphs.add_edge!(g.graph, i, j, weight)
				# # # edge from i to j
				if retainEdge(i, j, weight, spanner)
					# LightGraphs.add_edge!(g.graph, i, j)
					SimpleWeightedGraphs.add_edge!(spanner.graph, i, j, weight)
				end
			end
		end
	end
	# println("limit: ", spanner.limit)
	# println(g.graph)
	println(spanner.graph)
	# println(SimpleWeightedGraphs.ne(spanner.graph) / count)
	println(SimpleWeightedGraphs.ne(spanner.graph) / SimpleWeightedGraphs.ne(g.graph))
	# filter

	# u = Random.rand(rng, 1:n, 1)[1]
	# v = Random.rand(rng, 1:n, 1)[1]
	# while v == u
	# 	v = Random.rand(rng, 1:n, 1)[1]
	# end
	# println(u, ", ", v)
	# d1 = SimpleWeightedGraphs.dijkstra_shortest_paths(g.graph, u)
	# d2 = SimpleWeightedGraphs.dijkstra_shortest_paths(spanner.graph, u)

	# println("dist: ", d1.dists[v], ", after: ", d2.dists[v], ", ratio: ", d2.dists[v] / d1.dists[v])
	maxR = 1
	for u = 1:n
		d1 = SimpleWeightedGraphs.dijkstra_shortest_paths(g.graph, u)
		d2 = SimpleWeightedGraphs.dijkstra_shortest_paths(spanner.graph, u)
		for i = 1:length(d1.dists)
			if !allClose(d1.dists[i], d2.dists[i])
				# println("u: ", u, ", v: ", i)
				ratio = d2.dists[i] / d1.dists[i]
				if ratio > maxR
					maxR = ratio
				end
				# println("dist: ", d1.dists[i], ", after: ", d2.dists[i], ", ratio: ", ratio)
			end
		end
	end
	println("maxR: ", maxR)

	# if fixed number ok, then test adding vertex on the fly

	# if all good, commit, then merge

end

function testWeightedIncremental(n = 100)
	# create 30 to 50 vertices
	# randomize edge weights
	# m::Int, n::Int, epsilon::Float64, wMin::Float64, wMax::Float64
	m = 3
	# n = 30
	g = SpannerGraph(m, 0.1, 1.0, 100.0)
	spanner = SpannerGraph(m, 0.1, 1.0, 100.0)
	# assignR(g)
	# assignR(spanner)

	# make this geometric graph? 
	println("limit: ", spanner.limit, ", layers: ", spanner.layers)
	# if manually set the limit to be small? 
	# add vertices
	count = 0
	# set max to be n
	for i = 1:n
		# add vertex
		addVertex(g)
		addVertex(spanner)
		
	end

	for i = 1:n
		for j = i+1:n
			# if j == i
			# 	continue
			# end

			pb = Random.rand(rng, 1)[1]
			# if pb > 0.7
			if true
				count += 1
				weight = 100 * Random.rand(rng, 1)[1];
				if weight < 1
					weight = 1 + weight
				end

				SimpleWeightedGraphs.add_edge!(g.graph, i, j, weight)
				# if retainEdge(i, j, weight, spanner)
				# 	SimpleWeightedGraphs.add_edge!(spanner.graph, i, j, weight)
				# end
			end
		end
	end

	# try loop over edges randomly

	for e in SimpleWeightedGraphs.edges(g.graph)
		i = SimpleWeightedGraphs.src(e)
		j = SimpleWeightedGraphs.dst(e)
		weight = SimpleWeightedGraphs.weight(e)

		# if retainEdge(i, j, weight, spanner)
		# 	SimpleWeightedGraphs.add_edge!(spanner.graph, i, j, weight)
		# end
		if retainEdge(j, i, weight, spanner)
			SimpleWeightedGraphs.add_edge!(spanner.graph, j, i, weight)
		end

	end


	
	# println("limit: ", spanner.limit)
	# println(g.graph)
	println(spanner.graph)
	# println(SimpleWeightedGraphs.ne(spanner.graph) / count)
	println(SimpleWeightedGraphs.ne(spanner.graph) / SimpleWeightedGraphs.ne(g.graph))
	# filter

	# # u = Random.rand(rng, 1:n, 1)[1]
	# # v = Random.rand(rng, 1:n, 1)[1]
	# # while v == u
	# # 	v = Random.rand(rng, 1:n, 1)[1]
	# # end
	# # println(u, ", ", v)
	# # d1 = SimpleWeightedGraphs.dijkstra_shortest_paths(g.graph, u)
	# # d2 = SimpleWeightedGraphs.dijkstra_shortest_paths(spanner.graph, u)

	# # println("dist: ", d1.dists[v], ", after: ", d2.dists[v], ", ratio: ", d2.dists[v] / d1.dists[v])
	maxR = 1
	for u = 1:n
		d1 = SimpleWeightedGraphs.dijkstra_shortest_paths(g.graph, u)
		d2 = SimpleWeightedGraphs.dijkstra_shortest_paths(spanner.graph, u)
		for i = 1:length(d1.dists)
			if !allClose(d1.dists[i], d2.dists[i])
				# println("u: ", u, ", v: ", i)
				ratio = d2.dists[i] / d1.dists[i]
				if ratio > maxR
					maxR = ratio
				end
				# println("dist: ", d1.dists[i], ", after: ", d2.dists[i], ", ratio: ", ratio)
			end
		end
	end
	println("maxR: ", maxR)

	# # if fixed number ok, then test adding vertex on the fly

	# # if all good, commit, then merge
end

end