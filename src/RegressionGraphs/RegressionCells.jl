#=
 * File: RegressionCell.jl
 * Project: RegressionGraphs
 * File Created: Friday, 10th January 2020 10:22:39 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 1st March 2020 2:58:54 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module RegressionCells

using ProgressMeter
import LightGraphs

using MetricTools, MetricTools.MetricGraphs
using MetricRegressors
using MetricSpaces, MetricSpaces.Samplers, MetricSpaces.PLRs
using ..MotionPlanning, ..SampleCells

export RegressionCell, boundary_points, precompdijkstras, reconstructplanner

"""
A regression cell has a regression of all pairs distances inside of it
"""
mutable struct RegressionCell <: Regressor
    space::Space
    cellplanner::Function
    reconstructor::Function
    regressor::Regressor
    classifier::Regressor
    bpts::Array{Point}

    function RegressionCell(
            space::Space;
            cellplanner::Function=(p1, p2) -> [p1, p2],
            reconstructor::Function=() -> cellplanner,
            regressor::Regressor=XGBoostRegressor(; epochs=12, eta=1, maxdepth=10),
            classifier::Regressor=XGBoostRegressor(; epochs=12, eta=1, maxdepth=10),
            step::Float64=0.02,
            steps::Array{Float64}=[])
        
        if isempty(steps) steps = [step for i in eachindex(space.bounds)] end
        bpts::Array{Point} = boundary_points(space.bounds; steps=steps)
        
        rc::RegressionCell = new(
            space,
            cellplanner,
            reconstructor,
            regressor,
            classifier,
            bpts)
        
        train!(rc)
        return rc
    end
end

"""
Reconstructs this cell's planner
The original planner is freed after the cell is trained
"""
function reconstructplanner(rc::RegressionCell)
    rc.cellplanner = rc.reconstructor()
end

function MotionPlanning.plan(rc::RegressionCell, point1::Point, point2::Point)
    return rc.cellplanner(point1, point2)
end

@inline MetricTools.metric(rc::RegressionCell, point1::Point, point2::Point) =
    metric(rc.space, plan(rc, point1, point2))

"""
Trains a regression cell using samples on the boundary
spb means samples per boundary dimension
"""
function MetricRegressors.train!(rc::RegressionCell)
    bpts::Array{Point} = rc.bpts
    Xf::Array{Point}, Yf::Array{Float64} = [], []
    X::Array{Point}, Yc::Array{Float64} = [], []
    
    for (i, j) in Iterators.product(eachindex(bpts), eachindex(bpts))
        if j <= i continue end

        x::Point = vcat(bpts[i], bpts[j])
        push!(X, x)
        cost::Union{Float64, Nothing} = metric(rc, bpts[i], bpts[j])
        if isnothing(cost)
            push!(Yc, 0.)
        else
            push!(Yc, 1.)
            push!(Xf, x)
            push!(Yf, cost - metric(rc.space, bpts[i], bpts[j]))
        end
    end

    println("        Training cell...")
    println("        |X| = $(length(X))")
    println("        |Y| = $(length(Yc))")
    println("        |Xf| = $(length(Xf))")
    println("        |Yf| = $(length(Yf))")
    println()
    
    # Train the classifier in any case, but only train regressor if not a collision cell
    # This cell is a collision if all metric lengths were nothing, thus Xf = []
    train!(rc.classifier, X, Yc)
    if !isempty(Xf) train!(rc.regressor, Xf, Yf) end

    # Debugging: test the error
    rmse::Float64 = mse(rc.regressor, Xf, Yf)
    kmse::Float64 = mse(rc.classifier, X, Yc)
    println("rmse: $rmse")
    println("kmse: $kmse")
    
    rc.cellplanner = (p1, p2) -> error("Called cell planner after planner discarded.")
end

"""
If not connectable, returns nothing
"""
function MetricRegressors.predict(rc::RegressionCell, X::Array{Point})
    vals::Array{Float64} = predict(rc.classifier, X)
    bools::Array{Bool} = [val > 0.6 for val in vals]
    return [
        bools[i] ? predict(rc.regressor, x) : nothing
        for (i, x) in enumerate(X)]
end

"""
Plots the regression cell given a point to slice at
"""
function MetricTools.plot(rc::RegressionCell, start::Point)
    distf::Function = (p) -> predict(rc, vcat(start, p))
    plot(distf; bounds=rc.space.bounds)
    plot(rc.space)
end

"""
Cell planner using precomputed dijkstras
Needs to produce all points for training, including those in collision
"""
function precompdijkstras(space::Space, planner; step::Float64=1/16, steps::Array{Float64}=[])
    println("Precomputing djikstras paths...")
    if isempty(steps) steps = [step for i in eachindex(space.bounds)] end

    # precompute distances for boundary points
    points::Array{Point} = boundary_points(space.bounds; steps=steps)
    @showprogress for point in points
        if isfree(space, point) add!(planner, point) end
    end
    
    println("Running Dijkstras...")
    allpaths::Dict{Tuple{Point, Point}, Array{Point}} =
        Dict{Tuple{Point, Point}, Array{Point}}()
    @showprogress for p1 in points
        if isfree(space, p1)
            ds::LightGraphs.DijkstraState = dijkstras(planner.graph, p1;
                allpaths=false, trackvertices=false)

            for p2 in points
                if isfree(space, p2)
                    allpaths[(p1, p2)] = dijkstrapath(planner.graph, p1, p2, ds.parents)
                else
                    allpaths[(p1, p2)] = []
                end
            end
        else
            for p2 in points
                allpaths[(p1, p2)] = []
            end
        end
    end
    
    return (p1::Point, p2::Point) -> begin
        try
            path = (p1, p2) in keys(allpaths) ? allpaths[(p1, p2)] : allpaths[(p2, p1)]
            return path
        catch e
            println("Point pair not saved. ($p1, $p2).")
            println("Bounds were: $(space.bounds)")
            println("Steps were: $steps")
            println(e)
            return []
        end
    end
end
end
