#=
 * File: SampleCells.jl
 * Project: RegressionGraphs
 * File Created: Saturday, 11th January 2020 2:51:10 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 1st March 2020 2:44:29 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module SampleCells

using MetricTools
using MetricSpaces

export SampleCell, boundary_points

"""
Defines a simple cell containing boundary point samples
"""
mutable struct SampleCell
    space::Space
    bpts::Array{Point}
    
    function SampleCell(
        space::Space;
        step::Float64=0.02,
        steps::Array{Float64}=[])
        
        if isempty(steps) steps = [step for i in eachindex(space.bounds)] end
        return new(
            space,
            boundary_points(space.bounds; steps=steps))
    end
end

"""
Determines if a cell is free
"""
function MetricSpaces.isfree(sc::SampleCell)::Bool
    for (i, j) in Iterators.product(eachindex(sc.bpts), eachindex(sc.bpts))
        if j <= i continue end
        if !localplanner(sc.space, sc.bpts[i], sc.bpts[j]) return false end
    end

    return true
end

"""
Gets the boundary points
"""
function boundary_points(bounds::Array{Bound}; step::Float64=0.1, steps::Array{Float64}=[])::Array{Point}
    bpts::Set{Point} = Set{Point}()
    if isempty(steps) steps = [step for i in eachindex(bounds)] end
    for (dim, bound) in enumerate(bounds)
        steppers::Array{Any} = [
            d == dim ? [b.lo] : b.lo:steps[d]:b.hi
            for (d, b) in enumerate(bounds)]
        
        for p in Iterators.product(steppers...)
            lopoint::Point = Real[p...]
            hipoint::Point = Real[p...]
            hipoint[dim] = bound.hi
            push!(bpts, lopoint, hipoint)
        end
    end
    
    return collect(bpts)
end
end
