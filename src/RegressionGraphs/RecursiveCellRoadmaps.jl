#=
 * File: HierarchicalMetricCell.jl
 * Project: RegressionGraphs
 * File Created: Saturday, 11th January 2020 12:39:53 pm
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 1st March 2020 2:29:18 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module RecursiveCellRoadmaps

import PyPlot, PyCall

using MetricTools, MetricTools.MetricGraphs
using MetricRegressors
using MetricSpaces, MetricSpaces.BinaryPartitions
using ..RegressionCells, ..MotionPlanning, ..VisibilityGraphs, ..SampleCells

export RecursiveCellRoadmap

"""
Recursive cell approach to all pairs distance calculation
"""
mutable struct RecursiveCellRoadmap <: Planner
    bp::BinaryPartition
    space::Space
    cellfactory::Function
    graph::MetricGraph
    added::Set{Point}

    function RecursiveCellRoadmap(space::Space;
            boundstep::Float64=1/20,
            steps::Array{Float64}=[],
            maxdepth::Integer=10,
            cellfactory::Function=(subspace) -> SampleCell(
                subspace,
                step=boundstep,
                steps=steps))
        
        graph::MetricGraph = MetricGraph(space.dim, metric=(p1, p2) -> metric(space, p1, p2))
        
        rcrm::RecursiveCellRoadmap = new(
            BinaryPartition(),
            space,
            cellfactory,
            graph,
            Set{Point}())
            
        function splitifobst!(node::PartitionNode)
            node.bp.data = buildcell(rcrm, node.bounds)
            return !isfree(node.bp.data)
        end
            
        settings::PartitionSettings = PartitionSettings(splitifobst!, maxdepth)
        rcrm.bp = BinaryPartition(space.bounds, settings)
        return rcrm
    end
end

"""
Builds a single cell
"""
function buildcell(rcrm::RecursiveCellRoadmap, bounds::Array{Bound})::SampleCell
    space::Space = deepcopy(rcrm.space)
    for (d, b) in enumerate(bounds)
        space.bounds[d] = b
    end
    
    rc::SampleCell = rcrm.cellfactory(space)

    # add all vertices to the graph
    for bpt in rc.bpts
        if bpt in rcrm.added continue end
        add!(rcrm.graph, bpt)
        push!(rcrm.added, bpt)
    end

    # add all pairs connections
    for bpt in rc.bpts connect!(rcrm, bpt, rc.bpts) end
    return rc
end

"""
Connects a point into the graph
"""
function MetricTools.connect!(rcrm::RecursiveCellRoadmap, bpt::Point, bpts::Array{Point})
    for other in bpts
        if bpt == other continue end
        if !localplanner(rcrm.space, bpt, other) continue end
        add!(rcrm.graph, bpt, other)
    end
end

"""
Adds a point into the RCRM
"""
function MetricTools.add!(rcrm::RecursiveCellRoadmap, point::Point)
    if point in rcrm.added return end
    add!(rcrm.graph, point)
    push!(rcrm.added, point)
    cell::SampleCell = find(rcrm.bp, point).data
    connect!(rcrm, point, cell.bpts)
end

"""
Gets a path for RRC
"""
function MotionPlanning.plan(rcrm::RecursiveCellRoadmap, start::Point, goal::Point
        )::Array{Point}
    # first add the start and goal
    for point in (start, goal)
        add!(rcrm, point)
    end
    
    path::Array{Point} = astar(rcrm.graph, start, goal)
    return path
end

"""
Plots an RRC in 2D
"""
function MetricTools.plot(rcrm::RecursiveCellRoadmap;
        ax::PyCall.PyObject=PyCall.PyNULL())
    plot(rcrm.space; ax=ax)
    # plot(rcrm.graph; ax=ax)
    plot(rcrm.bp, rcrm.space.bounds; ax=ax)
end
end
