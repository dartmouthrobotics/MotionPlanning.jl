#=
 * File: Connectors.jl
 * Project: RegressionGraphs
 * File Created: Monday, 24th February 2020 11:25:32 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Monday, 24th February 2020 2:55:04 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#


module Connectors
using MetricTools
using MetricSpaces, MetricSpaces.Samplers

export reachable

"""
Attempts to connect two points via local planner and maintaining conencted components.
"""
function reachable(space::Space, start::Point, goal::Point;
        maxattempts::Integer=100, verbose::Bool=false)::Bool
    if localplanner(space, start, goal) return true end
        
    scomponent::Set{Point} = Set{Point}([start])
    gcomponent::Set{Point} = Set{Point}([goal])
      lines::Set{Line} = Set{Line}()
    
    for i in 1:maxattempts        
        point::Point = first(uniform_random(space, 1; maxattempts=10))
        
        for spoint in scomponent
            if localplanner(space, spoint, point)
                push!(scomponent, point)
                if verbose push!(lines, (spoint, point)) end
                break
            end
        end

        for gpoint in gcomponent
            if localplanner(space, gpoint, point)
                push!(gcomponent, point)
                if verbose push!(lines, (gpoint, point)) end
                break
            end
        end
        
        if point in scomponent && point in gcomponent
            if verbose
                plot(collect(scomponent); color="red")
                plot(collect(gcomponent); color="blue")
                plot(Point[point]; color="green")
                plot(collect(lines))
                plot(space)
            end
            
            return true
        end
    end
    
    return false
end

end
