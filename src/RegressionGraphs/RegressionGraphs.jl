#=
 * File: RegressionGraphs.jl
 * Project: RegressionGraphs
 * File Created: Friday, 10th January 2020 10:22:31 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
 * Last Modified: Sunday, 1st March 2020 5:01:49 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module RegressionGraphs

import PyPlot, PyCall
using MetricTools, MetricTools.MetricGraphs
using MetricRegressors

using
    MetricSpaces,
    MetricSpaces.GridPartitions,
    MetricSpaces.PLRs

using ..RegressionCells, ..MotionPlanning, ..MotionPlanning.VisibilityGraphs, ..RecursiveCellRoadmaps

export RegressionGraph, RegressionCell, precompdijkstras, boundary_points

mutable struct RegressionGraph <: Planner
    gp::GridPartition
    space::Space
    cellfactory::Function
    graph::MetricGraph
    added::Set{Point}
    verbose::Bool
    
    """
    Constructs a regression graph
    boundstep: the step size for boundary samples
    cellplanner: regional planner for use withing cell. Must be able to connect everything in cell
    """
    function RegressionGraph(space;
            sizes::Array{Float64}=[(b.hi - b.lo) / 2 for b in space.bounds],
            cellfactory::Function=(subspace) -> RegressionCell(
                subspace;
                cellplanner=(p1, p2) -> metric(subspace, p1, p2),
                regressor=XGBoostRegressor(; epochs=12, eta=1, maxdepth=10),
                classifier=XGBoostRegressor(; epochs=12, eta=1, maxdepth=10,
                step=1/20)),
            threaded::Bool=false,
            verbose::Bool=false)

        println("Constructing regression graph...")

        rg::RegressionGraph = new(
            GridPartition(),
            space,
            cellfactory,
            MetricGraph(space.dim),
            Set{Point}(),
            verbose)
        rg.gp = GridPartition(
            sizes, space.bounds;
            initf=(bounds) -> buildcell(rg, bounds),
            threaded=threaded)
        ggraph(rg)
        return rg
    end
end
    
"""
Builds a single cell
"""
function buildcell(rg::RegressionGraph, bounds::Array{Bound})::RegressionCell
    println("   Building cell at $bounds...")
    space::Space = deepcopy(rg.space)
    for (d, b) in enumerate(bounds)
        space.bounds[d] = b
    end
    
    return rg.cellfactory(space)
end

"""
Constructs the global graph
"""
function ggraph(rg::RegressionGraph)
    for c in rg.gp.cells        
        # add all vertices
        for bpt in c.bpts
            if !(bpt in rg.added) && isfree(rg.space, bpt)
                add!(rg.graph, bpt)
                push!(rg.added, bpt)
            end
        end
        
        # add all pairs edges
        for (i, j) in Iterators.product(eachindex(c.bpts), eachindex(c.bpts))
            if j <= i continue end
            if !(c.bpts[i] in rg.added && c.bpts[j] in rg.added) continue end
            
            x::Point = vcat(c.bpts[i], c.bpts[j])
            cost::Union{Float64, Nothing} = predict(c, x)
            # cost::Union{Float64, Nothing} = metric(c, c.bpts[i], c.bpts[j])
            if !isnothing(cost)
                try
                    add!(rg.graph, c.bpts[i], c.bpts[j])
                    set_weight!(rg.graph, c.bpts[i], c.bpts[j], cost + metric(rg.space, c.bpts[i], c.bpts[j]))
                catch e
                    println("    Classifier false-positive on $(c.bpts[i]) -> $(c.bpts[j]).")
                    println("    $(c.bpts[i]): isfree? $(isfree(rg.space, c.bpts[i]))")
                    println("    $(c.bpts[j]): isfree? $(isfree(rg.space, c.bpts[j]))")
                    println(e)
                end
            end
        end
    end
end

"""
Finds the cells that could be involved when connecting two points
Two points can share at most 2 cells, in the case where they are on the same boundary
"""
function MetricSpaces.find(rg::RegressionGraph,
        p1::Point, p2::Point)::Array{RegressionCell}
    
    midpoint::Point = (p1 .+ p2) ./ 2
    rcs::Array{RegressionCell} = [find(rg.gp, midpoint)]
    for d in eachindex(p1)
        if p1[d] == p2[d]
            nrc::Union{RegressionCell, Nothing} = find(rg.gp, midpoint; bounddim=d)
            if !isnothing(nrc) push!(rcs, nrc) end
        end
    end

    return rcs
end

"""
Reconstructs the incomplete path from the waypoints on the global graph
"""
function reconstruct(rg::RegressionGraph, path::Array{Point})
    println("    Reconstructing path...")
    display(path)
    println("    Pre-reconstruction cost: $(distance(rg.graph, path)))")
    if rg.verbose plot(path; ispath=true, color="red", zorder=4, linestyle="dashed", alpha=0.6) end
    if isempty(path) return path end
    
    reconstructed::Array{Point} = []
    for (p1, p2) in pathlines(path)
        # if connectible, connect directly
        if localplanner(rg.space, p1, p2)
            push!(reconstructed, p1)
            continue
        end
        
        # try planning using any cells that might be involved
        # with connecting the two points
        rcs::Array{RegressionCell} = find(rg, p1, p2)
        midpath::Array{Point} = []
        for rc in rcs reconstructplanner(rc)
            midpath = plan(rc, p1, p2)
            if isempty(midpath)
                println("Reconstruction planner failed between $p1 and $p2.")
                println("Trying next cell...")
            else
                break
            end
        end
        
        pop!(midpath)
        push!(reconstructed, midpath...)
    end
    
    push!(reconstructed, last(path))
    return reconstructed
end

"""
Plans using the regression graph
"""
function MotionPlanning.plan(rg::RegressionGraph, start::Point, goal::Point;
        plotggraph::Bool=false)::Array{Point}
    for point in (start, goal)
        if point in rg.added continue end
        add!(rg, point)
    end
    
    if plotggraph plot(rg.graph) end
    
    println("    Running A* Search...")
    path::Array{Point} = astar(rg.graph, start, goal)
    reconstructed::Array{Point} = @time reconstruct(rg, path)
    return reconstructed
end

"""
Connects a point into the graph
"""
function MetricTools.connect!(rg::RegressionGraph, bpt::Point, bpts::Array{Point})
    for other in bpts
        if bpt == other continue end
        if !localplanner(rg.space, bpt, other) continue end
        add!(rg.graph, bpt, other)
    end
end

"""
Adds a point into the RCRM
"""
function MetricTools.add!(rg::RegressionGraph, point::Point)
    if point in rg.added return end
    add!(rg.graph, point)
    push!(rg.added, point)
    cell::RegressionCell = find(rg.gp, point)
    connect!(rg, point, cell.bpts)
end

"""
Gets the number of parameters for the regression graph
"""
function MetricRegressors.getnumparams(rg::RegressionGraph)::Integer
    fps::Integer = 0
    for cell in rg.gp.cells
        fps += getnumparams(cell.regressor) + getnumparams(cell.classifier)
    end

    return fps
end

"""
Distance function 
"""
function MetricTools.distfunc(rg::RegressionGraph)
    # TODO
    return (p) -> nothing
end

"""
Plots an RG in 2D
"""
function MetricTools.plot(rg::RegressionGraph;
        ax::PyCall.PyObject=PyCall.PyNULL())
    plot(rg.space; ax=ax)
    plot(rg.gp; ax=ax)
    # plot(distfunc(rg); bounds=rg.space.bounds, ax=ax) 
end
end
