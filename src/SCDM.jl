module SCDM
import PyPlot
using PyCall
using DataStructures
using BlackBoxOptim
using MetricTools, MetricTools.MetricGraphs
using
    MetricSpaces,
    MetricSpaces.Planar,
    MetricSpaces.Samplers,
    MetricSpaces.BinaryPartitions,
    MetricSpaces.PLRs
using ..ShadowCells
using ..BaseCells
using ..PRMs, ..VisibilityGraphs
using LightGraphs, MetaGraphs
import MotionPlanning

export construct_scdm, plot_scdm, plot_env_and_paths, plot_lines, query_path_and_construct_global_graph_using_Astar,
        total_FP_of_one_rplr,total_FP_of_xgboost,get_memory_cost_of_SCDM,total_FP_of_prm,
        plot_real_env,plot_vg_path


"""
Adds an edge to the graph by points and weight ####### added by luyang
"""
function add_edge_with_weight!(g::MetricGraph, point1::Point, point2::Point, dis::Float64)
    vertex1, vertex2 = get_vertex(g, point1), get_vertex(g, point2)
    edge = MetaGraphs.Edge(vertex1, vertex2)
    MetaGraphs.add_edge!(g.graph, edge)
    MetaGraphs.set_prop!(g.graph, edge, :weight, dis)  #g.metric(point1, point2)
end



"""
Gets pre-computed distance for edge by two points --Added by luyang
"""
function edge_distance(g::MetricGraph, p1::Point, p2::Point)::Real
    vertex1 = g.pointids[p1]
    vertex2 = g.pointids[p2]
    edge = MetaGraphs.Edge(vertex1, vertex2)
    # return distance(g,edge)
    return MetaGraphs.get_prop(g.graph, edge, :weight)
end

function contains(b::Bound, x)
    return b.lo <= x <= b.hi
end

"""
Return two new bounds objects that each contain half of the input bounds.
"""
function split(b::Bound)
    middle = (b.lo + b.hi)/2
    return (Bound(b.lo, middle), Bound(middle, b.hi))
end

## difference between parent and root_cell, parent is in dividing, but root_cell is the same size cell in neighbor
## which upated current cell's dis_to_goal function
mutable struct Cell
    space::Space
    bounds::Array{Bound}
    children::Array{Cell}
    parent::Union{Nothing,Cell} # This array only have one element
    neighbors::Array{Cell}
    shadow_cell::Union{Nothing, ShadowCells.ShadowCell}
    base_cell::Union{Nothing, BaseCells.BaseCell}
    depth::Int64
    visited::Bool
    num_pts_per_edge::Integer
    added_to_global_graph::Bool
    function Cell(space::Space, bounds::Array{Bound}, depth::Int64; parent=nothing::Union{Nothing, Cell}, num_pts_per_edge::Integer=20)
        cell::Cell = new(
            space,
            bounds, 
            [], 
            parent, 
            [], 
            nothing::Union{Nothing, ShadowCells.ShadowCell}, 
            nothing::Union{Nothing, BaseCells.BaseCell},
            depth,
            false,
            num_pts_per_edge,
            false
            )
        return cell
    end
    # Cell() = new()
end

"""
check whether a point inside the cell
"""
function contains(cell::Cell, point)
    dim = cell.space.dim
    result = true
    for i in 1:dim
        result *= contains(cell.bounds[i], point[i])
    end
    return result

end

"""
use depth to recursively keep dividing the total_cell
"""

function divide_scdm(cell, depth ; dim::Integer=2)
    if cell.depth == depth
        nothing
    else
        divide(cell, cell.depth%dim+1)
        for sub_cell in cell.children
            divide_scdm(sub_cell, depth; dim=dim)
        end
    end
end

"""
Subdivide a cell by adding two children cells, along the axis direction specified. axis=1 is x axis, axis=2 is y axis, etc.
"""
function divide(cell::Cell, axis::Int)
    left, right = split(cell.bounds[axis])

    bounds1 = copy(cell.bounds)
    bounds1[axis] = left

    bounds2 = copy(cell.bounds)
    bounds2[axis] = right

    cell1 = Cell(cell.space, bounds1, cell.depth+1; parent=cell, num_pts_per_edge=cell.num_pts_per_edge)
    cell2 = Cell(cell.space, bounds2, cell.depth+1; parent=cell, num_pts_per_edge=cell.num_pts_per_edge)
    cell.children = [cell1,cell2]

end

""" 
find the cell that contains current point from all_leaves using function contains
"""
function find_current_nodes(point::Point, all_leaves)
    cells = []
    for leaf in all_leaves
        if contains(leaf, point)
            push!(cells, leaf)
        end
    end
    return cells
end





"""
leaves here exclude base_cells
base_cells + leaves = all leaves
"""
function get_all_leaves(cell)
    q = Queue{Cell}()
    enqueue!(q, cell)
    leaves = []
    while !isempty(q)
        node = dequeue!(q)
        if !isempty(node.children)
            enqueue!(q, node.children[1])
            enqueue!(q, node.children[2])
        else
            push!(leaves,node)
        end
    end

    return leaves
end


"""
    Use visited and finished to distinguish cells(not normal BFS)
    visited: this node already visited by one of its parent node(can still be updated if new parent node supply lower value)
    finished: all children of this node have been visited already, this node cannot be updated any more
"""

function generate_points(cell)
    dim = cell.space.dim
    num = cell.num_pts_per_edge
    num_edges_per_cell = dim * 2^(dim -1)

    bounds_array = [[b.lo,b.hi] for b in cell.bounds]
    points::Array{Point}= []
    for i in 1: dim
        t_array = deepcopy(bounds_array)
        deleteat!(t_array, i)
        part_edges = [[comb...] for comb in collect(Base.Iterators.product(t_array...))]
        ## add change of ith dimension to part_edges
        ith_bound = cell.bounds[i]
        ith_values = [ii for ii in range(ith_bound.lo, stop=ith_bound.hi, length = num)]
        for p_e in part_edges
            for i_v in ith_values
                p_e_copy = copy(p_e)
                temp_p::Point = insert!(p_e_copy,i,i_v)
                if !(temp_p in points)
                    push!(points, temp_p)
                end
            end
        end
        #TODO: make it simplier=> avoid two for loop
    end
    ###finished: delete common points on edges

    return points

end


function update_pt_into_global_graph(global_graph, cell, pt::Point ; inner_pt::Bool=false)
    if isfree(cell.space, pt)
        if !MetricGraphs.contains(global_graph, pt)
            all_added = MetricGraphs.add!(global_graph, pt) #add this pt
            points::Array{Point} = generate_points(cell) 
            free_points::Array{Point} = [p for p in points if isfree(cell.space, p)] ## These pts should now all in global_graph
            if !inner_pt 
                for b_p in free_points
                    dis = cell.shadow_cell.predict(b_p, pt)  
                    if dis != -2  ## if == -2, not add
                        dis += MetricGraphs.metric(global_graph ,b_p, pt)
                        add_edge_with_weight!(global_graph, b_p, pt, dis) #add edges
                    end
                end
            else
                for b_p in free_points
                    if localplanner(cell.space, pt,b_p)
                        dis = MetricGraphs.metric(global_graph ,b_p, pt)
                        add_edge_with_weight!(global_graph, b_p, pt, dis) #add edges
                    else ##todo: try to find one intermediate point, if cannot, then ignore
                        inter_p, min_d = find_optimal_intermediate_point(cell, pt, b_p)
                        if min_d > 1000 # only one intermediate point doesn't work
                            nothing  ## todo: may try to find two intermediate points then
                        else
                            if !MetricGraphs.contains(global_graph, inter_p)
                                all_added = MetricGraphs.add!(global_graph, inter_p) #add this pt
                            end
                            dis1 = MetricGraphs.metric(global_graph,pt,inter_p)
                            dis2 = MetricGraphs.metric(global_graph,b_p,inter_p)
                            add_edge_with_weight!(global_graph, inter_p, pt,  dis1)
                            add_edge_with_weight!(global_graph, inter_p, b_p, dis2)
                        end
                    end
                end
            end
        else
            # println("This point is already contained in graph, not need to add again")
        end
    else
        println("This point is not free!!! Shoudn't be added")
    end
end

"""
    for each cell, generate_points and put points and edges into graph
"""
function update_cell_graph(cell, global_graph::MetricGraph; target_point_array=[], localplanneronly=false)
    points::Array{Point} = generate_points(cell)
    """
    if dis = predict(p1,p2)
        then,  
            dis = -1  not add points (but cannot tell which point is bad, so need to use local_planner firstly)
            dis = -2  not add edge
    """
    free_points::Array{Point} = []
    for p in points  ###TODO: change predict in shadowcell, no need to do !isfree(space, i_point)), since already did here
        if isfree(cell.space, p)
            push!(free_points, p)
        end
    end
    
    if !isempty(target_point_array)
        for t_p in target_point_array
            if !(t_p in free_points)  # if target point not in free_points, then add t_p in
                push!(free_points, t_p)
            end
        end
    end
    ### Do check whether points is already contained, must do this, otherwise, it consider one point as two points since different ids
    free_points2::Array{Point} = [p for p in free_points if !MetricGraphs.contains(global_graph, p) ]
    all_added = MetricGraphs.add!(global_graph, free_points2)
    ### used for test only
    vg_temp = VisibilityGraphs.VisibilityGraph(cell.space)  ####for test only
    # println("all added? ", all_added)
    for p_i in 1:length(free_points)
        for p_j in (p_i+1):length(free_points)
            temp_p1 = free_points[p_i]
            temp_p2 = free_points[p_j]
            if localplanneronly  
                if localplanner(cell.space, temp_p1, temp_p2)
                    dis = MetricGraphs.metric(global_graph,temp_p1, temp_p2)
                    add_edge_with_weight!(global_graph, temp_p1, temp_p2, dis)
                #888888 use vg as gt
                # else
                    # dis = VisibilityGraphs.metric(vg_temp, temp_p1, temp_p2)
                    # add_edge_with_weight!(global_graph, temp_p1, temp_p2, dis)
                ##888888 use one intermediate point
                else  # use one intermediate point, if one doesn't work, stop trying
                    temp_inter_p, temp_min_d = find_optimal_intermediate_point(cell, temp_p1, temp_p2)
                    if temp_min_d > 1000 #dis is illegal
                        # current_path::Array{Point}, min_d::Float64= find_path_by_prm_star(current_root_cells,p1,p2)
                        nothing
                    else
                        # dis = MetricGraphs.metric(global_graph,temp_p1, temp_p2)
                        # println("metric dis and inter dis differ is: ",temp_min_d-dis)
                        add_edge_with_weight!(global_graph, temp_p1, temp_p2, temp_min_d)
                    end
                end
            else
                dis = cell.shadow_cell.predict(temp_p1, temp_p2)  
                if dis != -2  ## if == -2, not add
                    dis += MetricGraphs.metric(global_graph ,temp_p1, temp_p2)
                    add_edge_with_weight!(global_graph, temp_p1, temp_p2, dis)
                end
            end
        end
    end
end


"""
Construct global_graph for searching, it belongs to query phase

    changes made: 9/30
        Not from start cells to goal cells
        Need to cover whole spaces
        add start point and goal point into the graph [tricky things here!]
        

"""

function add_points_into_global_graph(pts, leaves, global_graph)
    for p in pts
        add_point_into_global_graph(p,leaves, global_graph)
    end
end



function add_point_into_global_graph(pt, leaves, global_graph)
    function pt_on_cell_bounds(pt, leaf)   
        # assume pt is already constained in leaf
        for i in 1:length(pt)
            if pt[i] == leaf.bounds[i].lo || pt[i] == leaf.bounds[i].hi
                return true
            end
        end
        return false
    end
    
    for leaf in leaves
        if contains(leaf, pt) 
            if pt_on_cell_bounds(pt, leaf)
                update_pt_into_global_graph(global_graph, leaf, pt)
            else
                update_pt_into_global_graph(global_graph, leaf, pt; inner_pt=true)
            end
        else
            println("point is not contained in!!!!!!!")
        end
    end
end
   


"""
Construct global graph without start and goal => add start and goal later
"""
function construct_global_graph(leaves; localplanneronly=false)  ###New version 
    ### Create a global_graph
    space = leaves[1].space
    global_graph::MetricGraph = MetricGraph(space.dim)

    for leaf in leaves
        update_cell_graph(leaf, global_graph; localplanneronly=localplanneronly)
    end
    return global_graph
end
  

"""
Using A* to construct global graph and find path simutaneously --> no need to traverse every cell
"""
function query_path_and_construct_global_graph_using_Astar(start::Point, goal::Point, leaves)
    space = leaves[1].space
    if !isfree(space, start) || !isfree(space, goal)
        return [], Inf, [], Inf
    end
    global_graph = MetricGraph(space.dim)
    for leaf in leaves
        if contains(leaf, start)
            if contains(leaf, goal)  # start and goal are in same cell
                update_cell_graph(leaf, global_graph)
                path, path_dis, refined_path,refined_path_dis = query_scdm(start,goal,leaves,global_graph; if_refine=true)
                return path, path_dis, refined_path,refined_path_dis
            end
            
            update_cell_graph(leaf, global_graph)
            leaf.added_to_global_graph = true
            SCDM.add_points_into_global_graph([start],leaves, global_graph)
        end
    end

    function add_all_cells_contain_p(p_i::Integer, global_graph::MetricGraph)
        p = get_point(global_graph, p_i) 
        for leaf in leaves
            if contains(leaf, p)
                if !leaf.added_to_global_graph
                    update_cell_graph(leaf, global_graph)
                    leaf.added_to_global_graph = true
                end
            end
        end
    end
    s_i = global_graph.pointids[start]
    if !MetricGraphs.contains(global_graph, goal)
        MetricGraphs.add!(global_graph, goal)
    end
    g_i = global_graph.pointids[goal]
    path::Array{Point} = MetricGraphs.changed_astar(global_graph, start, goal, add_all_cells_contain_p, length(leaves))
    path_dis::Union{Nothing,Float64} = distance(global_graph, path)
    if isnothing(path_dis) ## or isempty(path)
        return [], Inf, [], Inf
    else
        all_path_segs_free, refined_path, refined_leaves_set,refined_path_dis = SCDM.refine_path(path, space, leaves,path_dis, global_graph) 
        return path, path_dis, refined_path,refined_path_dis
    end

end




"""
    Find a path using dijkstra from global_graph
    ## For the final path:
    if two points can be connected by localplanner
        then ok
    else
        -> try to find an intermediate point 
        -> prm* and dijkstra inside

"""
function find_path_by_prm_star(current_root_cells,p1::Point,p2::Point)
    current_graph = current_root_cells[1].shadow_cell.prm.graph
    current_path::Array{Point}, min_d = find_path(p1, p2, current_graph)
    if length(current_root_cells) != 1
        for current_root_cell in current_root_cells[2:length(current_root_cells)]
            current_graph = current_root_cell.shadow_cell.prm.graph
            new_current_path::Array{Point}, new_min_d = find_path(p1, p2, current_graph)
            if new_min_d < min_d
                min_d = new_min_d
                current_path = new_current_path
            end
        end
    end
    return current_path, min_d
end


"""
    Need to check the output of this function
    if isempty(path_from_a_Star) or isnothing(dis_from_a_Star)
        It means there is no path between current start and goal
"""
function find_path(start::Point, goal::Point, global_graph)
    #plot(global_graph)
    s_i = global_graph.pointids[start]
    g_i = global_graph.pointids[goal]
    path_from_a_Star::Array{Point} = MetricGraphs.astar(global_graph, start, goal)
    # println("start is: ", start, " goal is: ", goal)
    # println("path is: ", path_from_a_Star)
    dis_from_a_Star::Union{Nothing,Float64} = distance(global_graph, path_from_a_Star)
    return path_from_a_Star, dis_from_a_Star
end


"""
find the cell contains both p1 and p2
"""
#TODO: There is a case that one cell contains both p1 and p2??? How would that happen?
function find_current_root_cell(p1, p2, leaves)
    p1_cells = find_current_nodes(p1, leaves)
    p2_cells = find_current_nodes(p2, leaves)
    same_cells = []
    for cell in p1_cells
        if cell in p2_cells
            push!(same_cells, cell)
        end
    end
    return same_cells
end


function refine_path(original_path, space, leaves,path_dis,g)
    refined_path = []
    refined_leaves_set = []
    all_path_segs_free = true
    new_path_dis = path_dis
    for p_i in 1:length(original_path)-1
        p1 = original_path[p_i]
        p2 = original_path[p_i+1]
        if localplanner(space, p1, p2)
            push!(refined_path, p1)
        else
            all_path_segs_free = false
            current_root_cells = find_current_root_cell(p1,p2,leaves) ### p1 p2 can have common 2 cells
            p1_and_inter_p, min_dis::Float64= find_optimal_inter_p(current_root_cells,p1,p2)
            old_min_dis::Float64 = edge_distance(g,p1,p2)
            new_path_dis = new_path_dis + min_dis - old_min_dis
            append!(refined_path,p1_and_inter_p)
        end
    end
    push!(refined_path, last(original_path)) ## add the goal
    return all_path_segs_free, refined_path, refined_leaves_set, new_path_dis
end

function find_optimal_inter_p(current_root_cells,p1::Point,p2::Point)
    inter_p, min_d = find_optimal_intermediate_point(current_root_cells[1], p1, p2)
    if length(current_root_cells) != 1
        for current_root_cell in current_root_cells[2:length(current_root_cells)]
            new_inter_p, new_min_d = find_optimal_intermediate_point(current_root_cell, p1, p2)
            if new_min_d < min_d
                min_d = new_min_d
                inter_p = new_inter_p
            end
        end
    end
    #--------------- If min_d is still > 1000, it means not path founded, local prm* should be used------#
    if min_d > 1000
        current_path::Array{Point}, min_d::Float64= find_path_by_prm_star(current_root_cells,p1,p2)
        return current_path[1:length(current_path)-1]
    end
    return [p1, inter_p], min_d
end

function find_optimal_intermediate_point(cell, c_start::Point, c_goal::Point)
    
    dim = cell.space.dim
    temp_graph::MetricGraph = MetricGraph(cell.space.dim)
    function objective(x)
        yy::Point = [x...]
        if isfree(cell.space, yy)  ###  if not, just return min_dis
            if localplanner(cell.space, yy, c_start)
                if localplanner(cell.space, yy, c_goal)
                    f0 = MetricGraphs.metric(temp_graph, yy, c_start)
                    fq = MetricGraphs.metric(temp_graph, yy, c_goal)
                    return f0+fq
                end
            end
        end
        return 10000.0
    end
    search_range = [(b.lo,b.hi) for b in cell.bounds]
            # MaxSteps=50000, TraceInterval=1.0, TraceMode=:silent 
    # res = bboptimize(objective; SearchRange = search_range, NumDimensions = dim,Method = :adaptive_de_rand_1_bin_radiuslimited)
    res = bboptimize(objective; SearchRange = search_range, NumDimensions = dim,Method = :adaptive_de_rand_1_bin_radiuslimited, TraceMode=:silent)
    # MaxTime = 3
    # println("The best result is: ", best_fitness(res), " ", best_candidate(res))
    inter_p_second::Point = [best_candidate(res)...]
    return inter_p_second,best_fitness(res)
end

function divide_current_leaf_node(space, n, leaf_cell, max_cell_depth,use_2D_rplr)
    dim = space.dim
    function divide_current_leaf_node_helper(cell)
        if cell.base_cell.inside_is_free 
            nothing
        elseif cell.depth >= max_cell_depth
            nothing
        else
            divide(cell, cell.depth%dim+1)
            for sub_cell in cell.children
                sub_cell.base_cell = BaseCell(space, n, sub_cell.bounds,use_2D_rplr)
                divide_current_leaf_node_helper(sub_cell)
            end
        end
    end
    divide_current_leaf_node_helper(leaf_cell)
end

"""
    Much all cell.children.children is empty!!!
"""
function cell_grandchildren_are_leaf_nodes(cell, dim)
    # grandfather_depth = cell.depth
    # function cell_grandchildren_are_leaf_nodes_helper(sub_cell)
    #     if isempty(sub_cell.children)
    #         return true
    #     else
    #         if sub_cell.depth == grandfather_depth + dim
    #             return false
    #         else
    #             for sub_child in sub_cell.children
    #                 result = cell_grandchildren_are_leaf_nodes_helper(sub_child)
    #                 if result == false
    #                     return false
    #                 end
    #             end
    #             return true
    #         end
    #     end
    # end
    # result = cell_grandchildren_are_leaf_nodes_helper(cell)
    # return result
    if isempty(cell.children)
        return false
    else
        for c in cell.children
            if !isempty(c.children)
                return false
            end
        end
        return true
    end
end

function get_all_grandchildren_cells(cell,dim)
    grandfather_depth = cell.depth
    gdchildren_leaves = []
    function get_all_grandchildren_cells_helper(sub_cell)
        if sub_cell.depth == grandfather_depth + dim
            push!(gdchildren_leaves, sub_cell)
        else
            if isempty(sub_cell.children)
                push!(gdchildren_leaves, sub_cell)
            else
                for sub_child in sub_cell.children
                    get_all_grandchildren_cells_helper(sub_child)
                end
            end
        end
    end
    get_all_grandchildren_cells_helper(cell)
    return gdchildren_leaves
end
    
###todo: combine the two function above together
function construct_current_leaf_node_helper(current_global_graph, cell, space, grandchildren_leaves)
           #Notice!!! use globalgraph to get all pairs distances, saved in a new graph
    #TODO: use intermediate points as well for smallest cell

    ##TODO: points can be reused, find a method to do this
    current_global_graph_pts= get_points(current_global_graph)
    sub_bd_points::Array{Point} = cell.base_cell.bd_points
    for bd_p in sub_bd_points
        if !(bd_p in current_global_graph_pts)
            println("!!!!!####Add this points into global_graph")
            add_point_into_global_graph(bd_p,grandchildren_leaves,current_global_graph)
        end
    end
    distance_of_edges::Array{Float64} = []
    for i1 in 1:length(sub_bd_points)
        for i2 in i1+1:length(sub_bd_points) #todo i1+1 or i1
            bd_p1 = sub_bd_points[i1]
            bd_p2 = sub_bd_points[i2]
            ## Added localplanner, can delete if
            if localplanner(cell.space, bd_p1, bd_p2)
                dis = MetricGraphs.metric(current_global_graph,bd_p1, bd_p2)
                push!(distance_of_edges, dis)
            else
                path::Array{Point}, path_dis = find_path(bd_p1, bd_p2, current_global_graph)
                if isnothing(path_dis)
                    push!(distance_of_edges, Inf)
                else
                    push!(distance_of_edges, path_dis)
                end
            end
        end
    end
    # cell.children = [] #abandon children and grandchildren
    return sub_bd_points, distance_of_edges
end
"""
This cell is completely free inside
"""
function cell_is_leaf_node(cell) 
    free_points = cell.base_cell.bd_points ###no need to call generate_points, bd_points are same
    temp_graph::MetricGraph = MetricGraph(cell.space.dim)
    distance_of_edges::Array{Float64} = []
    ### added for test
    vg_temp = VisibilityGraphs.VisibilityGraph(cell.space)   ####added for test
    ### added for test
    for p_i in 1:length(free_points)
        for p_j in (p_i+1):length(free_points)
            if localplanner(cell.space, free_points[p_i], free_points[p_j])
                dis = MetricGraphs.metric(temp_graph,free_points[p_i], free_points[p_j])
                push!(distance_of_edges, dis)
            else
                push!(distance_of_edges, Inf)
            end
            #88888 use vg as getleaves
            # dis = VisibilityGraphs.metric(vg_temp, free_points[p_i], free_points[p_j])
            # push!(distance_of_edges, dis)
        end
    end
    return free_points, distance_of_edges
end
        

function construct_current_leaf_node(cell, space, max_cell_depth)
    if cell_grandchildren_are_leaf_nodes(cell, space.dim) #grandfather has 4 grandchildren leaf node     
        println("--------Cell1: ", cell.bounds)
        # grandchildren_leaves = get_all_grandchildren_cells(cell, space.dim) 
        grandchildren_leaves = cell.children
        if length(grandchildren_leaves) != space.dim
            println("ERROR OCCUR!!!")
        end
        current_global_graph = construct_global_graph(grandchildren_leaves; localplanneronly=true)
        # fig, ax = PyPlot.subplots()
        # plot(current_global_graph)
        bd_points, distance_of_edges = construct_current_leaf_node_helper(current_global_graph,cell, space,grandchildren_leaves)
        return bd_points, distance_of_edges
    elseif isempty(cell.children)
        println("--------Cell2: ", cell.bounds)
        bd_points, distance_of_edges = cell_is_leaf_node(cell)
        return bd_points, distance_of_edges
    else
        println("--------Cell3: ", cell.bounds)
        current_leaf_global_graph::MetricGraph = MetricGraph(space.dim)
        # grandchildren_leaves = get_all_grandchildren_cells(cell, space.dim) 
        grandchildren_leaves = cell.children
        for gdchild in grandchildren_leaves     
            bd_points::Array{Point}, distance_of_edges::Array{Float64} = construct_current_leaf_node(gdchild, space, max_cell_depth)
            bd_points_not_added::Array{Point} = [p for p in bd_points if !MetricGraphs.contains(current_leaf_global_graph, p) ]
            all_added = MetricGraphs.add!(current_leaf_global_graph, bd_points_not_added)
            # println("all_added?", all_added)
            counter = 0
            for i1 in 1:length(bd_points)
                for i2 in i1+1:length(bd_points)
                    bd_p1 = bd_points[i1]
                    bd_p2 = bd_points[i2]
                    counter +=1
                    temp_dis = distance_of_edges[counter]
                    if temp_dis != Inf 
                        add_edge_with_weight!(current_leaf_global_graph, bd_p1, bd_p2, temp_dis)
                    end
                end
            end
            if counter != length(distance_of_edges)
                println("ERROR OCCUR!!!")
            end
        end
        # fig, ax = PyPlot.subplots()
        # plot(current_leaf_global_graph)
        bd_points, distance_of_edges = construct_current_leaf_node_helper(current_leaf_global_graph,cell, space,grandchildren_leaves)
        return bd_points, distance_of_edges
    end
end


function get_pre_training_data(leaf, bd_points, distance_of_edges,space)
    # total_bd_points = leaf.base_cell.total_bd_points
    # println("length of bd_points: ", length(bd_points))
    # println("length of distance_of_edges: ", length(distance_of_edges))
    pre_Xc, pre_Yc, pre_X, pre_Y =[],[],[],[] #pre_X is non_zero_pairs
    use_clf = false 
    counter = 0
    temp_graph::MetricGraph = MetricGraph(space.dim)
    for i1 in 1:length(bd_points)
        i_point = bd_points[i1]
        for i2 in i1+1:length(bd_points)
            ####TODO: NEED TO MODIFY TO 1:LENGTH(BD_POTS)
        ####Important!!!! Must train all pairs, not half triangle, 
        #### because those points has no specific order, hard to know should p1p2 or p2p1
            j_point = bd_points[i2]
            counter +=1
            x1_y1_x2_y2 = []
            append!(x1_y1_x2_y2, i_point)
            append!(x1_y1_x2_y2, j_point)
            push!(pre_Xc, x1_y1_x2_y2)
            ###-----------------
            x2_y2_x1_y1 = []
            append!(x2_y2_x1_y1, j_point)
            append!(x2_y2_x1_y1, i_point)
            push!(pre_Xc, x2_y2_x1_y1)
            ###-----------------
            dis = distance_of_edges[counter]
            if localplanner(space, i_point, j_point)
                nothing
            else
                if dis == Inf 
                    push!(pre_Yc,-2)
                    use_clf = true
                else
                    metric_dis = MetricGraphs.metric(temp_graph, i_point, j_point)
                    push!(pre_Yc,1) # even dis-metric_dis == 0, label=1
                    push!(pre_X, x1_y1_x2_y2)
                    push!(pre_Y, dis-metric_dis)
                    ###-----------------
                    push!(pre_Yc,1) # even dis-metric_dis == 0, label=1
                    push!(pre_X, x2_y2_x1_y1)
                    push!(pre_Y, dis-metric_dis)
                    ###-----------------
                end
            end
        end
    end
    if length(distance_of_edges) != counter
        println("ERROR!!!!!NOT EQUAL!!!!")
    end
    println("counter is: ", counter)
    println("use clf: ",use_clf)
    return pre_Xc, pre_Yc, pre_X, pre_Y, use_clf
end


"""
    Construct the whole scdm:
    training classifier and regressor of each cell
"""
function construct_scdm(
        clf_choosed,
        reg_choosed,
        depth,
        space,
        n;
        num_pts_per_edge::Integer=20,
        use_vg_as_gt::Bool=true,
        use_localplanner::Bool=false,
        use_2D_rplr::Bool=false,
        use_nD_rplr::Bool=false,
        plot_rplr::Bool=false,
        max_cell_depth::Integer=20,  #added for new method
        use_multi_resolution_approach::Bool=false, #added for new method
        use_vg_as_gt_for_multi_resolution_approach::Bool=false
        )
    root_cell = Cell(space, space.bounds,0; num_pts_per_edge=num_pts_per_edge)
    divide_scdm(root_cell, depth; dim=space.dim)                                        
    leaves = get_all_leaves(root_cell)
    vg_array = use_vg_as_gt ? [VisibilityGraphs.VisibilityGraph(space)] : []
    for leaf in leaves
        clf = deepcopy(clf_choosed) #Each cell should have a differ classifier
        reg = deepcopy(reg_choosed)  #Each cell should have a differ Regressor
        if use_nD_rplr  reg.bounds = [leaf.bounds;leaf.bounds] end
        # use base_cell to determine whether inside_is_free firstly 
        leaf.base_cell = BaseCell(space,n,leaf.bounds,use_2D_rplr)
        pre_Xc, pre_Yc, pre_X, pre_Y = [],[],[],[]
        pre_use_clf = false
        if use_multi_resolution_approach
            divide_current_leaf_node(space, n, leaf, max_cell_depth, use_2D_rplr)    
            bd_points, distance_of_edges = construct_current_leaf_node(leaf, space, max_cell_depth)
            pre_Xc, pre_Yc, pre_X, pre_Y, pre_use_clf = get_pre_training_data(leaf, bd_points, distance_of_edges,space)
            if isempty(pre_X)
                println("ERROR!!!pre_X is empty!!!!!!!!!!")
            end
        end
        leaf.shadow_cell= ShadowCell(
            leaf.base_cell,
            space,
            n,
            leaf.bounds;
            inside_is_free=leaf.base_cell.inside_is_free,
            pre_Xc=pre_Xc,
            pre_Yc=pre_Yc,
            pre_X=pre_X,
            pre_Y=pre_Y,
            pre_use_clf=pre_use_clf,
            clf=clf,
            reg=reg,
            use_2D_rplr=use_2D_rplr,
            use_localplanner=use_localplanner,
            vg_array=vg_array,
            plot_rplr=plot_rplr,
        )
    end
    fig, ax = PyPlot.subplots()
    ax.set_aspect(aspect=1)
    plot_multi_resolution_cells(leaves,ax)
    PyPlot.savefig("data/output/images/multi-resolution cells")
    return leaves
end






"""
Query SCDM: Find a path and refine the path
"""
function query_scdm(start::Point,goal::Point,leaves,global_graph; if_refine=true)
    space = leaves[1].space
    if isfree(space, start) && isfree(space, goal)
        add_points_into_global_graph([start, goal],leaves, global_graph)
        path::Array{Point}, path_dis = SCDM.find_path(start, goal, global_graph)
        if isnothing(path_dis) ## or isempty(path)
            return [], Inf, [], Inf
        else
            all_path_segs_free, refined_path, refined_leaves_set,refined_path_dis = SCDM.refine_path(path, space, leaves,path_dis, global_graph) 
            return path, path_dis, refined_path,refined_path_dis
        end
    else
        return [], Inf, [], Inf
    end 
end


#region plot helpers
"""
Plots the contour of a function
"""
function plot_cell(func::Function, length, bounds, ax::PyObject;
    flevels=100::Integer, clevels=10::Integer, cbar=true::Bool)

    len = length
    x_bounds =  bounds[1]
    y_bounds =  bounds[2]

    plot_cell_rec(bounds, ax, facecolor="w", zorder=1)

    x = range(x_bounds.lo, stop=x_bounds.hi, length=len)
    y = range(y_bounds.lo, stop=y_bounds.hi, length=len)'
    
    function V(x_,y_)
        i = findall(e->e==x_, x)[1] #TODO: a faster way
        j = findall(e->e==y_, y')[1]
        return func(i, j)
    end
    z = V.(x, y)
    cpf = ax.contourf(x, y, z, levels=clevels, zorder=2) #levels=flevels,
    cp = ax.contour(x, y, z, levels=clevels, colors="black", zorder=3)
    if cbar 
        cbar_ = PyPlot.colorbar(cpf, ax=ax) 
#         cbar.set_label('Electric Field [V/m]')
        cbar_.set_ticks(clevels)
    end
end

"""
Plots a line
"""
function plot_line(line::Line, ax::PyObject; color="grey"::String, zorder=2::Integer, linestyle::String="solid",alpha=1.0)
    x, y = [p[1] for p in line], [p[2] for p in line]
    ax.plot(x, y; color=color, zorder=zorder, linestyle=linestyle,alpha=alpha)
end



"""
Plots an array of lines
"""
function plot_lines(lines::Array{Line}, ax::PyObject;
        color="grey"::String, zorder=2::Integer, vertices=false::Bool, linestyle::String="solid",alpha::Float64=1.0)

    for line in lines
        if vertices plot(line[1], ax=ax, color=color, zorder=zorder) end
        plot_line(line, ax; color=color, zorder=zorder,linestyle=linestyle, alpha=0.7)
    end
    if vertices plot(last(last(lines)); ax=ax, color=color, zorder=zorder) end
end

"""
Plots the boundary of a cell (the Rectangle)
"""
function plot_cell_rec(bounds, ax::PyObject;facecolor::String="grey", zorder::Integer=2)

    x_bounds =  bounds[1]
    y_bounds =  bounds[2]

    poly::Polygon = Polygon([
        [x_bounds.lo, y_bounds.lo],
        [x_bounds.hi, y_bounds.lo],
        [x_bounds.hi, y_bounds.hi],
        [x_bounds.lo, y_bounds.hi]
    ])
    poly_array::Array{Polygon} = [poly]
    MetricTools.plot(poly_array; ax=ax, facecolor=facecolor, zorder=zorder)

end
#endregion plot helpers

#region plots
function plot_gt(leaves)
    fig, ax = PyPlot.subplots()
    ax.title.set_text("Ground Truth of 4 cells")
    ax.set_aspect(aspect=1)
    cmin = -0.1
    cmax = 0.2
    lvls = range(cmin,stop=cmax, length=10)
    # for leaf in leaves
    for l in 1:length(leaves)
        leaf=leaves[l]
        ground_truth = ShadowCells.ground_truth(leaf.shadow_cell)
        if l != length(leaves)
            plot_cell(ground_truth, length(leaf.shadow_cell.total_bd_points),leaf.bounds, ax, clevels=lvls, cbar = false)
        else
            plot_cell(ground_truth, length(leaf.shadow_cell.total_bd_points),leaf.bounds, ax, clevels=lvls, cbar = true)
        end
    end
end


function plot_pred(leaves)
    fig, ax = PyPlot.subplots()
    ax.title.set_text("Prediction of 4 cells")
    ax.set_aspect(aspect=1)
    cmin = -0.1
    cmax = 0.2
    lvls = range(cmin,stop=cmax, length=10)
    for l in 1:length(leaves)
        leaf=leaves[l]
        predictforplot = ShadowCells.predict_for_plot(leaf.shadow_cell)
        if l != length(leaves)
            plot_cell(predictforplot, length(leaf.shadow_cell.total_bd_points), leaf.bounds, ax, clevels=lvls, cbar = false)
        else
            plot_cell(predictforplot, length(leaf.shadow_cell.total_bd_points), leaf.bounds, ax, clevels=lvls, cbar = true)
        end
    end
end

function plot_error(leaves,total_RMSE)
    fig, ax = PyPlot.subplots()
    ax.title.set_text("Error of 4 cells")
    ax.set_aspect(aspect=1)
    cmin = 0.0
    cmax = 0.12
    lvls = range(cmin,stop=cmax, length=50)
    for l in 1:length(leaves)
        leaf = leaves[l]
        ground_truth = ShadowCells.ground_truth(leaf.shadow_cell)
        predictforplot = ShadowCells.predict_for_plot(leaf.shadow_cell)
        RMSE = 0
        counter = 0
        function differ_function(x_i,y_i)
            temp = predictforplot(x_i,y_i)-ground_truth(x_i,y_i)
            t = temp*temp
            RMSE += t
            counter +=1
            return abs(temp)
        end
        if l != length(leaves)
            plot_cell(differ_function, length(leaf.shadow_cell.total_bd_points), leaf.bounds, ax, clevels=lvls, cbar = false)
        else
            plot_cell(differ_function, length(leaf.shadow_cell.total_bd_points), leaf.bounds, ax, clevels=lvls, cbar = true)
        end
        total_RMSE += sqrt(RMSE/counter)
        println("RMSE of current cell is: ", RMSE)
    end
    return total_RMSE
end

function plot_scdm(
    leaves,
    start::Point,
    goal::Point,
    space;
    plot_scdm_gt::Bool=false, 
    plot_scdm_pred::Bool=false,
    plot_scdm_error::Bool=false)
    total_RMSE = 0
    if plot_scdm_gt plot_gt(leaves) end
    if plot_scdm_pred plot_pred(leaves) end
    if plot_scdm_error 
        total_RMSE = plot_error(leaves, total_RMSE)
    end
    return total_RMSE
end

"""
plot the workspace, i.e. the real 
"""
function plot_real_env(leaves,ax; title="Environment and paths")
    ax.title.set_text(title)
    # plot cells division in workspace
    for leaf in leaves
        plot_cell_rec(leaf.bounds, ax, facecolor="w", zorder=1)
        # MetricGraphs.plot(leaf.shadow_cell.prm.graph; fig=fig, ax=ax) ### plot the graph -for generate all-pairs
    end
    plot(leaves[1].space.obstacles; ax=ax)    
end
# Plot resolutional cells
function plot_multi_resolution_cells(leaves,ax)
    sub_leaves = []
    for leaf in leaves
        temp = get_all_leaves(leaf)
        append!(sub_leaves, temp)
    end
    ax.title.set_text("Environment and multi resolution cells")
    # plot cells division in workspace
    for sub_leaf in sub_leaves
        plot_cell_rec(sub_leaf.bounds, ax, facecolor="b", zorder=1)
        # MetricGraphs.plot(leaf.shadow_cell.prm.graph; fig=fig, ax=ax) ### plot the graph -for generate all-pairs
    end
    plot(leaves[1].space.obstacles; ax=ax)    
end

function plot_scdm_path(leaves, path::Union{Array,Array{Point}}, ax; refined_path::Array=[])
    if !isempty(path)
        ax.set_aspect(aspect=1)
        cmin = 0.0
        cmax = 2.0  ### previously, 2.0
        lvls = range(cmin,stop=cmax, length=50)
        plot(path; ax=ax, color="red",s=40, zorder=1)
        if !isempty(refined_path)
            refined_path_plot::Array{Line} = [(refined_path[i],refined_path[i+1]) for i in 1:length(refined_path)-1 ]
            plot_lines(refined_path_plot,ax,color="blue", zorder=2 ,vertices=false,linestyle="-.",alpha=0.5)
            middle_points::Array{Point} = [p for p in refined_path if !(p in path)]
            println("the middle points are: ", middle_points)
            plot(middle_points; ax=ax, color="blue", s=40, zorder=1)
        end
    end
end


function plot_vg_path(ax,space, start, goal; linestyle="-.", linecolor="orange")
    vg = VisibilityGraphs.VisibilityGraph(space)
    vg_path::Union{Array{Point},Array}= VisibilityGraphs.plan(vg, start, goal)
    vg_dis::Float64 = metric(vg.graph, vg_path)
    vg_path_plot::Array{Line} = [(vg_path[i],vg_path[i+1]) for i in 1:length(vg_path)-1 ]
    plot_lines(vg_path_plot,ax,color=linecolor, zorder=2 ,vertices=false, linestyle=linestyle,alpha=0.5)
    return vg_path, vg_dis
end

function plot_env_and_paths(
        leaves,
        space,
        start,
        goal;
        path::Union{Array{Point}, Array}=[],
        refined_path::Array=[],
        plot_env::Bool=true,
        plot_path_of_scdm::Bool=false,
        plot_path_of_vg::Bool=false
        )
    fig, ax = PyPlot.subplots()
    if plot_env plot_real_env(leaves,ax) end
    if plot_path_of_scdm plot_scdm_path(leaves, path, ax,refined_path=refined_path) end
    vg_path::Union{Array{Point}, Array}=[] ; vg_dis::Float64 = 0
    if plot_path_of_vg
        vg_path, vg_dis = plot_vg_path(ax,space, start,goal) 
    end
    #PyPlot.savefig("Workspace and paths")
    return ax,vg_path, vg_dis
end
#endregion plots

#region memorycost

function total_FP_of_one_rplr(plr)
    D = 2  ## dimenstion of sample points
    leaves::Array{BinaryPartition} = getleaves(plr)
    numparams::Integer = getnumparams(plr)
    num_fp_in_bsp = length(leaves) - 1
    num_fp_bounds = 2 * D 
    return numparams + num_fp_in_bsp + num_fp_bounds
end

function total_FP_of_xgboost(n_cells,n_depth, n_trees)
    return n_cells*n_trees*(2^(n_depth+1)-1)
end

function get_memory_cost_of_SCDM(
    leaves, 
    use_2D_rplr::Bool, 
    use_nD_rplr::Bool; 
    c_epoch::Integer=200,
    r_epoch::Integer=200, 
    xgboost_tree_depth::Integer=3)

    FP = 0
    if use_nD_rplr || use_2D_rplr
        for leaf in leaves
            FP += total_FP_of_one_rplr(leaf.shadow_cell.reg)
            if leaf.shadow_cell.use_clf
                FP += FP = total_FP_of_xgboost(1,xgboost_tree_depth,c_epoch)
            end
        end
    else
        FP = total_FP_of_xgboost(length(leaves),xgboost_tree_depth,r_epoch)
    end
    return FP
end

function total_FP_of_prm(N,D)
    # total_FP = N*D + N*log(2,N)*3/2.0 
    ## adjacent list can only save index_of_p1, index_of_p2, dis
    k::Integer = round(Int, log(2, N))
    total_FP = N*D + N*k*3/2.0
    return total_FP
end
#endregion

end #end of the module
