#=
 * File: FSTs.jl
 * Project: planners
 * File Created: Tuesday, 13th August 2019 11:12:15 am
 * Author: Josiah Putman (joshikatsu@gmail.com)
 * -----
  * Last Modified: Thursday, 7th November 2019 10:00:45 pm
 * Modified By: Josiah Putman (joshikatsu@gmail.com)
=#

module FSTs

#region imports
import PyCall, PyPlot
import LinearAlgebra
using DataStructures

using MetricTools, MetricTools.Math, 
    MetricTools.MetricGraphs, MetricTools.Visualization
using MetricRegressors
using MetricSpaces, MetricSpaces.Samplers
using ..MotionPlanning
#endregion imports

export FST

#region constants
const FST_IMG_DIR = "data/output/images/fst/"
const FST_VID_DIR = "data/output/videos/fst/"

"""
Called when the module is loaded, makes sure all directories are created
"""
function __init__()
    mkpath(FST_IMG_DIR)
    mkpath(FST_VID_DIR)
end
#endregion constants


#region structs
mutable struct Field
    center::Point
    radius::Float64
    distance::Union{Nothing, Float64}
    parent::Union{Nothing, Field}
    children::Set{Field}
    processed::Bool
    regressor::Union{Nothing, Regressor}    # local regressor
    
    function Field(center::Point, radius::Float64;
            parent::Union{Nothing, Field}=nothing,
            regressor::Union{Nothing, Regressor}=nothing)
        field::Field = new(center, radius, nothing, parent, Set(), false, regressor)
        return field
    end
end

"""
For verbosity settings
"""
mutable struct VerbositySettings
    enabled::Bool
    video::Bool
    videodir::String
    imagedir::String
end

"""
Struct for the Field Search Tree
"""
mutable struct FST
    space::Space
    sampler::Function
    fields::Dict{Point, Field}
    graph::MetricGraphs.MetricGraph     # graph keeping track of parents
    start::Point                        # start point of the FST
    minr::Float64
    maxr::Float64
    verbosity::VerbositySettings

    function FST(
            space::Space,
            n::Integer,
            start::Point;
            sampler=uniform_random::Function,
            minr::Float64=0.05,
            maxr::Float64=0.3,
            verbose::Bool=true,
            video::Bool=false)

        fst::FST = new(
            space,
            sampler,
            Dict(),
            MetricGraph(space.dim;
                metric=dmetric(space),
                knntype=KNNs.BallTreeKNN),
            start,
            minr,
            maxr,
            VerbositySettings(verbose, video, "", ""))
        create!(fst, n)
        return fst
    end
end
#endregion structs

#region functions
"""
Gets the optimal point connecting two fields
"""
function optimalpoint(fst::FST, f::Field, pf::Field, p::Point)::Point
    c::Point = f.center; r::Float64 = f.radius

    grad::Point = f.regressor.approx[2:lastindex(f.regressor.approx)]
    x::Point = c - grad * r / metric(fst.space, grad, [0. for _ in c])
    if !localplanner(fst.space, x, c) || !localplanner(fst.space, x, p)
        x = c
    end

    return x
end

"""
Uses nonlinear opimization to calculate the distance to a point from a field
"""
function calcdist!(fst::FST, known::Field, intermed::Field, unknown::Field)
    x::Point = optimalpoint(fst, known, intermed, unknown.center)
    if isempty(x) 
        if localplanner(fst.space, known.center, unknown.center) error("Optimizer failed when obvious path existed.") end
        return end
        
    pred::Float64 = max(
        predict(known.regressor, x),
        known.distance - metric(fst.space, x, known.center))
    dist::Float64 = pred + metric(fst.space, x, unknown.center)
    
    unknown.distance = dist
    unknown.parent = known
    add!(fst.graph, unknown.center, known.center)
end

"""
Calculates the regression for this field
Assumes that all children have already had their distances calculated
"""
function regress!(fst::FST, field::Field)
    points::Array{Point} = [c.center for c in field.children if !isnothing(c.distance)]
    values::Array{Float64} = [c.distance for c in field.children if !isnothing(c.distance)]

    if length(points) <= fst.space.dim 
        display(points)
        error("$(field.center): failed to find nearby points within radius $(field.radius). Increase samples and decrease min radius.")
    end

    train!(field.regressor, points, values)
    if isempty(field.regressor.approx) error("Regression failed on $(field.center) with points $points and values $values.") end
    field.processed = true
end

"""
Samples the space and constructs a Metric Graph for the FST
"""
function initgraph!(fst::FST, n::Integer)
    if fst.verbosity.enabled println("Initializing graph...") end
    points::Array{Point} = [p for p in fst.sampler(fst.space, n)]
        # if cert(fst.space, p) > fst.minr]
    add!(fst.graph, points)
    add!(fst.graph, fst.start)
    nnbuild!(fst.graph)
end

"""
Sets up all fields in the FST
"""
function initfields!(fst::FST)
    # creates a field for each point in the metric graph
    if fst.verbosity.enabled println("Initializing fields...") end
    for p in get_points(fst.graph)
        r::Float64 = fst.maxr # min(cert(fst.space, p), fst.maxr)
        fst.fields[p] = Field(p, r; regressor=LinearRegressor())
    end

    # computes all of the children of each field
    if fst.verbosity.enabled println("Computing child fields...") end
    for (point, field) in fst.fields
        childpts::Array{Point} = inradius(fst.graph, point, field.radius; ordered=false)
        field.children = Set([fst.fields[cp] for cp in childpts if cp != point])
    end

    startf::Field = fst.fields[fst.start]
    startf.distance = 0
    for child in startf.children
        child.distance = metric(fst.space, fst.start, child.center)
    end

    regress!(fst, startf)
    if fst.verbosity.enabled println("Done initfields.") end
end

function findparents(fst::FST, parent::Field, children::Array{Field})::Array{Field}

    # For each grandchild, keep track of all possible parents
    parentcosts::Dict{Field, Float64} =
        Dict{Field, Float64}()

    for child in children
        for grandchild in child.children
            if !isnothing(grandchild.distance) continue end
            parentcost::Float64 = metric(fst.space, grandchild.center, child.center) + predict(parent.regressor, child.center)

            # If this is the closest pair seen so far
            if !(grandchild in keys(parentcosts)) || parentcost < parentcosts[grandchild]
                parentcosts[grandchild] = parentcost
                grandchild.parent = child
            end
        end
    end

    return collect(keys(parentcosts))
end

"""
Builds the FST
"""
function create!(
        fst::FST,
        n::Integer;
        ax=PyCall.PyNULL()::PyCall.PyObject,
        dirname="default"::String)::PyCall.PyObject

    if fst.verbosity.video
        fst.verbosity.imagedir = "$FST_IMG_DIR$dirname/"
        fst.verbosity.videodir = "$FST_VID_DIR$dirname/"
        mkpath(fst.verbosity.imagedir)
        mkpath(fst.verbosity.videodir)

        # setup for plotting
        PyPlot.clf()
        PyPlot.autoscale(false)
        i::Integer = 0
    end

    initgraph!(fst, n)
    initfields!(fst)

    # init the priority queue
    pq::PriorityQueue = PriorityQueue()     # queue of known fields (approx complete)
    pq[fst.fields[fst.start]] = 0

    while !isempty(pq)
        parent::Field = dequeue!(pq)
        if fst.verbosity.enabled println("Parent: $(parent.center)") end
        
        if fst.verbosity.video
            plot(fst, parent, i, dirname; ax=ax)
            i += 1
        end
        
        # Calculate distances to all grandchildren
        children::Array{Field} = [
            child for child in parent.children
            if !isnothing(child.distance) && !child.processed]
        
        grandchildren::Array{Field} = findparents(fst, parent, children)
        for grandchild in grandchildren
            calcdist!(fst, parent, grandchild.parent, grandchild)
        end
        
        for child in children
            regress!(fst, child)
            pq[child] = child.distance
            if fst.verbosity.enabled println("    Regressed $(child.center)") end
        end
    end

    if fst.verbosity.video compilemp4("$FST_IMG_DIR$dirname/";
        outdir="$FST_VID_DIR$dirname/", 
        outfile="fst.mp4")
    end
end

function bpoints(fst::FST, f::Field; l::Integer=30)::Array{Point}
    bU::Array{Point} =
        gridl([Bound(0, 2*pi) for _ in 1:fst.space.dim - 1], l)
    bX::Array{Point} =
        [polar2rn(bu, f.center, f.radius) for bu in bU]
    c::Point = f.center; r::Float64 = f.radius

    grad::Point = f.regressor.approx[2:lastindex(f.regressor.approx)]
    x::Point = c - grad * r / metric(fst.space, grad, [0. for _ in c])
    push!(bX, x)
    return bX
end

"""
Finds a path for the FST

Start at the field containing the goal, find its parent.
Find the optimal point on that field's boundary to connect to goal.
Set the current to this new point and recurse.
"""
function MotionPlanning.plan(fst::FST, goal::Point; useopt=true::Bool)::Array{Point}
    npoint::Point = first(knn(fst.graph, goal, 1))
    
    ggraph::MetricGraph = MetricGraph(fst.space.dim;
        metric=dmetric(fst.space))
    pfield::Field = fst.fields[npoint]

    field::Field = pfield.parent
    fieldpath::Array{Field} = [pfield]

    # Add the goal
    add!(ggraph, goal)
    pbX::Array{Point} = [goal]

    while true
        println("Path center: $(field.center)")
        bX::Array{Point} = bpoints(fst, field)
        add!(ggraph, bX)
            
        # add all pairs edges
        for (bx1, bx2) in Iterators.product(pbX, bX)
            if !localplanner(fst.space, bx1, bx2) continue end
            add!(ggraph, bx1, bx2)
        end

        pfield = field
        pbX = bX
        push!(fieldpath, field)

        if field.center == fst.start || isnothing(field.parent) break end
        field = field.parent
    end

    if !useopt return [f.center for f in fieldpath] end

    # Add the start
    add!(ggraph, fst.start)
    for bx in pbX
        if !localplanner(fst.space, bx, fst.start) continue end
        add!(ggraph, bx, fst.start)
    end

    println("Optimizing path...")
    path::Array{Point} = astar(ggraph, fst.start, goal)
    return path
end

"""
Predicts the value of a point using the field tree
"""
function MetricRegressors.predict(fst::FST, point::Point)::Union{Nothing, Float64}    
    npoint::Point = first(knn(fst.graph, point, 1))
    field = fst.fields[npoint]
    if isnothing(field.regressor) || isempty(field.regressor.approx)
        return nothing
    end

    val = predict(field.regressor, point)
    return val
end

"""
Returns the distance function for an FST
"""
function MetricTools.distfunc(fst::FST)::Function
    return (point::Point) -> predict(fst, point)
end

"""
Plot step of PRM construction and safe to file
"""
function MetricTools.plot(
        fst::FST,
        parent::Field,
        i::Integer,
        dirname::String;
        ax=PyCall.PyNULL()::PyCall.PyObject)
        
    println("Processing $(parent.center)...")

    points::Array{Point} = [
        field.center for field in parent.children
        if !field.processed
    ]
    PyPlot.clf()
    PyPlot.autoscale(false)
    plot(points; ax=ax, color="blue", zorder=4)
    plot(fst; ax=ax, cbar=true)
    PyPlot.savefig("$(FST_IMG_DIR)$(dirname)/img$(lpad(i, 4, '0')).png")
end

"""
Plots a fst
"""
function MetricTools.plot(fst::FST;
        ax=PyCall.PyNULL()::PyCall.PyObject,
        cbar=true::Bool, cmax::Float64=1.5)::PyCall.PyObject
    
    plot(distfunc(fst); ax=ax, cbar=cbar, cmax=cmax)
    plot(fst.space; ax=ax)
    plot(fst.graph; ax=ax, progress=false)
    
    return ax
end
#endregion functions

end
