# MotionPlanning.jl

Open motion planning library used by Dartmouth Robotics Lab.

```julia
Julia version: 1.3
```
## Setup for usage

```julia
] registry add https://gitlab.com/dartmouthrobotics/DartmouthJuliaRegistry
add MotionPlanning
```

## Setup for development

Clone the repo, and then:

```julia
] registry add https://gitlab.com/dartmouthrobotics/DartmouthJuliaRegistry
update
dev MetricTools MetricSpaces
```

Deving MetricTools and MetricSpaces allows you to get the most recent versions of these supporting packages even if they haven't been updated in the Dartmouth Registry yet.

```shell
cd ~/.julia/dev/MetricTools
git pull
cd ~/.julia/dev/MetricSpaces
git pull
```
