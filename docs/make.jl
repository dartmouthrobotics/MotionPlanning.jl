if false
    include("../src/MotionPlanning.jl")
    using .MotionPlanning
end

using Documenter, MotionPlanning

Documenter.makedocs(
    sitename="MotionPlanning.jl",
    modules=[
        MotionPlanning
    ],
    pages=[
        "index.md",
        "reference.md"
    ],
    repo="https://gitlab.com/dartmouthrobotics/MotionPlanning.jl"
)

