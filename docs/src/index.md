# Notes

Implementation notes.

## Structure

The main library is broken into the following submodules in `src`:

* `MotionPlanning.jl` - module containing all planners as submodules
  * `PRM.jl` - a probabilistic roadmap implementation
  * `RRT.jl` - a rapidly-exploring random tree implementation
* `Spaces.jl` - module containing all search spaces, collision checkers, simulations, and corresponding visualization
* More coming soon!
